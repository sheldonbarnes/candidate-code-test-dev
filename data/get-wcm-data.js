// Run npm install in this directory
// Run `node get-wcm-data` in this directory, then copy all data to the localData-dev repo
"use strict";

var exec    = require('child_process').exec;
var fs      = require('fs');
// This must be listed in the package.json file for the main app
var request	= require('request');
var util    = require('util');

var routes = require('../src/js/data/routes');

var url = 'http://www.ftwcm.oneamerica.com/wps/contenthandler/wcmrest/Content/';

var getData = function(id, name) {
    if (id) {
        request.get({
	        followAllRedirects : true,
	        strictSSL          : false,
	        url                : url + id + '?mime-type=application/json'
        }, function(error, response, body) {
	        if (!error) {
                var dataFile;
		        util.log('Got data for ' + id + ', writing to file');
                dataFile = fs.createWriteStream('all-' + name + '.json');
                dataFile.on('error', function(err) {
                    util.log('Could not write file for ' + id);
                    response.end();
                });
                dataFile.write(body);
                dataFile.end();
            } else {
		        util.log('Error getting data for ' + id);
	        }
        });
    }
};

var current;
exec('rm -rf ./all-*.json');

Object.keys(routes).forEach(function(route) {
    current = routes[route];
    getData(current.WCMID, current.LINK);
});
