# Data

The `get-wcm-data.js` file here is used to pull down the most current data from WCM for use in the localData-dev repo. It's here versus in that repo because this repo contains the `src/js/data/routes.js` file, which is the master list of the needed WCM ids.

See that script for instructions on running it.
