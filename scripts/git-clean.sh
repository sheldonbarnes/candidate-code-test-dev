#!/bin/bash

# remove all non-tracked files except
# those passed with -e below
# basically set up for a clean build

git clean -dx -e checksum.txt -e node_modules
