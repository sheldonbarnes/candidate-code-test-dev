#!/bin/bash

status=0

# Runs all sets of tests
# Must be run from top level of project directory
# This task will dump a LOT of output to the console

# errors out on first failure

sh ./scripts/functional-tests.sh
if [ $? -ne 0 ]; then
    status=1
fi

sh ./scripts/unit-tests.sh
if [ $? -ne 0 ]; then
    status=1
fi

sh ./scripts/unit-browser-tests.sh
if [ $? -ne 0 ]; then
    status=1
fi

exit $status
