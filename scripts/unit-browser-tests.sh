#!/bin/bash

status=0

# Runs unit tests that require a browser
# Example - cookies, window.print, etc
# Must be run from top level of project directory

echo '--------------------------------------------------'
echo '--------- UNIT BROWSER TESTS BEGINNING------------'
echo '--------------------------------------------------'
echo '\n'

script=./scripts/preserve-reports.sh

# move existing reports
# source $script PREV

# Set up the code for being unit tested in the browser
gulp test-unit-test-setup 2>&1 &
wait

# Common parameters needed for the different OSes
# selpath=./bin/selenium-server-standalone-2.42.2.jar
# javawindows=-Dwebdriver.chrome.driver=./bin/chromedriver.exe

# start the selenium server as needed per the OS
# http://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux
# if [ "$(uname)" == "Darwin" ]; then
#     # mac
#     java -jar $selpath > selenium.log 2>&1 &
#     javaProcessFlag=$selpath
# elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
#     # windows
#     java -jar $selpath $javawindows > selenium.log 2>&1 &
#     # ps in git bash only shows we are using jdk
#     javaProcessFlag='Java/jdk.*/bin/java'
# fi

# start phantomjs directly using webdriver
phantomjs --webdriver=4444 > phantomjs.log 2>&1 &

# or can use chrome
# chromedriver --port=4444 --url-base=wd/hub > chromedriver.log 2>&1 &
# need to adjust the process detection below as well

# cheat, waiting to let the server spin up
sleep 1

./node_modules/.bin/intern-runner config=test/config/intern-unit-browser
if [ $? -ne 0 ]; then
    status=1
fi

# Cleanup the background processes
# Find all processes
# Grep for the process name
# Invert grep to remove the grep process that has that name too
# print the PID
kill `ps -ef | grep phantomjs | grep -v grep | awk '{print $2}'`
#kill `ps -ef | grep chromedriver | grep -v grep | awk '{print $2}'`

# rename our reports
# source $script unit-browser
# move existing reports back
# source $script PREV invert

echo '--------------------------------------------------'
echo '---------- UNIT BROWSER TESTS ENDING -------------'
echo '--------------------------------------------------'
echo '\n'

exit $status
