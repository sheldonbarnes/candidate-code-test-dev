#!/bin/bash

# Runs unit tests that require a browser
# Example - cookies, window.print, etc
# Must be run from top level of project directory

# Set up the code for being unit tested in the browser
# and wait for it to complete
gulp test-unit-test-setup 2>&1 &
wait

# kick off devserver for unit testing
# and wait for it to spin up
gulp devserver-unit-test-develop 2>&1 &

# arbitrary wait time to get past gulp messages
# might take longer than this
sleep 2

# The port below is defined in gulp/config/index.js
# as EXPRESS_PORT
echo 'Visit this URL to load and run tests:'
echo 'http://localhost:3010/node_modules/intern/client.html?config=../../../test/config/intern-unit-browser-develop'
read -p 'Press ENTER to kill the local server and clean up...'

# Cleanup the background processes
# Find all processes
# Grep for the process name
# Invert grep to remove the grep process that has that name too
# print the PID
kill `ps -ef | grep gulp | grep -v grep | awk '{print $2}'`
