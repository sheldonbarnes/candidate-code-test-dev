REM Inside node_modules, some paths are too long to delete using standard
REM windows commands, but this works using robocopy.
REM Documentation suggests that this should be available in all versions of
REM Windows since Windows 7, including Windows Server 2008, so should be
REM a safe bet to be present on machines that would consume this code.
REM
REM Original suggestion to use this was found at 
REM http://superuser.com/a/467814
REM
echo 'Deleting node_modules with robocopy, may take a while'
mkdir safe_to_delete
robocopy safe_to_delete ..\node_modules /S /MIR /LOG:robocopy.log
rmdir safe_to_delete
rmdir /S /Q ..\node_modules
del robocopy.log
echo 'Done deleting files with robocopy'
exit
