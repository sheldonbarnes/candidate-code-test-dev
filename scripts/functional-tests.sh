#!/bin/bash

status=0

# Runs functional tests only
# Must be run from top level of project directory
# This task will dump a LOT of output to the console

echo '--------------------------------------------------'
echo '---------- FUNCTIONAL TESTS BEGINNING ------------'
echo '--------------------------------------------------'

script=./scripts/preserve-reports.sh

# move existing reports
#source $script PREV

# Common parameters needed for the different OSes
# selpath=./bin/selenium-server-standalone-2.42.2.jar
# javawindows=-Dwebdriver.chrome.driver=./bin/chromedriver.exe

# Start the local dev server up
# Log standard output to a file
# Redirect error output to standard output (the file)
# And background the process
gulp dev > gulp.log 2>&1 &

# start the selenium server as needed per the OS
# http://stackoverflow.com/questions/3466166/how-to-check-if-running-in-cygwin-mac-or-linux
if [ "$(uname)" == "Darwin" ]; then
    # mac
    # java -jar $selpath > selenium.log 2>&1 &
    # javaProcessFlag=$selpath
    gulpProcessFlag='gulp'
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # windows
    # java -jar $selpath $javawindows > selenium.log 2>&1 &
    # ps in git bash only shows we are using jdk
    # javaProcessFlag='Java/jdk.*/bin/java'
    gulpProcessFlag='nodejs/node'
fi

# start phantomjs directly using webdriver
phantomjs --webdriver=4444 > phantomjs.log 2>&1 &

# or can use chrome
# chromedriver --port=4444 --url-base=wd/hub > chromedriver.log 2>&1 &
# need to adjust the process detection below as well

# cheat, waiting to let the servers spin up
sleep 1

./node_modules/.bin/intern-runner config=test/config/intern-functional
if [ $? -ne 0 ]; then
    status=1
fi

# Cleanup the background processes
# Find all processes
# Grep for the process name
# Invert grep to remove the grep process that has that name too
# print the PID
kill `ps -ef | grep $gulpProcessFlag | grep -v grep | awk '{print $2}'`
kill `ps -ef | grep phantomjs | grep -v grep | awk '{print $2}'`
# kill `ps -ef | grep chromedriver | grep -v grep | awk '{print $2}'`

# kill `ps -ef | grep $javaProcessFlag | grep -v grep | awk '{print $2}'`

# rename our reports
# source $script functional
# move existing reports back
# source $script PREV invert

echo '--------------------------------------------------'
echo '------------ FUNCTIONAL TESTS ENDING -------------'
echo '--------------------------------------------------'

exit $status
