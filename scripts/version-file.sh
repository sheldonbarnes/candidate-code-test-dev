#!/bin/bash

# write out the results of git-describe to a version.html file
# this file will be included in the bundled results

echo '<html><head><title>Version of Employee Benefits Frontend</title></head><body>' > release/version.html
echo '<p>employeebenefits-frontend</p><p>' >> release/version.html
echo `git describe --long` >> release/version.html
echo '</p></body></html>' >> release/version.html

