var config = require('../config');
var gulp   = require('gulp');
var path   = require('path');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('uglify-scripts-release-app', function() {
    return gulp.src(config.APP_DIST_BUILT)
               .pipe(uglify({
                   // uncomment to get unminifed version
                   // compress: false,
                   // output: { beautify: true },
                   // mangle: false
               }))
               .pipe(rename(path.join(config.APP_START_MIN_FILE)))
               .pipe(gulp.dest(config.APP_RELEASE_JS));
});

gulp.task('uglify-scripts-release-vendor', function() {
    return gulp.src(config.APP_DIST_VENDOR_BUILT)
               .pipe(uglify({
                   // uncomment to get unminifed version
                   // compress: false,
                   // output: { beautify: true },
                   // mangle: false
               }))
               .pipe(rename(path.join(config.VENDOR_START_MIN_FILE)))
               .pipe(gulp.dest(config.APP_RELEASE_JS));
});
