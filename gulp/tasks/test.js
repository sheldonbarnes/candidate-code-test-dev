// https://github.com/greypants/gulp-starter/
var gulp = require('gulp');
// We are using this plugin to allow the entire set of tasks
// to be defined in a single location vs having to trace a
// chain of tasks throughout multiple files/tasks.
var rs   = require('run-sequence');

// Browserify files for using in unit tests
// And serve local data for testing REST
gulp.task('test-unit-test-setup', function() {
    rs(
        [ 'clean-test', 'lint-test' ],
        [ 'browserify-app-modules', 'browserify-test-utils', 'browserify-vendor-unit-test' ]
    );
});
