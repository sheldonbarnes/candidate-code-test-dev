// Copy tasks
var config = require('../config');
var gulp   = require('gulp');

// DEV TASKS
gulp.task('copy-css', function() {
    // Only the IE stylesheet should be copied manually
    // Others are done via LESS
    return gulp.src(config.APP_SRC_CSS + '/ie.css')
        .pipe(gulp.dest(config.APP_DIST_CSS));
});

gulp.task('copy-fonts', function() {
    return gulp.src(config.APP_SRC_FONTS + '/**/*')
        .pipe(gulp.dest(config.APP_DIST_FONTS));
});

gulp.task('copy-images', function() {
    return gulp.src([
        config.APP_SRC_IMG + '/**/*',
        config.APP_SRC + '/apps/**/images/**/*',
        config.APP_ROOT + '/node_modules/datatables/media/images/**/*.png'
    ]).pipe(gulp.dest(config.APP_DIST_IMG));
});

gulp.task('copy-images-siteminder', function() {
    return gulp.src([
        config.APP_SRC + '/ui/img/blank.gif',
    ]).pipe(gulp.dest(config.APP_DIST + '/public/ui/img'));
});

gulp.task('copy-submodule-images', function() {
    return gulp.src(config.SUBMODULE_IMAGES + '/**/*')
        .pipe(gulp.dest(config.APP_DIST_IMG));
});

gulp.task('copy-submodule-fonts', function() {
    return gulp.src(config.SUBMODULE_FONTS + '/**/*')
        .pipe(gulp.dest(config.APP_DIST_FONTS));
});

// RELEASE TASKS
gulp.task('copy-css-release', function() {
    // Only the IE stylesheet should be copied manually
    // Others are done via LESS
    return gulp.src(config.APP_DIST_CSS + '/ie.css')
        .pipe(gulp.dest(config.APP_RELEASE_CSS));
});

gulp.task('copy-images-release', function() {
    return gulp.src(config.APP_DIST_IMG + '/**/*')
        .pipe(gulp.dest(config.APP_RELEASE_IMG));
});

gulp.task('copy-images-siteminder-release', function() {
    return gulp.src([
        config.APP_DIST + '/public/ui/img/blank.gif',
    ]).pipe(gulp.dest(config.APP_RELEASE + '/public/ui/img'));
});

gulp.task('copy-fonts-release', function() {
    return gulp.src(config.APP_DIST_FONTS + '/**/*')
        .pipe(gulp.dest(config.APP_RELEASE_FONTS));
});
