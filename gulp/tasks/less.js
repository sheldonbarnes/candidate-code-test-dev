// Builds the LESS files into CSS files.
// Normally, you should NOT edit the css files directly.
// Edit the appropriate LESS module.

// https://github.com/greypants/gulp-starter/
var config = require('../config');
var gulp   = require('gulp');
var less   = require('gulp-less');
var path   = require('path');

var files = [
    config.APP_SRC_LESS + '/base.less',
    config.APP_SRC_LESS + '/oso.less',
    config.APP_SRC_LESS + '/oso2.less',
    config.APP_SRC_LESS + '/app.less'
];

gulp.task('less', function () {
    return gulp.src(files)
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest(config.APP_DIST_CSS));
});
