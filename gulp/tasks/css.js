// https://github.com/greypants/gulp-starter/
var config    = require('../config');
var gulp      = require('gulp');
var minifyCSS = require('gulp-minify-css');
var path      = require('path');
var rename    = require('gulp-rename');

gulp.task('css-minify-css-prod', function () {
    return gulp.src(config.APP_DIST_CSS + '/**/*.css')
               .pipe(minifyCSS({
                   // set to 1 to keep all
                   keepSpecialComments: 0,
                   // set to true to preserve for readability
                   keepBreaks: false,
                   // set to true to turn off merging, etc
                   // since this will modify the generated css
                   // it's possible there could be subtle bugs
                   // here
                   // turn off if things get odd
                   noAdvanced: false
               }))
               .pipe(rename(function (path) {
                   path.basename += ".min";
               }))
               .pipe(gulp.dest(config.APP_DIST_CSS));
});

gulp.task('css-minify-css-release', function () {
    return gulp.src(config.APP_DIST_CSS + '/**/*.css')
               .pipe(minifyCSS({
                   keepSpecialComments : 0,
                   keepBreaks          : false,
                   noAdvanced          : false
               }))
               .pipe(rename(function (path) {
                   path.basename += ".min";
               }))
               .pipe(gulp.dest(config.APP_RELEASE_CSS));
});
