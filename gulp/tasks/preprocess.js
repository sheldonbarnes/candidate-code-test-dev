// Allows writing out files with environment-specific code
// depending on the target.
// Ex - when running preprocess-dev below, this chunk of HTML
//
// <!-- @ifdef DEV -->
// <link rel="stylesheet" href="css/all.css" media="screen" />
// <!-- @endif -->
// <!-- @ifdef PROD -->
// <link rel="stylesheet" href="css/all.min.css" />
// <!-- @endif -->
//
// would render as
// <link rel="stylesheet" href="css/all.css" media="screen" />

// IMPORTANT!!!
// This task is ONLY for processing the HTML
// The JS preprocessing is done as part of browserify

var config     = require('../config');
var gulp       = require('gulp');
var path       = require('path');
var preprocess = require('gulp-preprocess');

var context = {
    dev: {
        DEV: true
    },
    prod: {
        PROD: true
    }
};

var fileList = [
    config.APP_SRC + '/*.html',
    // We have a single .faces file used to handle redirects
    config.APP_SRC + '/*.faces'
];

var task = function(target, location) {
    return gulp.src(fileList)
               .pipe(preprocess({
                   context: context[target]
               }))
               .pipe(gulp.dest(location));
};

// DEV
gulp.task('preprocess-dev', [ 'preprocess-dev-secure'], function() {
    return task('dev', config.APP_DIST_PUBLIC);
});

gulp.task('preprocess-dev-secure', function() {
    return task('dev', config.APP_DIST_SECURE);
});

// PROD
gulp.task('preprocess-prod', [ 'preprocess-prod-secure'], function() {
    return task('prod', config.APP_DIST_PUBLIC);
});

gulp.task('preprocess-prod-secure', function() {
    return task('prod', config.APP_DIST_SECURE);
});

// RELEASE
gulp.task('preprocess-release', [ 'preprocess-release-secure'], function() {
    return task('prod', config.APP_RELEASE_PUBLIC);
});

gulp.task('preprocess-release-secure', function() {
    return task('prod', config.APP_RELEASE_SECURE);
});
