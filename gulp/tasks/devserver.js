// Start a local webserver.
var config  = require('../config');
var cors    = require('../util/cors');
var express = require('express');
var gulp    = require('gulp');
var fs      = require('fs');
var path    = require('path');
var request = require('request');
var util    = require('util');

var siteminderPing =  '/secure/servlet';

var restUrl = [
    '/employeebenefits/oasec/public/rest',
    '/employeebenefits/oasec/public',
    '/employeebenefits/oasec/secure/rest',
    '/employeebenefits/secure/rest',
    '/wps',
    siteminderPing
].join('|');

var dataHost = config.DEV_HOST;
var proxy = config.DEV_LOCALDATA_PROXY;

var handleError = function(event) {
    util.log('Error in getting data from local server');
};

// normal server runs on the config.DEV_EXPRESS_PORT
// must run the localData-dev repo as well to serve
// local data
gulp.task('devserver-localdata', function() {

    var app	= module.exports = express();

    app.use(cors);
    // serve static files by default
    // in this case, this is our bundle
    app.use(express.static(config.APP_DIST));

    // main page of app
    // on any request, if it's for REST data
    // we proxy against the real env and return data
    // Proxies for the REST data
    app.all(restUrl + '/*', function(req, res) {
        
        // If this was the siteminder check, just give back a 20x or 30x
        if (req.url.indexOf(siteminderPing) !== -1) {
            // Uncomment to test redirects
            // res.set({
            //     'Content-Type': 'text/html',
            //     'Location': '/secure/logout.html'
            // });
            // res.status(302).end();

            // Comment out if you uncomment the above piece
            res.status(200).end();

        } else {

            var proxyUrl = req.url.split(restUrl)[1];

            var newReqUrl = proxy + req.url;

            // util.log("Proxying '" + req.url + "' to '" + newReqUrl + "'");

            var newReq;
            if (req.method === 'POST') {
                newReq = request.post({uri: newReqUrl, json: req.body});
            } else if (req.method === 'PUT') {
                newReq = request.put({uri: newReqUrl, json: req.body});
            } else {
                newReq = request(newReqUrl);
            }
            req.pipe(newReq)
                .on('error', function(e) { handleError(); })
                .pipe(res)
                .on('error', function(e) { handleError(); });

        }
    });

    app.listen(config.DEV_EXPRESS_PORT);

});

// normal server runs on the config.DEV_EXPRESS_PORT
// must run the localData-dev repo as well to serve
// local data
gulp.task('devserver-prod', function() {

    var app	= module.exports = express();
    var restUrl = '/secure/rest/employeebenefits';
    var dataHost = 'http://localhost:5000';
    app.use(cors);
    // serve static files by default
    // in this case, this is our bundle
    app.use(express.static(config.APP_RELEASE));

    // main page of app
    // on any request, if it's for REST data
    // we proxy against the real env and return data
    app.all(restUrl + '/*', function(req, res) {
        var proxyUrl = req.url.split(restUrl)[1];

        var newReqUrl = proxy + req.url;

        // util.log("Proxying '" + req.url + "' to '" + newReqUrl + "'");

        var newReq;
        if (req.method === 'POST') {
            newReq = request.post({uri: newReqUrl, json: req.body});
        } else if (req.method === 'PUT') {
            newReq = request.put({uri: newReqUrl, json: req.body});
        } else {
            newReq = request(newReqUrl);
        }
        req.pipe(newReq).pipe(res);
    });

    app.listen(config.DEV_EXPRESS_PORT);

});

// Local server for developing unit tests
gulp.task('devserver-unit-test-develop', function() {

    var app	= module.exports = express();
    app.use(express.static(config.APP_ROOT));
    app.listen(config.DEV_EXPRESS_PORT);

});
