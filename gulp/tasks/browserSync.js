// https://github.com/greypants/gulp-starter/

// Automatically keeps any browsers that are open to the
// local development environement in sync using socket.io.
// The task of `bs-reload` is triggered as needed in other
// tasks.

var browserSync = require('browser-sync');
var config      = require('../config');
var gulp        = require('gulp');

gulp.task('browserSync', function() {
    return browserSync({
        notify: false,
        open: false,
        port: config.DEV_PORT_START,
        // the proxy handles the static files in the APP_DIST
        // directory - they are bundled by gulp
        proxy: config.DEV_HOST + ':' + config.DEV_EXPRESS_PORT,
        // injecting in the head makes for weird bugs
        snippetOptions: {
            // Provide a custom Regex for inserting the snippet.
            rule: {
                match: /<\/body>/i,
                fn: function (snippet, match) {
                    return snippet + match;
                }
            }
        }
    });
});

// Reload all Browsers
gulp.task('bs-reload', function(){
    browserSync.reload();
});
