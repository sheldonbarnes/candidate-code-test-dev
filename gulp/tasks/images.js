// https://github.com/greypants/gulp-starter/
// Minifies images for less bytes across the wire.

// Not currently calling this task anywhere, but leaving it in place in case there
// is utility in using it.

var changed    = require('gulp-changed');
var config     = require('../config');
var gulp       = require('gulp');

// minify images
gulp.task('images', function() {
    return gulp.src(config.APP_SRC_IMG + '/**/*')
        .pipe(changed(config.APP_DIST_IMG))
        .pipe(gulp.dest(config.APP_DIST_IMG));
});
