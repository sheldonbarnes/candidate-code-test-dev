// https://github.com/greypants/gulp-starter/
var gulp = require('gulp');
// We are using this plugin to allow the entire set of tasks
// to be defined in a single location vs having to trace a
// chain of tasks throughout multiple files/tasks.
var rs   = require('run-sequence');


// This is the normal task to be run when doing local development.
// It starts a local server on the port defined as DEV_PORT_START
// in gulp/config/index.js, running on the host defined there as well
// (normally localhost).
// Changes to any html, images, js files, or less files
// will automatically trigger a build of the files into
// `dist` and will also refresh the browser.
// Proxies to apiary.io for RESTful data
gulp.task('default', function() {
    // These tasks will run in the order given.
    // Entries that are arrays will run async
    // relative to each other, but all the
    // items in the array will complete before
    // the next entry in the list starts.
    // Ex - `lint` will run after `clean-dist`
    // runs, and before the 3 tasks listed in the
    // next line. Those three tasks will start
    // at the same time, but may finish in any order.
    // But all three tasks will finish before
    // `browserify-dev` begins.
    rs(
        'clean-dist',
        'lint',
        [ 'less', 'preprocess-dev' ],
        [ 'copy-css', 'copy-images', 'copy-fonts', 'copy-submodule-images', 'copy-submodule-fonts' ],
        'browserify-dev',
        'devserver-localdata',
        'watch-dev'
    );
});

// An alias for the default task show above.
gulp.task('dev', [ 'default' ]);

// INCOMPLETE
// uses http://localhost:5000 for data
// No server running on that port currently
gulp.task('dev-apiary', function() {
    rs(
        'clean-dist',
        'lint',
        [ 'less', 'preprocess-dev' ],
        'browserify-dev',
        'devserver-apiary',
        'watch-dev'
    );
});

// Use this when code-complete, but you want to test
// the minified versions of your JS and CSS.
// It uses built and minified files and updates the
// html files using preprocess and the 'PROD' target
gulp.task('prodcheck', function() {
    rs(
       'devserver-prod',
       'watch-prod'
    );
});
