/* global err */

// https://github.com/greypants/gulp-starter/

// Browserify bundles CommonJS modules into a format
// usable by browsers. A single file (defined in the gulp config)
// is created for us in the browser. Sourcemaps can show the browser
// the individual files for easier debugging.

// To debug, uncomment the line below before running the gulp task
// process.env.BROWSERIFYSHIM_DIAGNOSTICS=1

var _            = require('underscore');
var browserify   = require('browserify');
var bundleLogger = require('../util/bundleLogger');
var config       = require('../config');
var foreach      = require('gulp-foreach');
var fs           = require('fs');
var gulp         = require('gulp');
var hbsfy        = require('hbsfy');
var istanbul     = require('browserify-istanbul');
var handleErrors = require('../util/handleErrors');
var path         = require('path');
var preprocessify = require('preprocessify');
var source       = require('vinyl-source-stream');
var watchify     = require('watchify');

var libs = [
    'backbone',
    'backbone-fetch-cache',
    'backbone.marionette',
    'brightcove',
    'has',
    'jquery',
    'spin',
    'underscore',
    'vendor',
    'webtrends'
];

// Normally, you may not want sourcemaps when doing local
// development, but they are turned on here.
// Usually this is the only difference between dev and prod
// for this task.
var baseConfig = {
    // excludes will prevent libraries from being bundled
    excludes   : true,
    input      : [ config.APP_START ],
    outputName : config.APP_BUILT_FILE,
    outputDir  : config.APP_DIST_JS,
    sourcemaps : true
};

// dev: true will trigger rebuilds
var devConfig = _.extend({}, baseConfig, { dev: true });

// used to make a bundle for all library/framework pieces
// won't change nearly as much, will reduce bundle time for other files
var vendorConfig = {
    dev        : false,
    exclude    : false,
    input      : [ config.VENDOR_START ],
    // use this for modules that have NO require statements
    // might be more that can be added here, FYI
    noParse    : [ 'jquery', 'spin', 'has' ],
    outputName : config.VENDOR_BUILT_FILE,
    outputDir  : config.APP_DIST_JS,
    sourcemaps : true,
    standalone : 'vendor'
};

var vendorConfigUnitTest        = _.clone(vendorConfig);
vendorConfigUnitTest.outputDir  = config.APP_TEST_DIST;
vendorConfigUnitTest.sourcemaps = false;

// All the places to look for modules when we require them
// this lets us do this for a model: require('some-m')
// instead of this: require('models/some-m')
// do NOT use ./, this can cause an error on windows systems
var moduleSuffixes = [
    '',
    '/collections',
    '/config',
    '/helpers',
    '/logic',
    '/models',
    '/templates',
    '/views'
];

// Normal paths for modules
var modulePaths = (function() {
    return moduleSuffixes.map(function(suffix) {
        return path.join(config.APP_SRC_JS, suffix);
    });
})();

// used below for repeated tasks
// run like
// baseTask({ entries: [ filename ]})
var baseTask = function(taskConfig) {

    var bundle;
    var bundler;

    bundler = browserify({
        // Specify the entry point of your app
        debug: taskConfig.sourcemaps,
        entries: taskConfig.input,
        // Add file extensions to make optional in your requires
        extensions: ['.hbs'],
        paths: modulePaths,
        standalone: taskConfig.standalone,
        // Required watchify args
        cache: {},
        packageCache: {},
        fullPaths: true
    });

    // exclude any vendor files
    if (taskConfig.excludes === true ) {
        libs.forEach(function(lib) {
            bundler.exclude(lib);
        });
    }

    // bundle templates
    bundler.transform(hbsfy);

    // js preprocessing
    if (taskConfig.dev === true) {
        bundler.transform(preprocessify({ DEV: true }));
    } else {
        bundler.transform(preprocessify({ PROD: true }));
    }

    bundle = function() {

        bundleLogger.start();

        return bundler.bundle()
                      .on('error', handleErrors)
                      .pipe(source(taskConfig.outputName))
                      .pipe(gulp.dest(taskConfig.outputDir))
                      .on('end', bundleLogger.end);
    };

    // Rebundle with watchify on changes when running in dev mode
    if (taskConfig.dev === true) {
        bundler = watchify(bundler);
        bundler.on('update', bundle);
    }

    return bundle();
};

// Build modules for unit testing
// This task browserifies each individual file
// so that the unit tests can consume them as needed.
//
// Each file will be built as a completely self contained module, with all
// needed dependencies in place.
//
// Based on some ideas found here:
// http://justinjohnson.org/javascript/getting-started-with-gulp-and-browserify
//
gulp.task('browserify-app-modules', function() {

    var singleFileBundler = function(stream, file) {

        var relativePath = file.path.split(config.APP_SRC + path.sep)[1];

        if (relativePath) {
            // this will fail if you dot-namespace a file
            // ex someobj.somefile.js
            var fileName = relativePath.split(path.sep).pop().split('.').shift();
            var configObj = {
                debug      : true,
                extensions : ['.hbs'],
                paths      : modulePaths,
                standalone : fileName
            };

            var bundler = browserify(file.path, configObj);

            // do not pull in setup files
            libs.forEach(function(lib) {
                bundler.exclude(lib);
            });

            bundler.transform(istanbul({
                // https://github.com/theintern/intern/issues/329
                instrumenterConfig: {
                    coverageVariable: '__internCoverage'
                },
                // these are in addition to the defaults of the module
                ignore: [
                    '**/vendor*',
                    '**/src/lib/**',
                    '**/*.hbs',
                    '**/test/**'
                ]
            }));

            if (fileName !== 'setup') {
                // do not pull in setup files
                libs.forEach(function(lib) {
                    bundler.exclude(lib);
                });
            }

            bundler.transform(hbsfy);

            // js preprocessing
            bundler.transform(preprocessify({ DEV: true }));

            return bundler.bundle()
                          .pipe(source(relativePath))
                          .pipe(gulp.dest(config.APP_TEST_DIST));
        }

        return true;

    }; // singleFileBuilder

    return gulp.src([config.APP_SRC + '/**/*.js', '!'+config.APP_SRC +'/lib/**'])
               .pipe(foreach(singleFileBundler));

});

gulp.task('browserify-test-utils', function() {

    var singleFileBundler = function(stream, file) {

        var relativePath = file.path.split(config.APP_TEST + path.sep)[1];

        if (relativePath) {
            // this will fail if you dot-namespace a file
            // ex someobj.somefile.js
            var fileName = relativePath.split(path.sep).pop().split('.').shift();
            var configObj = {
                debug      : true,
                paths      : modulePaths,
                standalone : fileName
            };

            var bundler = browserify(file.path, configObj);

            // do not pull in setup files
            libs.forEach(function(lib) {
                bundler.exclude(lib);
            });

            bundler.transform(istanbul({
                // https://github.com/theintern/intern/issues/329
                instrumenterConfig: {
                    coverageVariable: '__internCoverage'
                },
                // these are in addition to the defaults of the module
                ignore: [
                    '**/vendor*',
                    '**/src/lib/**',
                    '**/*.hbs',
                    '**/test/**'
                ]
            }));
            bundler.transform(hbsfy);

            // js preprocessing
            bundler.transform(preprocessify({ DEV: true }));

            return bundler.bundle()
                          .pipe(source(relativePath))
                          .pipe(gulp.dest(config.APP_TEST_DIST));
        }

        return true;

    }; // singleFileBuilder

    return gulp.src([
        config.APP_TEST + '/unit/unit-utils.js',
        config.APP_TEST + '/unit-browser/unit-browser-utils.js'
    ])
               .pipe(foreach(singleFileBundler));

});

gulp.task('browserify-vendor', function() {
    return baseTask(vendorConfig);
});

gulp.task('browserify-vendor-unit-test', function() {
    return baseTask(vendorConfigUnitTest);
});

gulp.task('browserify-dev', [ 'browserify-vendor'], function() {
    return baseTask(devConfig);
});

gulp.task('browserify-prod', [ 'browserify-vendor' ], function() {
    return baseTask(baseConfig);
});

gulp.task('browserify-release', [ 'browserify-vendor' ], function() {
    return baseTask(baseConfig);
});
