// Removes all files in the specified location.
// Normally runs before starting up.
// You should NOT clean when watching for file changes,
// becaus we only rebuild what has changed.

var config = require('../config');
var del    = require('del');
var gulp   = require('gulp');

var glob = '/**/*';
var keep = '!/**/*.gitkeep';
var artifact  = '!/**/*.zip';

gulp.task('clean-dist', function () {
    return del([
        config.APP_DIST + glob,
        keep
    ]);
});

gulp.task('clean-release', function () {
    return del([
        config.APP_RELEASE + glob,
        keep
    ]);
});

// alias
gulp.task('clean-prod', [ 'clean-release' ]);

// only use this if you want to keep JUST the zip file in release
gulp.task('clean-release-keep-artifact', function () {
    return del([
        config.APP_RELEASE + glob,
        artifact
    ]);
});

gulp.task('clean-test', function () {
    return del([
        config.APP_TEST_DIST,
        config.APP_TEST + '/unit-browser/specs/dist',
        config.APP_TEST_DIST_EXTERNAL,
        config.APP_REPORT,
        keep
    ]);
});
