// https://github.com/greypants/gulp-starter/
var gulp = require('gulp');
// We are using this plugin to allow the entire set of tasks
// to be defined in a single location vs having to trace a
// chain of tasks throughout multiple files/tasks.
var rs   = require('run-sequence');

// Builds files into `release`.
// Add tasks here as needed that will generate the
// needed code.
gulp.task('build-release', function() {
    rs(
        // some of the tasks here build files into dist/, then process
        // them into release/
        [ 'clean-dist', 'clean-release' ],
        [ 'lint-release' ],
        // gradle does not like this task being part of others
        'preprocess-release',
        [
            'copy-css',
            'copy-images',
            'copy-images-siteminder',
            'copy-fonts',
            'copy-submodule-images',
            'copy-submodule-fonts'
        ],
        [
            'copy-css-release',
            'copy-images-release',
            'copy-images-siteminder-release',
            'copy-fonts-release'
        ],
        'less',
        [ 'css-minify-css-release', 'browserify-release' ],
        [ 'uglify-scripts-release-app', 'uglify-scripts-release-vendor' ]
    );
});

gulp.task('build-release-archive', function() {
    rs(
        'zip-release'
    );

});
