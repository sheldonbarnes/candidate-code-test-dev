// https://github.com/greypants/gulp-starter/
var config = require('../config');
var gulp   = require('gulp');
var p      = require('../../package.json');
var zip    = require('gulp-zip');

gulp.task('zip-release', function () {
    return gulp.src(config.APP_RELEASE + '/**/*', { base: config.APP_RELEASE })
        .pipe(zip(p.name + '.zip'))
        .pipe(gulp.dest(config.APP_RELEASE));
});
