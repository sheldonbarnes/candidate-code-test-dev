// Watches for file changes and triggers other gulp tasks
// as needed.
// This explicity ONLY watches files that are not browserified, that task
// watches those files already. IE, anything that browserify builds, it will
// watch for changes in and rebundle already.

// https://github.com/greypants/gulp-starter/
var config = require('../config');
var gulp   = require('gulp');
var rs     = require('run-sequence');

// files we always watch
var commonWatch = [
     config.APP_SRC      + '/**/*.html',
     config.APP_SRC_IMG  + '/**/*',
     config.APP_SRC_LESS + '/**/*.less',
     config.APP_SRC      + '/**/*.hbs',
     config.APP_SRC      + '/**/apps/**/templates/*.hbs'
];

// Normal development, look for files changes.
// Then places built files in the right places.
gulp.task('watch-dev', [
    'browserSync'
], function() {
    gulp.watch(commonWatch, [
        'lint',
        [ 'less', 'preprocess-dev' ],
        'bs-reload'
    ]);
});

// Same as above, but also minify the files to test as in prod.
gulp.task('watch-prod', [
    'browserSync'
], function() {
    gulp.watch(commonWatch, function() {
        rs(
            [ 'less', 'lint-prod', 'preprocess-prod' ],
            [ 'minify-css-prod' ],
            'bs-reload'
        );
    });
});
