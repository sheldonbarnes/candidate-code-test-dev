var allowCORS = function(req, res, next) {
	res.header('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, X-File-Name, Content-Type, Cache-Control');
	res.header('Access-Control-Allow-Method', 'POST, GET, PUT, DELETE, OPTIONS');
	res.header('Access-Control-Allow-Origin', req.headers.origin);
	res.header('Access-Control-Allow-Credentials', 'true');
	if( 'OPTIONS' === req.method ) {
		res.header('Content-Type','application/json');
		res.send( 203, JSON.stringify({ msg: 'OK' }));
		res.end();
	} else {
		next();
	}
};

module.exports = allowCORS;
