// https://github.com/greypants/gulp-starter/
// This file exports an global CONFIG object.
// Any properties added below dealing with
// paths are relative to THIS file location.
// ALL configuration for gulp tasks should live
// in this file.
var path = require('path');

var DEV_EXPRESS_PORT    = 3010;
var DEV_HOST            = 'localhost';
var DEV_PORT_START      = 3000;
var DEV_PORT_END        = 3005;
var DEV_LOCALDATA_PROXY = 'http://localhost:5000';

var _CONFIG    = 'config';
var _CSS       = 'css';
var _DIST      = 'dist';
var _FONTS     = 'fonts';
var _IMG       = 'images';
var _JS        = 'js';
var _LESS      = 'less';
var _REPORT    = 'html-report';
var _TEMPLATES = 'templates';
var _TEST      = 'test';

var JS_CONFIG_FILE = 'config.js';

var APP_START_FILE     = 'app.js';
var APP_BUILT_FILE     = 'app.built.js';
var APP_START_MIN_FILE = 'app.min.js';

var VENDOR_START_FILE     = 'vendor.js';
var VENDOR_BUILT_FILE     = 'vendor.built.js';
var VENDOR_START_MIN_FILE = 'vendor.min.js';

// these get reused in the export

var APP_ROOT                = path.join(__dirname, '../../');
var APP_GULP                = path.join(APP_ROOT, 'gulp');
var APP_SRC                 = path.join(APP_ROOT, 'src');

var APP_DIST                = path.join(APP_ROOT, _DIST);
var APP_DIST_PUBLIC         = path.join(APP_DIST, 'public');
var APP_DIST_SECURE         = path.join(APP_DIST, 'secure');

var APP_RELEASE             = path.join(APP_ROOT, 'release');
var APP_RELEASE_PUBLIC      = path.join(APP_RELEASE, 'public');
var APP_RELEASE_SECURE      = path.join(APP_RELEASE, 'secure');

var APP_SRC_CSS             = path.join(APP_SRC, _CSS);
var APP_SRC_FONTS           = path.join(APP_SRC, _FONTS);
var APP_SRC_IMG             = path.join(APP_SRC, _IMG);
var APP_SRC_JS              = path.join(APP_SRC, _JS);
var APP_SRC_JS_TEMPLATES    = path.join(APP_SRC_JS, _TEMPLATES);
var APP_SRC_LESS            = path.join(APP_SRC_CSS, _LESS);

var APP_SRC_CONFIG          = path.join(APP_SRC_JS, _CONFIG);
var APP_SRC_CONFIG_FILE     = path.join(APP_SRC_CONFIG, JS_CONFIG_FILE);

var APP_DIST_CSS            = path.join(APP_DIST, _CSS);
var APP_DIST_FONTS          = path.join(APP_DIST, _FONTS);
var APP_DIST_IMG            = path.join(APP_DIST, _IMG);
var APP_DIST_JS             = path.join(APP_DIST, _JS);
var APP_DIST_CONFIG         = path.join(APP_DIST_JS, _CONFIG);
var APP_DIST_CONFIG_FILE    = path.join(APP_DIST_CONFIG, JS_CONFIG_FILE);
var APP_DIST_BUILT          = path.join(APP_DIST_JS, APP_BUILT_FILE);
var APP_DIST_VENDOR_BUILT   = path.join(APP_DIST_JS, VENDOR_BUILT_FILE);

var APP_RELEASE_CSS         = path.join(APP_RELEASE, _CSS);
var APP_RELEASE_FONTS       = path.join(APP_RELEASE, _FONTS);
var APP_RELEASE_IMG         = path.join(APP_RELEASE, _IMG);
var APP_RELEASE_JS          = path.join(APP_RELEASE, _JS);

var APP_REPORT              = path.join(APP_ROOT, _REPORT);

var APP_START               = path.join(APP_SRC_JS, APP_START_FILE);

var APP_TEST                = path.join(APP_ROOT, _TEST);
var APP_TEST_DIST           = path.join(APP_TEST, _DIST);
var APP_TEST_DIST_EXTERNAL  = path.join(APP_TEST, _DIST + '-external');

var SUBMODULE_ASSETS        = path.join(APP_ROOT, 'submodules/oso-design/Development/Clickable-Prototype/assets');
var SUBMODULE_IMAGES        = path.join(SUBMODULE_ASSETS, 'images');
var SUBMODULE_FONTS        = path.join(SUBMODULE_ASSETS, 'fonts');

var VENDOR_START            = path.join(APP_SRC_JS, VENDOR_START_FILE);


module.exports = {

    _CONFIG                 : _CONFIG,
    _CSS                    : _CSS,
    _DIST                   : _DIST,
    _FONTS                  : _FONTS,
    _IMG                    : _IMG,
    _JS                     : _JS,
    _LESS                   : _LESS,
    _REPORT                 : _REPORT,
    _TEMPLATES              : _TEMPLATES,
    _TEST                   : _TEST,

    APP_BUILT_FILE          : APP_BUILT_FILE,

    APP_DIST                : APP_DIST,
    APP_DIST_PUBLIC         : APP_DIST_PUBLIC,
    APP_DIST_SECURE         : APP_DIST_SECURE,

    APP_DIST_CONFIG_FILE    : APP_DIST_CONFIG_FILE,
    APP_DIST_CSS            : APP_DIST_CSS,
    APP_DIST_FONTS          : APP_DIST_FONTS,
    APP_DIST_IMG            : APP_DIST_IMG,
    APP_DIST_JS             : APP_DIST_JS,
    APP_DIST_BUILT          : APP_DIST_BUILT,
    APP_DIST_VENDOR_BUILT   : APP_DIST_VENDOR_BUILT,

    APP_GULP                : APP_GULP,

    APP_RELEASE             : APP_RELEASE,
    APP_RELEASE_PUBLIC      : APP_RELEASE_PUBLIC,
    APP_RELEASE_SECURE      : APP_RELEASE_SECURE,

    APP_RELEASE_CSS         : APP_RELEASE_CSS,
    APP_RELEASE_FONTS       : APP_RELEASE_FONTS,
    APP_RELEASE_IMG         : APP_RELEASE_IMG,
    APP_RELEASE_JS          : APP_RELEASE_JS,

    APP_REPORT              : APP_REPORT,

    APP_ROOT                : APP_ROOT,

    APP_SRC                 : APP_SRC,
    APP_SRC_CSS             : APP_SRC_CSS,
    APP_SRC_CONFIG_FILE     : APP_SRC_CONFIG_FILE,
    APP_SRC_FONTS           : APP_SRC_FONTS,
    APP_SRC_IMG             : APP_SRC_IMG,
    APP_SRC_JS              : APP_SRC_JS,
    APP_SRC_LESS            : APP_SRC_LESS,
    APP_SRC_JS_TEMPLATES    : APP_SRC_JS_TEMPLATES,

    APP_START               : APP_START,
    APP_START_FILE          : APP_START_FILE,
    APP_START_MIN_FILE      : APP_START_MIN_FILE,

    APP_TEST                : APP_TEST,
    APP_TEST_DIST           : APP_TEST_DIST,
    APP_TEST_DIST_EXTERNAL  : APP_TEST_DIST_EXTERNAL,

    DEV_EXPRESS_PORT        : DEV_EXPRESS_PORT,
    DEV_HOST                : DEV_HOST,
    DEV_PORT_START          : DEV_PORT_START,
    DEV_PORT_END            : DEV_PORT_END,
    DEV_LOCALDATA_PROXY     : DEV_LOCALDATA_PROXY,

    SUBMODULE_ASSETS        : SUBMODULE_ASSETS,
    SUBMODULE_IMAGES        : SUBMODULE_IMAGES,
    SUBMODULE_FONTS         : SUBMODULE_FONTS,

    VENDOR_BUILT_FILE       : VENDOR_BUILT_FILE,

    VENDOR_START            : VENDOR_START,
    VENDOR_START_FILE       : VENDOR_START_FILE,
    VENDOR_START_MIN_FILE   : VENDOR_START_MIN_FILE

};
