// Configuration for running unit tests with intern
// This config is for running automated unit testing
// where a browser is needed.

define([
    './intern-base-config'
], function(
    baseConfig
) {

    // these paths are from the spec file itself
    baseConfig.loader.paths = {
        // used for getting built modules themselves
        // ../../../ should resolve as APP_ROOT
        dist: '../../../test/dist'
    };

    baseConfig.suites = [
        'test/unit-browser/specs/contentparser-spec',
        'test/unit-browser/specs/utils-spec',
        'test/unit-browser/specs/config-spec'
    ];

    return baseConfig;

});
