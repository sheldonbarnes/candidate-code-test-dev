// Configuration for running unit tests with intern
// This config is for running automated unit testing
// where a browser is needed.

define([
    './intern-unit-browser'
], function(
    browserConfig
) {

    browserConfig.reporters = [
        'console',
        // CANNOT use the lcovhtml reporter when running in browser directly
        'html'
    ];

    // suites are defined on the command line for running
    // in develop mode

    return browserConfig;

});
