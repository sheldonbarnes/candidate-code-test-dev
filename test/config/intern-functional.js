// Configuration for running functional tests with intern

define([
    './intern-base-config'
], function(
    baseConfig
) {

	baseConfig.functionalSuites = [

    ];

    // This is nulled out in order to NOT overwrite existing intern
    // mappings that are used internally by intern
    // https://github.com/theintern/intern/issues/348
    baseConfig.loader.map = null;

    baseConfig.loader.paths = {
        // paths defined here are from the top level
        // directory of the app
    };

    return baseConfig;

});
