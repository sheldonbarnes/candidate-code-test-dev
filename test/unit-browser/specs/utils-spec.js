/* global describe, expect, it */
/* jshint -W030 */

define([
    'intern!bdd',
    'chai',
    'sinon',
    'sinon-chai',
    'dist/vendor.built',
    'test/unit-browser/helpers/helpers',
    'dist/js/utils'
], function (
    bdd,
    chai,
    sinon,
    sinonChai,
    vendor,
    helpers,
    utils
) {

    var expect = chai.expect;
    chai.use(sinonChai);

    /* jshint withstmt: true */
    with (bdd) {
    /* jshint withstmt: false */
        describe('utils module test spec (js/utils)', function() {

            bdd.before(function(){
               
            });

            bdd.after(function() {
                
            });

            it('utils object should exist', function() {
                expect(utils).to.exist;
            });

            describe('Testing utils method', function() {

                it('method createWrapper should exist', function() {
                    expect(utils.createWrapper).to.exist;
                });


            });

        });

    }

});
