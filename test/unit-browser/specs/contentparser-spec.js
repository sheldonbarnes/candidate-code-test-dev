/* global describe, expect, it */
/* jshint -W030 */

define([
    'intern!bdd',
    'chai',
    'sinon',
    'sinon-chai',
    'dist/vendor.built',
    'test/unit-browser/helpers/helpers',
    'dist/apps/wcm-content/contentParser'
], function (
    bdd,
    chai,
    sinon,
    sinonChai,
    vendor,
    helpers,
    contentParser
) {

    var expect = chai.expect;
    chai.use(sinonChai);

    /* jshint withstmt: true */
    with (bdd) {
    /* jshint withstmt: false */
        describe('contentParser module test spec (apps/wcm-content/contentParser)', function() {

            var getTabContentSpy;
            var getListItemsSpy;
            var getMediaBlockSpy;

            bdd.before(function(){
               getTabContentSpy = sinon.spy(contentParser, 'getTabContent');
               getListItemsSpy  = sinon.spy(contentParser, 'getListItems');
               getMediaBlockSpy = sinon.spy(contentParser, 'getMediaBlock');
            });

            bdd.after(function() {
                getTabContentSpy.restore();
                getListItemsSpy.restore();
                getMediaBlockSpy.restore();
            });

            it('contentParser object should exist', function() {
                expect(contentParser).to.exist;
            });

            describe('Testing getContent method', function() {

                it('method getContent should exist', function() {
                    expect(contentParser.getContent).to.exist;
                });

                //basic Content
                it('parsing contentType \'basicContent\'', function(){
                    var results = contentParser.getContent(helpers.basicContentInput);
                    expect(results.basicContent).to.deep.equal(helpers.basicContentOutputExpected);
                });

                it('permit/deny values should parse correctly for contentType \'basicContent\' ', function() {
                    var results = contentParser.getContent(helpers.basicContentInput);
                    expect(results.basicContent[0].permit).to.equal('producer delegate');
                    expect(results.basicContent[0].deny).to.equal('');
                });

                //FAQ content
                it('parsing contentType \'faqContent\'', function(){
                    var results = contentParser.getContent(helpers.faqContentInput);
                    expect(results.faqContent).to.deep.equal(helpers.faqContentOutputExpected);
                });

                it('permit/deny values should return undefined for contentType \'faqContent\' ', function() {
                    var results = contentParser.getContent(helpers.faqContentInput);
                    expect(results.faqContent[0].permit).to.equal(undefined);
                    expect(results.faqContent[0].deny).to.equal(undefined);
                });

                //all-faq-data parsing need to be considered?

                //gridContent
                it('parsing contentType \'gridContent\'', function(){
                    var results = contentParser.getContent(helpers.gridContentInput);
                    expect(results.gridContent).to.deep.equal(helpers.gridContentOutputExpected);
                });

                //images
                it('parsing contentType \'image\'', function(){
                    var results = contentParser.getContent(helpers.imageContentInput);
                    expect(results.images).to.deep.equal(helpers.imageContentOutputExpected);
                });

                it('getMediaBlock should called if there is Media items to parse ', function() {
                    contentParser.getContent(helpers.mediaBlockContentInput);
                    expect(getMediaBlockSpy).to.have.been.called;
                });

                it('parsing contentType \'mediaBlockContent\'', function(){
                    var results = contentParser.getContent(helpers.mediaBlockContentInput);
                    expect(results.mediaBlock).to.deep.equal(helpers.mediaBlockContentOutputExpected);
                });

                it('URIs with \'/myconnect\' should replace with \'/connect\' to enable anonymous access', function(){
                    var results = contentParser.getContent(helpers.mediaBlockContentInput);
                    expect(results.mediaBlock[0].link.match(/\/myconnect/mg)).to.equal(null);
                });

                it('parsing contentType \'sectionContent\'', function(){
                    var results = contentParser.getContent(helpers.sectionContentInput);
                    expect(results.sectionContent).to.deep.equal(helpers.sectionContentOutputExpected);
                });

                it('parsing contentType \'subSectionContent\'', function(){
                    var results = contentParser.getContent(helpers.subSectionContentInput);
                    expect(results.subSectionContent).to.deep.equal(helpers.subSectionContentOutputExpected);
                });

                it('parsing contentType \'quickLinks\'', function(){
                    var results = contentParser.getContent(helpers.quickLinkContentInput);
                    expect(results.quickLinks).to.deep.equal(helpers.quickLinkContentOutputExpected);
                });
               
                it('getTabContent should called if there is tabContent to parse ', function() {
                    contentParser.getContent(helpers.formsTabContent);
                    expect(getTabContentSpy).to.have.been.called;
                });

                it('getListItems should called if there is match in list item for tabbed content ', function() {
                    contentParser.getContent(helpers.tabContentWithListItem);
                    expect(getListItemsSpy).to.have.been.called;
                });

                //pending tab content parsing

            });

        });

    }

});
