/* global describe, expect, it */
/* jshint -W030 */

define([
    'intern!bdd',
    'chai',
    'sinon',
    'sinon-chai',
    'dist/vendor.built',
    'test/unit-browser/helpers/helpers',
    'dist/js/config/config'
], function (
    bdd,
    chai,
    sinon,
    sinonChai,
    vendor,
    helpers,
    config
) {

    var expect = chai.expect;
    chai.use(sinonChai);

    /* jshint withstmt: true */
    with (bdd) {
    /* jshint withstmt: false */
        describe('config module test spec (js/config/config)', function() {

            bdd.before(function(){
               
            });

            bdd.after(function() {
                
            });

            it('config object should exist', function() {
                expect(config).to.exist;
            });

            describe('Testing config objects', function() {

                it('object hosts should exist', function() {
                    expect(config.hosts).to.exist;
                });

            });

        });

    }

});
