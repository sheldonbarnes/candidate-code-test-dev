var basicContentInput = [
  {
    "name": "BasicContent-Heading 1",
    "title": {
      "lang": "en",
      "value": "Basic Content Heading 1"
    },
    "type": "ShortTextComponent",
    "data": {
      "type": "text/plain",
      "value": "Welcome to OneAmerica<sup>®</sup> Employee Benefits Online"
    }
  },
  {
    "name": "BasicContent-Text 1",
    "title": {
      "lang": "en",
      "value": "Basic Content Text 1"
    },
    "type": "RichTextComponent",
    "data": {
      "type": "text/html",
      "value": "<p dir=\"ltr\">Our refreshed eBenefits website aims to provide a \"one-stop shop\" for the tools and resources you need to do business with us. With easy access to information --&nbsp;on your time --&nbsp;paired with access to our local representatives, we're focused&nbsp;on working alongside you every step of the way. And this is just the beginning of our journey to enhance our online presence to benefit you - <em>our valued client</em>!</p>\n\n<p dir=\"ltr\">First time on our refreshed website?&nbsp;Click to view a flyer to help you navigate.</p>\n"
    }
  },
  {
    "name": "QuickLinks-Permit-RT 1",
    "title": {
      "lang": "en",
      "value": "Quick Links Permit RT1"
    },
    "type": "ShortTextComponent",
    "data": {
      "type": "text/plain",
      "value": "producer delegate"
    }
  },
  {
    "name": "QuickLinks-Deny-RT 1",
    "title": {
      "lang": "en",
      "value": "Quick Links Deny RT1"
    },
    "type": "ShortTextComponent",
    "data": {
      "type": "text/plain",
      "value": ""
    }
  }
];

var basicContentOutputExpected = [
    {
      "heading": "Welcome to OneAmerica<sup>®</sup> Employee Benefits Online",
      "text": "<p dir=\"ltr\">Our refreshed eBenefits website aims to provide a \"one-stop shop\" for the tools and resources you need to do business with us. With easy access to information --&nbsp;on your time --&nbsp;paired with access to our local representatives, we're focused&nbsp;on working alongside you every step of the way. And this is just the beginning of our journey to enhance our online presence to benefit you - <em>our valued client</em>!</p>\n\n<p dir=\"ltr\">First time on our refreshed website?&nbsp;Click to view a flyer to help you navigate.</p>\n",
      "permit": "producer delegate",
      "deny": ""
    }
];

var faqContentInput = [
    {
    "name": "FaqContent-Heading 1",
    "title": {
      "lang": "en",
      "value": "FaqContent Heading 1"
    },
    "type": "ShortTextComponent",
    "data": {
      "type": "text/plain",
      "value": "Billing and Payment FAQs"
    }
  },
  {
    "name": "FaqContent-Text 1",
    "title": {
      "lang": "en",
      "value": "FaqContent Text 1"
    },
    "type": "RichTextComponent",
    "data": {
      "type": "text/html",
      "value": "<p dir=\"ltr\">This section provides you with a fast, easy way to find answers to your most common questions.</p>\n\n<p dir=\"ltr\">If you have any questions not addresses on this page, please contact our Group Contact Center at (800) 553-5318 or <a target=\"\" title=\"\" href=\"mailto:GroupContactCenter@oneamerica.com\">GroupContactCenter@oneamerica.com</a>.</p>\n\n<p dir=\"ltr\">NOTE: Answers may not be applicable to your specific contract(s). For details of your coverage, please refer to your contract(s)</p>\n"
    }
  }
];

var faqContentOutputExpected = [
  {
    "heading": "Billing and Payment FAQs",
    "text": "<p dir=\"ltr\">This section provides you with a fast, easy way to find answers to your most common questions.</p>\n\n<p dir=\"ltr\">If you have any questions not addresses on this page, please contact our Group Contact Center at (800) 553-5318 or <a target=\"\" title=\"\" href=\"mailto:GroupContactCenter@oneamerica.com\">GroupContactCenter@oneamerica.com</a>.</p>\n\n<p dir=\"ltr\">NOTE: Answers may not be applicable to your specific contract(s). For details of your coverage, please refer to your contract(s)</p>\n"
  }
];

var gridContentInput = [
                            {
                                "name" : "GridContent-Heading",
                                "title" :
                                {
                                    "lang" : "en",
                                    "value" : "Grid Content Heading"
                                },
                                "type" : "ShortTextComponent",
                                "data" :
                                {
                                    "type" : "text/plain",
                                    "value" : "Marketing Materials"
                                }
                            },
                            {
                                "name" : "GridContent-Text",
                                "title" :
                                {
                                    "lang" : "en",
                                    "value" : "Grid Content Text"
                                },
                                "type" : "RichTextComponent",
                                "data" :
                                {
                                    "type" : "text/html",
                                    "value" : "<p dir=\"ltr\" style=\"margin-bottom: 0pt;\">These materials are for use with your various audiences to educate on this product.&nbsp;<span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9pt;\">Intended audience by piece is noted as follows:&nbsp;<\/span><\/span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">B = Broker;&nbsp;<\/span><\/span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">ER = Employer;&nbsp;<\/span><\/span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">EE = Employee<\/span><\/span><\/p>\n"
                                }
                            }
                        ];

var gridContentOutputExpected = [
                                    {
                                        "heading": "Marketing Materials",
                                        "text": "<p dir=\"ltr\" style=\"margin-bottom: 0pt;\">These materials are for use with your various audiences to educate on this product.&nbsp;<span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9pt;\">Intended audience by piece is noted as follows:&nbsp;</span></span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">B = Broker;&nbsp;</span></span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">ER = Employer;&nbsp;</span></span><span style=\"font-family:Arial, Helvetica, sans-serif\"><span style=\"font-size:9.0pt;\">EE = Employee</span></span></p>\n"
                                    }
                                ];

var imageContentInput = [
                            {
                                "name" : "Header-Image",
                                "title" :
                                {
                                    "lang" : "en",
                                    "value" : "Header Image"
                                },
                                "type" : "FileComponent",
                                "data" :
                                {
                                    "type" : "application/vnd.ibm.wcm+xml",
                                    "resourceUri" :
                                    {
                                        "type" : "image/jpeg",
                                        "value" : "/wps/wcm/connect/17e2cd98-d3ee-4f40-b49d-6b5eb4b3ee12/h-odit-eb.jpg?MOD=AJPERES"
                                    }
                                }
                            }
                        ];

var imageContentOutputExpected = [
        {
            "image":"/wps/wcm/connect/17e2cd98-d3ee-4f40-b49d-6b5eb4b3ee12/h-odit-eb.jpg?MOD=AJPERES"
        }
    ];

var sectionContentInput = [
                            {
                                "name" : "SectionContent-Heading",
                                "title" :
                                {
                                    "lang" : "en",
                                    "value" : "Section Content Heading"
                                },
                                "type" : "ShortTextComponent",
                                "data" :
                                {
                                    "type" : "text/plain",
                                    "value" : "Contact OneAmerica® Employee Benefits Division"
                                }
                            },
                            {
                                "name" : "SectionContent-Text",
                                "title" :
                                {
                                    "lang" : "en",
                                    "value" : "Section Content - Text"
                                },
                                "type" : "RichTextComponent",
                                "data" :
                                {
                                    "type" : "text/html",
                                    "value" : "<p dir=\"ltr\">Providing you with excellent service starts with you being able to reach us easily when we're needed. Contact numbers, email and mailing addresses for our most frequently contacted departments are provided below.<\/p>\n"
                                }
                            }
                        ];

var sectionContentOutputExpected = [
                                        {
                                            "heading": "Contact OneAmerica® Employee Benefits Division",
                                            "text": "<p dir=\"ltr\">Providing you with excellent service starts with you being able to reach us easily when we're needed. Contact numbers, email and mailing addresses for our most frequently contacted departments are provided below.</p>\n"
                                        }
                                     ];

var subSectionContentInput =[ 
                                {
                                    "name" : "SubSectionContent-Heading 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Sub Section Content Heading 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : "Resources"
                                    }
                                },
                                {
                                    "name" : "SubSectionContent-Text 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Sub Section Content Text 1"
                                    },
                                    "type" : "RichTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/html",
                                        "value" : "<p dir=\"ltr\">These documents are resources to prepare for scheduling and instructing&nbsp;a Continuing Education course.<\/p>\n\n<p dir=\"ltr\"><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-ce-instructor-certification-form?contentIDR=92d703e6-4c77-4c0e-9e9b-3db17a3aa927&amp;useDefaultText=0&amp;useDefaultDesc=0\">CE&nbsp;Instructor&nbsp;Certification&nbsp;Form<\/a><br />\nComplete this form to become certified to teach CE courses; must be completed before scheduling a course.<\/p>"
                                    }
                                }
                            ];

var subSectionContentOutputExpected = [
                                      {
                                        "heading": "Resources",
                                        "text": "<p dir=\"ltr\">These documents are resources to prepare for scheduling and instructing&nbsp;a Continuing Education course.</p>\n\n<p dir=\"ltr\"><a target=\"_blank\" title=\"\" href=\"/wps/wcm/connect/eben/eben2/files/file-ce-instructor-certification-form?contentIDR=92d703e6-4c77-4c0e-9e9b-3db17a3aa927&amp;useDefaultText=0&amp;useDefaultDesc=0\">CE&nbsp;Instructor&nbsp;Certification&nbsp;Form</a><br />\nComplete this form to become certified to teach CE courses; must be completed before scheduling a course.</p>"
                                      }
                                    ];

var quickLinkContentInput = [
                                {
                                    "name" : "QuickLinks-Type 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Links Type 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : "money"
                                    }
                                },
                                {
                                    "name" : "QuickLinks-Header 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Link Header 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : "Commissions"
                                    }
                                },
                                {
                                    "name" : "QuickLinks-Text 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Links Text 1"
                                    },
                                    "type" : "RichTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/html",
                                        "value" : "<p dir=\"ltr\">View your commission statement<\/p>\n"
                                    }
                                },
                                {
                                    "name" : "QuickLinks-Link 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Links Link 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : "commissions"
                                    }
                                },
                                {
                                    "name" : "QuickLinks-Permit 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Links Permit 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : "delegate producer"
                                    }
                                },
                                {
                                    "name" : "QuickLinks-Deny 1",
                                    "title" :
                                    {
                                        "lang" : "en",
                                        "value" : "Quick Links Deny 1"
                                    },
                                    "type" : "ShortTextComponent",
                                    "data" :
                                    {
                                        "type" : "text/plain",
                                        "value" : ""
                                    }
                                }
                            ];

var quickLinkContentOutputExpected = [
                                        {
                                            "heading": "Commissions",
                                            "link": "commissions",
                                            "text": "<p dir=\"ltr\">View your commission statement</p>\n",
                                            "type": "money",
                                            "permit": "delegate producer",
                                            "deny": ""
                                        }
                                    ];

var formsTabContent = [
                          {
                            "name": "TabContent-Type 3",
                            "title": {
                              "lang": "en",
                              "value": "TabContent Type 3"
                            },
                            "type": "ShortTextComponent",
                            "data": {
                              "type": "text/plain",
                              "value": "Cogs"
                            }
                          },
                          {
                            "name": "TabContent-Title 3",
                            "title": {
                              "lang": "en",
                              "value": "TabContent Title 3"
                            },
                            "type": "ShortTextComponent",
                            "data": {
                              "type": "text/plain",
                              "value": "Policy/Employee Admin"
                            }
                          },
                          {
                            "name": "TabContent-Text 3",
                            "title": {
                              "lang": "en",
                              "value": "TabContent Text 3"
                            },
                            "type": "RichTextComponent",
                            "data": {
                              "type": "text/html",
                              "value": "<p dir=\"ltr\">Forms may be downloaded and completed using Adobe Acrobat software. Once downloaded, forms should be completed, printed, and then submitted to the address, email or fax number provided. If you have any questions on forms, please contact our Group Contact Center at (800) 553-5318 or&nbsp;<a target=\"_blank\" title=\"\" href=\"mailto:GroupContactCenter@oneamerica.com\">GroupContactCenter@oneamerica.com</a>.</p>\n\n<h3 dir=\"ltr\">Beneficiary Designation</h3>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-13117?contentIDR=9edd9b12-2573-481a-8e55-b520a909b7e4&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;(G-13117)</a></li>\n\t<li>Standard form to be used to identify your beneficiary designations to ensure proceeds can be paid according to your wishes. Examples:&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18497?contentIDR=9b7cede0-7dbe-4de3-bef2-8ede9998272c&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example&nbsp;(G-18497)</a>;&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18496-spanish?contentIDR=ce426e52-87f7-44b8-9d1f-476abd0d185e&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example:&nbsp;Spanish&nbsp;(G-18496)</a></li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-13117-mn?contentIDR=3e2fbc5b-4f8c-46b6-8db5-d33965ae9306&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation:&nbsp;MN&nbsp;Residents&nbsp;Only&nbsp;(G-13117&nbsp;(MN))</a></li>\n\t<li>Standard form to be used to identify your beneficiary designations to ensure proceeds can be paid according to your wishes. Examples:&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18497?contentIDR=9b7cede0-7dbe-4de3-bef2-8ede9998272c&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example&nbsp;(G-18497)</a>;&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18496-spanish?contentIDR=ce426e52-87f7-44b8-9d1f-476abd0d185e&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example:&nbsp;Spanish&nbsp;(G-18496)</a></li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-13117-spanish?contentIDR=6627f7f0-7306-40c3-9884-667680ea2544&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation:&nbsp;Spanish&nbsp;(G-13117)</a></li>\n\t<li>Standard form to be used to identify your beneficiary designations to ensure proceeds can be paid according to your wishes. Examples:&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18497?contentIDR=9b7cede0-7dbe-4de3-bef2-8ede9998272c&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example&nbsp;(G-18497)</a>;&nbsp;<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-18496-spanish?contentIDR=ce426e52-87f7-44b8-9d1f-476abd0d185e&amp;useDefaultText=0&amp;useDefaultDesc=0\">Beneficiary&nbsp;Designation&nbsp;Example:&nbsp;Spanish&nbsp;(G-18496)</a></li>\n</ul>\n\n<h3 dir=\"ltr\">Employee Forms</h3>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24123?contentIDR=3e4f8ee3-4aa5-414d-bafd-ceccdabcdedf&amp;useDefaultText=0&amp;useDefaultDesc=0\">Disabled&nbsp;Dependent&nbsp;Questionnaire&nbsp;(G-24123)</a></li>\n\t<li>Standard form to be used to verify eligibility for a disabled dependent child who is over the limiting age for Group Life coverage.</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24000?contentIDR=67a24ec2-1524-4de2-87d1-14c21f8c960a&amp;useDefaultText=0&amp;useDefaultDesc=0\">Employee&nbsp;Request&nbsp;for&nbsp;Changes&nbsp;(G-24000)</a></li>\n\t<li>Standard form used to make Employee initiated changes that require the Employee's signature, such as a name or address change, increasing coverage due to a life event, adding newly acquired dependents, and terminating Employee-paid coverage.</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-27064?contentIDR=90b7c972-2e7b-4b3f-b0bf-d47975c8dd56&amp;useDefaultText=0&amp;useDefaultDesc=0\">Authorization&nbsp;of&nbsp;Use&nbsp;and&nbsp;Disclosure&nbsp;of&nbsp;Coverage&nbsp;Information&nbsp;(G-27064)</a></li>\n\t<li>Standard form used to authorize the Company to disclose Employee coverage information to an individual other than the insured Employee.</li>\n</ul>\n\n<h3 dir=\"ltr\">Continuation</h3>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-21776?contentIDR=3ef39a05-d07b-4c4d-aa5c-487b8a9fb292&amp;useDefaultText=0&amp;useDefaultDesc=0\">Application&nbsp;to&nbsp;Continue/Port&nbsp;or&nbsp;Convert&nbsp;Group&nbsp;Insurance&nbsp;(G-21776)</a></li>\n\t<li>Standard form to apply for continuing group coverage under the Portability/Continuation and/or Conversion provisions of the Group insurance policy.</li>\n</ul>\n\n<h3 dir=\"ltr\">Policyholder Forms</h3>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-20977?contentIDR=6261f7c1-3199-4bcf-b18e-de1345186b07&amp;useDefaultText=0&amp;useDefaultDesc=0\">Policyholder&nbsp;Change&nbsp;Request&nbsp;Form&nbsp;for&nbsp;AUL&nbsp;Group&nbsp;Insurance&nbsp;Policyholders&nbsp;(G-20977)</a></li>\n\t<li>Standard form to be used to make changes to your Group, such as name, address, contact information and/or an Agent of Record change.</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-23999?contentIDR=d20e3944-6db9-42e3-a520-c5755e8a053b&amp;useDefaultText=0&amp;useDefaultDesc=0\">Policyholder&nbsp;Request&nbsp;for&nbsp;Employee&nbsp;Changes&nbsp;(G-23999)</a></li>\n\t<li>Standard form used to make Employee level changes initiated by the Policyholder that do not require an Employee's signature, such as salary changes, reinstatement of coverage, and Employee terminations.</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li>Worksite Disability Annual Increase in Benefit (AIB) Offer Form (G-xxxxx) FORM TO COME</li>\n\t<li>Template used by policyholders with an AIB provision in their Worksite Disability Policy to allow eligible Employees an opportunity to increase their coverage.</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-administrative-services-agreement?contentIDR=0ba5b768-6623-442e-9779-61a8c5b62270&amp;useDefaultText=0&amp;useDefaultDesc=0\">Administrative&nbsp;Services&nbsp;Agreement</a></li>\n\t<li>Document outlining roles and responsibilities when the Group Policyholder utilizes a Third Party Administrator (TPA).</li>\n</ul>\n\n<p dir=\"ltr\">&nbsp;</p>\n\n<h3 dir=\"ltr\">Statement of Insurability</h3>\n\n<ul dir=\"ltr\">\n\t<li>All state versions (<a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ak?contentIDR=13517e07-ed9f-4517-9c34-02d8557f4e39&amp;useDefaultText=0&amp;useDefaultDesc=0\">AK</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-az?contentIDR=df755412-07c1-420a-b2fd-0ab202a8f581&amp;useDefaultText=0&amp;useDefaultDesc=0\">AZ</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-14113-ca?contentIDR=55bbe70d-8539-4f98-a2cf-6b79e2c9b6e7&amp;useDefaultText=0&amp;useDefaultDesc=0\">CA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ct?contentIDR=219392c6-2d60-4fc6-86e9-dd64c96786e8&amp;useDefaultText=0&amp;useDefaultDesc=0\">CT</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-fl?contentIDR=6a41c444-c85c-4f90-aa5d-1a6894f42a8f&amp;useDefaultText=0&amp;useDefaultDesc=0\">FL</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ia?contentIDR=a42d7616-96d2-45ad-9f00-d7556f7a4005&amp;useDefaultText=0&amp;useDefaultDesc=0\">IA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-id?contentIDR=ada40fa1-9586-4813-9ff4-9bca83723b30&amp;useDefaultText=0&amp;useDefaultDesc=0\">ID</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-il?contentIDR=b8c4b1eb-9f54-4b91-985c-a308b3e3596b&amp;useDefaultText=0&amp;useDefaultDesc=0\">IL</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-in?contentIDR=936727d3-f2b0-43cb-bc72-99ee2336e98b&amp;useDefaultText=0&amp;useDefaultDesc=0\">IN</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ks?contentIDR=b9642599-e4e7-4af8-b9f1-01cef8ec1bd4&amp;useDefaultText=0&amp;useDefaultDesc=0\">KS</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ky?contentIDR=5c759fb7-f79d-47a8-8e08-6b55c63c62c2&amp;useDefaultText=0&amp;useDefaultDesc=0\">KY</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-la?contentIDR=c74c1708-f58b-4a5b-a333-e8b6ffdf402a&amp;useDefaultText=0&amp;useDefaultDesc=0\">LA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ma?contentIDR=3d54aa2c-3f26-45e0-9dd0-d78f1fcf3516&amp;useDefaultText=0&amp;useDefaultDesc=0\">MA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-md?contentIDR=728b21d9-3b83-463b-b34d-22d72c1a86ec&amp;useDefaultText=0&amp;useDefaultDesc=0\">MD</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-me?contentIDR=5609f68e-d51b-4310-bcb1-7fcdb6234270&amp;useDefaultText=0&amp;useDefaultDesc=0\">ME</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-mn?contentIDR=9cb8ed7f-937a-44b7-b3b4-12626c60fd9d&amp;useDefaultText=0&amp;useDefaultDesc=0\">MN</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-mo?contentIDR=94e38814-60f1-4d4b-a286-0fe12d9dbc5f&amp;useDefaultText=0&amp;useDefaultDesc=0\">MO</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-nc?contentIDR=8880bff9-e6ea-49f4-81d2-8dbc83557036&amp;useDefaultText=0&amp;useDefaultDesc=0\">NC</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-nd?contentIDR=2f9620dd-080d-4bbe-a2af-fee5239e608c&amp;useDefaultText=0&amp;useDefaultDesc=0\">ND</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ne?contentIDR=b69e9f1c-8bf5-4582-b8d1-512263d89a3e&amp;useDefaultText=0&amp;useDefaultDesc=0\">NE</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-nh?contentIDR=90ee1f62-c9e2-46e3-8517-955c4ff4367c&amp;useDefaultText=0&amp;useDefaultDesc=0\">NH</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-nj?contentIDR=0ebfc139-b6de-4952-acf6-ff5d1fe4d1b4&amp;useDefaultText=0&amp;useDefaultDesc=0\">NJ</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-nm?contentIDR=d72276f1-cd12-446f-b421-b3699ddb0fa5&amp;useDefaultText=0&amp;useDefaultDesc=0\">NM</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-oh?contentIDR=aeba3843-6894-4ec1-9bdd-10ef0d288fd8&amp;useDefaultText=0&amp;useDefaultDesc=0\">OH</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ok?contentIDR=d203d5ef-ed0c-42ea-b7a1-f59c37bcb712&amp;useDefaultText=0&amp;useDefaultDesc=0\">OK</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-or?contentIDR=bf002003-936a-4ae0-87e8-e30d28e4a13a&amp;useDefaultText=0&amp;useDefaultDesc=0\">OR</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-pa?contentIDR=0d81c501-de53-4131-a8c6-a5644c937f2d&amp;useDefaultText=0&amp;useDefaultDesc=0\">PA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-sc?contentIDR=ee802169-a69b-48ae-8530-9b751f223110&amp;useDefaultText=0&amp;useDefaultDesc=0\">SC</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-sd?contentIDR=2516bf42-21fd-464e-8e1b-4bf9dfae1de3&amp;useDefaultText=0&amp;useDefaultDesc=0\">SD</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-tx?contentIDR=9b6734e4-8f8b-4035-aa58-f0665fa5646e&amp;useDefaultText=0&amp;useDefaultDesc=0\">TX</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-ut?contentIDR=3d7e9d00-8862-4d24-a00c-52a51523ac8b&amp;useDefaultText=0&amp;useDefaultDesc=0\">UT</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-va?contentIDR=51659e35-7eaa-470c-9c56-47c0b4662578&amp;useDefaultText=0&amp;useDefaultDesc=0\">VA</a>, <a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-24650-all-other-states?contentIDR=b8b855dc-d2bb-4869-b556-5fa1581d9568&amp;useDefaultText=0&amp;useDefaultDesc=0\">All&nbsp;Other&nbsp;States</a>)</li>\n\t<li>Standard form used when applying for coverage as a late enrollee and/or applying for coverage over the guaranteed issue amount.</li>\n</ul>\n\n<h3 dir=\"ltr\">Other</h3>\n\n<ul dir=\"ltr\">\n\t<li><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-c-20610?contentIDR=0b50307d-dcb5-4969-a774-b89b36c895bd&amp;useDefaultText=0&amp;useDefaultDesc=0\">Privacy&nbsp;Notice&nbsp;(C-20610)</a></li>\n\t<li>OneAmerica Privacy Notice</li>\n</ul>\n"
                            }
                          },
                          {
                            "name": "TabContent-Permit 3",
                            "title": {
                              "lang": "en",
                              "value": "TabContent Permit 3"
                            },
                            "type": "ShortTextComponent",
                            "data": {
                              "type": "text/plain",
                              "value": ""
                            }
                          },
                          {
                            "name": "TabContent-Deny 3",
                            "title": {
                              "lang": "en",
                              "value": "TabContent Deny 3"
                            },
                            "type": "ShortTextComponent",
                            "data": {
                              "type": "text/plain",
                              "value": ""
                            }
                          }
                        ];

var mediaBlockContentInput = [
                           {
                            "name" : "MediaBlock-Type 1",
                            "title" :
                            {
                                "lang" : "en",
                                "value" : "Media Block Type 1"
                            },
                            "type" : "ShortTextComponent",
                            "data" :
                            {
                                "type" : "text/plain",
                                "value" : "file"
                            }
                        },
                        {
                            "name" : "MediaBlock-Header 1",
                            "title" :
                            {
                                "lang" : "en",
                                "value" : "Media Block Header 1"
                            },
                            "type" : "RichTextComponent",
                            "data" :
                            {
                                "type" : "text/html",
                                "value" : "<p dir=\"ltr\"><a target=\"_blank\" title=\"\" href=\"/wps/wcm/myconnect/eben/eben2/files/file-g-17180?contentIDR=f1f3ce4e-b2a2-406b-90a3-0d5c6f248816&amp;useDefaultText=0&amp;useDefaultDesc=0\">EAP Employer Data Sheet - B-ER (G-17180)<\/a><\/p>\n"
                            }
                        },
                        {
                            "name" : "MediaBlock-Text 1",
                            "title" :
                            {
                                "lang" : "en",
                                "value" : "Media Block Text 1"
                            },
                            "type" : "RichTextComponent",
                            "data" :
                            {
                                "type" : "text/html",
                                "value" : "<p dir=\"ltr\">Highlights the employer value of the&nbsp;standard EAP plan and enhanced plan buy-up options.<\/p>\n"
                            }
                        }
                        ];

var mediaBlockContentOutputExpected = [
  {
    "heading": "EAP Employer Data Sheet - B-ER (G-17180)",
    "link": "/wps/wcm/connect/eben/eben2/files/file-g-17180?contentIDR=f1f3ce4e-b2a2-406b-90a3-0d5c6f248816&amp;useDefaultText=0&amp;useDefaultDesc=0",
    "text": "<p dir=\"ltr\">Highlights the employer value of the&nbsp;standard EAP plan and enhanced plan buy-up options.</p>\n",
    "type": "file"
  }
];

define({
    basicContentInput               : basicContentInput,
    basicContentOutputExpected      : basicContentOutputExpected,
    faqContentInput                 : faqContentInput,
    faqContentOutputExpected        : faqContentOutputExpected,
    gridContentInput                : gridContentInput,
    gridContentOutputExpected       : gridContentOutputExpected,
    imageContentInput               : imageContentInput,
    imageContentOutputExpected      : imageContentOutputExpected,
    mediaBlockContentInput          : mediaBlockContentInput,
    mediaBlockContentOutputExpected : mediaBlockContentOutputExpected,
    sectionContentInput             : sectionContentInput,
    sectionContentOutputExpected    : sectionContentOutputExpected,
    subSectionContentInput          : subSectionContentInput,
    subSectionContentOutputExpected : subSectionContentOutputExpected,
    quickLinkContentInput           : quickLinkContentInput,
    quickLinkContentOutputExpected  : quickLinkContentOutputExpected,
    formsTabContent                 : formsTabContent
});
