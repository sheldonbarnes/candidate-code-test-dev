// data that would be filled into a form, for example
var data = {
    firstname : 'John',
    lastname  : 'Doe',
    email     : 'email@example.com',
    badEmail  : 'not an email address'
};

var helpers = {

    data: data

    // NOTE
    // Purpose of this file would be to have repetive tasks be called
    // here so that we DRY.
    // Promise chain doesn't seem to allow ordered calls to `then`
    // so it's recommended to NOT use this currently.
    // fillForm1Data: function(){
    //     return this
    //         .findByCssSelector(F1.FIRSTNAME_INPUT)
    //         .click()
    //         .pressKeys(data.firstname)
    //         .end()
    //         .findByCssSelector(F1.LASTNAME_INPUT)
    //         .click()
    //         .pressKeys(data.lastname)
    //         .end()
    //         .findByCssSelector(F1.EMAIL_INPUT)
    //         .click()
    //         .pressKeys(data.email)
    //         .end()
    //         .findByCssSelector(F1.STATES_SELECT)
    //         .click()
    //         .pressKeys('a')
    //         .end()
    //         .findByCssSelector(F1.POLICY_SELECT)
    //         .click()
    //         .pressKeys('s')
    //         .end();
    // }
};

module.exports = helpers;
