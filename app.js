// This file used by Heroku when publishing this app to their nodejs hosting system

/*global module */
/*jshint node:true */
"use strict";

// Application Dependencies
var express = require('express');

// Application variables
var app	= module.exports = express();
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/release'));

