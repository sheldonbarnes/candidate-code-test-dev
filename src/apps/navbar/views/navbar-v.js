var CONST              = require('../const');
var navbarViewTemplate = require('../templates/navbar');

// will autoload the partials in that directory
require('../partials');

var NavbarView = Marionette.ItemView.extend({

    events: {
      "click button[id='user-info-save']": "saveUserInfo"
    },

    saveUserInfo: function(ev) {

      this.model.set('firstName', $('input[id=firstName]').val() );
      this.model.set('lastName', $('input[id=lastName]').val() );
      this.model.set('email', $('input[id=emailAddress]').val() );

      this.model.save ( this.model.toJSON(), {
          success        : this.onSaveSuccess
      });

      console.log('This is the model for the userModel' + JSON.stringify(this.model));

      this.render();

    },
    onSaveSuccess: function(response) {
      console.log('This is the response ' + JSON.stringify(response));
    }
    ,

    initialize: function(options){

        this.model = null;
        console.log('initialize for NavbarView' + JSON.stringify(options));


        if (options && options.userModel) {
            this.model = options.userModel;


            if (this.model.get('role') === 'delegate' && options.selectProducerRoute) {
              this.selectProducerRoute = options.selectProducerRoute;
            }

        }

    },

    template: navbarViewTemplate,

    templateHelpers: function() {

        return {
        };
    }


});

module.exports = NavbarView;
