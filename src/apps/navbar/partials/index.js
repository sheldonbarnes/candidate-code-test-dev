var Handlebars = require("hbsfy/runtime");

// TODO
// Should be able to write this file out using a gulp task
// So that it's created on build, and just reads the directory

Handlebars.registerPartial('navbar-mobile-toggle', require('./navbar-mobile-toggle'));

Handlebars.registerPartial('navbar-header', require('./navbar-header'));
Handlebars.registerPartial('navbar-header-brand', require('./navbar-header-brand'));

Handlebars.registerPartial('navbar-dropdown', require('./navbar-dropdown'));
Handlebars.registerPartial('navbar-dropdown-usermenu', require('./navbar-dropdown-usermenu'));
Handlebars.registerPartial('navbar-dropdown-usermenu-user', require('./navbar-dropdown-usermenu-user'));
Handlebars.registerPartial('navbar-dropdown-usermenu-item', require('./navbar-dropdown-usermenu-item'));
Handlebars.registerPartial('navbar-user-info-update', require('../../userinfo/partials/userinfo-update'));

module.exports = Handlebars;
