var NavbarView = require('./views/navbar-v');
var utils = require('./utils');

var Navbar = Marionette.Application.extend({

    initialize: function(options) {

        // Placeholder variable used below to shorten line length
        var user;
        var navbarOptions;

        var defaults = {
            API: {}
        };

        var userDefaults = {
            data                    : null,
            logoutRoute             : '',
            changeSecurityInfoRoute : '',
            changePasswordRoute     : '',
            changeUserIdRoute       : ''
        };

        // Defaults are false for all the components this app supports
        var apiValues = [ 'messages', 'notifications', 'tasks', 'user' ];

        // Populate the defaults API object
        _.each(apiValues, function(value) {
            defaults.API[value] = false;
        });

        // Fail fast overall
        if (typeof options !== 'object') {
            options = defaults;
        }

        // Fail fast for the API subo
        if (options.API && typeof options.API !== 'object') {
            options.API = defaults.API;
        }

        // Strip out any options we don't support
        options.API = _.pick(options.API, apiValues);

        // Now we can activate the options that were passed
        // For the moment, this only supports the user menu
        // So setting all others to false
        options.API.messages = options.API.notifications = options.API.tasks = false;

        // Now create a locally scoped API objec that gets populated as things are validated
        this.API = {};

        // The idea is that the application should make sure that if a component
        // is requested, that all the needed parts for that are also requested

        // Validate the User object we were passed
        // shorten reference as mentioned above
        user = options.API.user;
        if (user === false) {
            // do not show anything
            user = null;
        } else if (
            typeof user !== 'object' ||
                // user data must be a backbone model
                (!user.data || (user.data instanceof Backbone.Model) !== true) ||
                // logoutRoute not defined
                (typeof user.logoutRoute !== 'string') ||
                // changeSecurityInfo route not defined
                (typeof user.changeSecurityInfoRoute !== 'string')
            ) {
                // user options were not correctly passed, do not initialize the user with
                // any data
                utils.logger.error('User information not correctly passed, will not initialize the user');
                this.API.user = userDefaults;
        } else {
            // We have a (hopefully) valid user object to work with
            this.API.user = options.API.user;


        console.log('The options ' + JSON.stringify(options));
            this.alertMessages = options.API.user.alerts.get('items');

            console.log('The alertMessages ' + JSON.stringify(this.alertMessages));

        }

        // Now start things off
        return new NavbarView({
            el                      : options.$rootEl,
            logoutRoute             : (user ? this.API.user.logoutRoute : ''),
            userInfoRoute             : (user ? this.API.user.userInfoRoute : ''),
            changeSecurityInfoRoute : (user ? this.API.user.changeSecurityInfoRoute : ''),
            changeUserIdRoute       : (user ? this.API.user.changeUserIdRoute : ''),
            changePasswordRoute     : (user ? this.API.user.changePasswordRoute : ''),
            selectProducerRoute     : (user ? this.API.user.selectProducerRoute : null),
            userModel               : (user ? this.API.user.data : null),
            messages                : this.alertMessages
        }).render();
    }

});

module.exports = Navbar;
