var _global = (function() { return this;})();

// Config file is a singleton so that runtime configuration is set on load.
// You can set values in the query parameters like this: debug=false
// Or in localStorage like this: localStorage.setItem('debug', 'false');

// Will be overridde by preprocessing in browserify
// Can check for a value of 'true' to mean we are in DEV mode
var debugValue = '/* @echo DEV */';
debugValue = (debugValue === 'true' ? true : false );

// Only place values in here that you will allow to be overridden
// For other values, place in the protectedConfig
var config = {

    debug: debugValue

};

// Values placed here will not be overridden by query params
// or localStoreage
var protectedConfig = {

    console    : !!_global.console,
    namespace  : 'EBEN_NAVBAR'

};

// TODO handle arrays, objects
// Currently just returns strings
function parseLocalStorage(config) {

    var localStorageConfig = {};

    // loop variables, for iterating over object properties
    var current, prop;

    if (!_global.localStorage) {
        return localStorageConfig;
    }

    for (prop in config) {
        if (
            config.hasOwnProperty(prop) === true &&
                _global.localStorage.getItem(prop) !== 'undefined'
        ) {
            current = _global.localStorage.getItem(prop);
            if (current === 'true') {
                current = true;
            } else if (current === 'false') {
                current = false;
            }
            localStorageConfig[prop] = current;
        }
    }
    return localStorageConfig;
}

/*
 * Turn any query parameters into a configuration object
 * @param {param} string query string
 * @returns {object}
 */

function parseQueryParams(params) {

    // return value
    var config = {};
    // looping vars
    var i, len, current;
    // temp variables for dealing with name/value pairs
    var name, value;

    if (!params) {
        return config;
    }

    if (/^\?/.test(params) === true) {
        params = params.substr(1);
    }

    // get a list of all values that were passed
    if (params.length > 0) {

        // check for encoded html
        if (/&amp;/.test(params) === true) {
            params = params.replace('&amp;', '&');
        }

        // break up into name/value pairs
        params = params.split('&');
        len = params.length;
        for (i = 0; i < len; i++) {

            // now split into name/value
            current = params[i];
            current = current.split('=');

            if (current.length > 0) {

                name = current[0];
                value = current[1];

                // convert to real booleans
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                }
                config[name] = value;

            } else {

                // If here, wasn't a valid foo=bar
                continue;

            } // if/else

        } // for loop

    } // for loop over params.length

    return config;

}

// Singleton
module.exports = (function() {
    var runtimeConfig = parseQueryParams(_global.location.search);
    runtimeConfig = _.extend({}, runtimeConfig, parseLocalStorage(config));
    config = _.extend({}, config, runtimeConfig, protectedConfig);
    return config;
})();

module.exports = config;
