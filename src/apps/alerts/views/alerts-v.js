var alertsViewTemplate = require('../templates/alert');

var AlertsModel      = require('../../../js/models/alerts-m');

require('../partials');  // this is the partials for the alerts cool

var AlertsView = Marionette.ItemView.extend({

    template: alertsViewTemplate,
    events: {
      "click div[id='deleteButton']": "deleteAlert",
      "click button[id='addAlert']" : "addAlert"
    },
    addAlert : function(ev) {

      var alertData = {"message" : $('input[id=newAlertText]').val(),   "messageType" : "fa-bell-o", "id": Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000  }

      this.newAlert = new AlertsModel(alertData);

      this.model.get('items').push(alertData);
      this.model.save();

      this.render();
    },

    deleteAlert: function(ev) {
      var alertId = $(ev.currentTarget).data('id');

      this.model.get('items').splice(_.indexOf(this.model.get('items'), _.findWhere(this.model.get('items'), { 'id' : alertId})), 1);
      this.newAlert = new AlertsModel( { 'id' : alertId });
      this.newAlert.destroy();
      this.render();
    },

    initialize: function(options) {
      this.model = options.alerts_1;
    },

    templateHelpers: function() {
        return {
          alerts: this.model.get('items')
        }
    }

});



module.exports = AlertsView;
