var AlertsView = require('./views/alerts-v');

var Alerts = Marionette.Application.extend({

    initialize: function(options) {
        return new AlertsView({
            el      : options.$rootEl
        }

      ).render();
    }

});

module.exports = Alerts;
