var Handlebars = require("hbsfy/runtime");

// TODO
// Should be able to write this file out using a gulp task
// So that it's created on build, and just reads the directory

Handlebars.registerPartial('sidebar-nav-menu', require('./sidebar-nav-menu'));
Handlebars.registerPartial('sidebar-nav-menu-item', require('./sidebar-nav-menu-item'));

module.exports = Handlebars;
