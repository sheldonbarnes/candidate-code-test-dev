var utils = require('../utils');

// The base model for an item in the sidebar navigation
var SidebarMenuModel = Backbone.Model.extend({

    defaults: function(){
        return {
            icon: '',
            item: '',
            link: '',
            subItems: null
        };
    },

    validate: function(attrs, options){

        if (typeof attrs.icon !== 'string') {
            return 'icon must be a string ';
        }

        if (typeof attrs.item !== 'string') {
            return 'item must be a string ';
        }

        if (typeof attrs.link !== 'string') {
            return 'link must be a string ';
        }

        if (typeof attrs.subItems !== null && utils.isArray(attrs.subItems) !== true) {
            return 'subItems must be an array';
        }

    }

});

module.exports = SidebarMenuModel;
