var _global = (function() { return this; })();

var config                = require('config');
var RootView              = require('./views/sidebar-v');
var SEL                   = require('./selectors');
var SidebarMenuModel      = require('./models/sidebar-m');
var SidebarMenuCollection = require('./collections/sidebar-menu-c');
var SidebarMenuView       = require('./views/sidebar-menu-v');
var utils                 = require('./utils');

require('./sidebar-helpers.js');
require('./partials');

var Sidebar = Marionette.Application.extend({

    // Function is specific to the menu, which is not ideal.
    // But w/o an anonymous function, not sure how to do this.
    // And trying to use named functions for stack trace legibility.
    createView: function (ViewName, viewConfig) {
        if (!ViewName || ViewName && typeof ViewName !== 'function') {
            utils.logger.error('Must pass a Constructor as ViewName to createView');
            return false;
        }
        if (viewConfig && typeof viewConfig !== 'object') {
            utils.logger.error('Must pass an object as viewConfig to createView');
            return false;
        } else {
            return new ViewName(viewConfig);
        }
    },

    initialize: function(options) {

        if (options.namespace && typeof options.namespace !== 'string') {
            utils.logger.error('\'namespace\' must be a string in Sidebar/index.js initialize');
            return false;
        }
        if (
            options.namespace &&
            typeof options.namespace === 'string' &&
            typeof _global[options.namespace] === 'object'
        ) {
            // add our namespace under the passed one
            _global[options.namespace][config.namespace] = {};
        } else {
            // Add to the top level object
            _global[config.namespace] = {};
        }

        // Menu setup
        this.menu = null;

        this.menu = (options.menu ? options.menu : this.menu);
        if (_.isArray(this.menu) !== true) {
            utils.logger.error('Menu is not an array, cannot create the Sidebar');
            return false;
        } else {
            this.menuCollection = new SidebarMenuCollection(this.menu);
        }

        // Allow listening to events on passed in arguments
        _.extend(this, Backbone.Events);

        // A LayoutView will hold the regions we want to manipulate
        // But we will do all logic here, in the Application.
        this.RootView = new RootView({
            el: options.$rootEl
        });
        // Must render the root view so that the elements for the regions exist
        // Before we can show the regions
        this.RootView.render();

        // Start adding any needed regions
        // And show views in the added regions
        var menuRegion = this.setupRegion('menuRegion', SEL.REGIONS.MENU);
        // A collection was used here, because it allowed validation
        // of the models for each item
        // But we aren't using a CollectionView to listen for any changes
        // We will shut down and recreate the views on changes
        this.showViewInRegion(menuRegion, this.createView(SidebarMenuView, {
            collection: this.menuCollection
        }));

        // Finally, handle changes
        this.listenTo(this.menuCollection, 'add', this.showMenuView);
        this.listenTo(this.menuCollection, 'remove', this.showMenuView);
        this.listenTo(this.menuCollection, 'update', this.showMenuView);
        this.listenTo(this.menuCollection, 'sort', this.showMenuView);
        this.listenTo(this.menuCollection, 'add', this.showMenuView);

        return true;

    },

    setupRegion: function(regionName, regionSelector) {
        return this.RootView.addRegion(regionName, regionSelector);
    },

    showMenuView: function() {
        // Calling a 'show' of a view in a region automatically destroys the
        // existing view in that region before creating the new view.
        // So this automatically does our cleanup for us.
        var menuRegion = this.RootView.regions.menuRegion;
        this.showViewInRegion(menuRegion, this.createView());
    },

    showViewInRegion: function (regionName, viewName) {
        regionName.show(viewName);
    }

});

module.exports = Sidebar;
