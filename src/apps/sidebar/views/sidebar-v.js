var SEL                 = require('../selectors');
var sidebarViewTemplate = require('../templates/sidebar-t');

var SidebarView = Marionette.LayoutView.extend({

    template: sidebarViewTemplate,

    templateHelpers: function(){
        return {
            MENU: SEL.REGIONS.MENU.substr(1)
        };
    }

});

module.exports = SidebarView;
