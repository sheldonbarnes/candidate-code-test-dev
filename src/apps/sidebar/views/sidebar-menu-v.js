var _global = (function() { return this; })();

var menuViewTemplate = require('../templates/sidebar-menu-t');
var SEL               = require('../selectors');

var SidebarMenuView = Marionette.ItemView.extend({

    onAttach: function(){
        // view is part of the document now, so safe to run plugins
        _global.ace.initialize(_global.$);
    },

    template: menuViewTemplate

});

module.exports = SidebarMenuView;
