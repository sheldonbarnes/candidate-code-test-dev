var SidebarNavModel = require('../models/sidebar-m');

var SidebarNavCollection = Backbone.Collection.extend({

    model: SidebarNavModel

});

module.exports = SidebarNavCollection;
