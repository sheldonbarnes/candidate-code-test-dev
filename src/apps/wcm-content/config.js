var _global = (function() { return this;})();

// Will be overridde by preprocessing in browserify
var debugValue = '/* @echo DEV */';
debugValue = (debugValue === 'true' ? true : false );

// Only place values in here that you will allow to be overridden
// For other values, place in the protectedConfig
var config = {

    debug      : debugValue,
    wcmBaseUrl : '/wps/contenthandler/wcmrest/Content/',
    // This is the URL used for custom data calls in WCM
    // For example, when loading the FAQs
    wcmDataUrl : '/wps/wcm/connect/eben/eben2data/'
};

// Values placed here will not be overridden by query params
var protectedConfig = {

    console   : !!_global.console,
    namespace : 'EBEN_WCM_CONTENT',
    // Special flag for loading FAQ data
    faqId: '_WCM_FAQ'

};

function parseQueryParams(params) {

    // return value
    var config = {};
    // looping vars
    var i, len, current;
    // temp variables for dealing with name/value pairs
    var name, value;

    if (!params) {
        return config;
    }

    if (/^\?/.test(params) === true) {
        params = params.substr(1);
    }

    // get a list of all values that were passed
    if (params.length > 0) {

        // check for encoded html
        if (/&amp;/.test(params) === true) {
            params = params.replace('&amp;', '&');
        }

        // break up into name/value pairs
        params = params.split('&');
        len = params.length;
        for (i = 0; i < len; i++) {

            // now split into name/value
            current = params[i];
            current = current.split('=');

            if (current.length > 0) {

                name = current[0];
                value = current[1];

                // convert to real booleans
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                } else {
                    // Don't want to re-encode here, but set to strings again
                    // just in case of references to scoped variables
                    value = value + '';
                }
                config[name] = value;

            } else {

                // If here, wasn't a valid foo=bar
                continue;

            } // if/else

        } // for loop

    } // for loop over params.length

    return config;

}

// Singleton
module.exports = (function() {
    var runtimeConfig = parseQueryParams(_global.location.search);
    config = _.extend({}, config, runtimeConfig, protectedConfig);
    return config;
})();
