var config = require('./config');

// Set up the categories we need to know about
var i;
var len;
var faqCategoryPrefix = '/eben/FAQ/';

// These are the FAQ categories that we need to know about
// in order to fetch them from WCM
var faqCategories = [
    'Admin-of-Benefits-Beneficiary-Information',
    'Admin-of-Benefits-Dependent-Coverage',
    'Admin-of-Benefits-Eligibility-and-Effective-Dates',
    'Admin-of-Benefits-Employee-Terminations',
    'Admin-of-Benefits-Forms-and-Certificates',
    'Admin-of-Benefits-Policy-Employee-Changes',
    'Admin-of-Benefits-iBill-Administration',
    'Billing-Payments-Bill-Types',
    'Billing-Payments-Premium',
    'Billing-Payments-Summary-Billing',
    'Billing-Payments-ePay-and-iBill',
    'Disability-Claims',
    'Disability-Coverage',
    'Life-Claims-Definitions',
    'Life-Claims-Employer-Info',
    'Life-Coverage'
];



len = faqCategories.length;
for (i = 0; i < len; i++) {
    faqCategories[i] = faqCategoryPrefix + faqCategories[i];
}
// Make the query param that WCM needs
faqCategories = faqCategories.join(',');
// Strip off the last comma
faqCategories = faqCategories.substr(0, faqCategories.length);

var WcmFaqModel = Backbone.Model.extend({

    defaults: function() {
        return {
            idAttribute: config.faqId,
            items: []
        };
    },

    parse: function(data) {

        var content = null;

        return {
            idAttribute: config.faqId,
            items: data.items
        };

    },

    // The url is defined here instead of on the collection because collections
    // are used when the collection itself is hitting a REST route and getting
    // back data. In this case, we are using the collection just to hold our
    // data since it's a convenient place to do so. But we aren't getting data
    // via the collection.For WCM, we are getting single content items each
    // time.
    // In addition, for the FAQ items, we are storing them all in a single model
    // since they are a special case.
    url: function() {

        return [
            config.wcmDataUrl,
            'listFaqs?categories=',
            encodeURIComponent(faqCategories)
        ].join('');
    }


});

module.exports = WcmFaqModel;
