var _global = (function() { return this;})();
var config = require('./config');

var _info = config.namespace + ': ';
var _logger = (function() {

    var logger = null;
    var noop = function(){};
    if (
        _global &&
            _global.console &&
            (Boolean(config.debug)) === true &&
            config.debug !== 'false'
    ) {
        logger = {};
        logger.log = _global.console.log;
        logger.error = _global.console.error;
        logger.debug = _global.console.debug;
        logger.warn = _global.console.warn;
        // table does not exist in all browsers
        logger.table = (_global.console.table ? _global.console.table : noop);
    } else {
        logger = function(){ return false; };
        logger.log = logger.error = logger.debug = logger.warn = logger.table = noop;
    }
    return logger;
})();

// There are 2 reasons to keep this function present:
// 1. You have to support older versions of IE
// 2. You want to customize how you do logging.
// For #2, we actually check for a config.debug property below, that is set on
// load by the config singleton. So you can safely put log statements in, knowing
// that they will NOT be rendered in prod
//

// For anything that does not support apply on the console object
var unWrappedLogger = _logger;
// For those that do
var wrappedLogger = {

    debug: function() {

        var args = Array.prototype.slice.call(arguments);

        if (typeof args[0] === 'string') {
            args[0] = _info + args[0];
        } else {
            args.unshift(_info);
        }
        _logger.debug.apply(_global.console, args);
    },

    log: function() {

        var args = Array.prototype.slice.call(arguments);
        if (typeof args[0] === 'string') {
            args[0] = _info + args[0];
        } else {
            args.unshift(_info);
        }
        _logger.log.apply(_global.console, args);
    },

    error: function() {

        var args = Array.prototype.slice.call(arguments);
        if (typeof args[0] === 'string') {
            args[0] = _info + args[0];
        } else {
            args.unshift(_info);
        }
        _logger.error.apply(_global.console, args);
    },

    warn: function() {

        var args = Array.prototype.slice.call(arguments);
        if (typeof args[0] === 'string') {
            args[0] = _info + args[0];
        } else {
            args.unshift(_info);
        }
        _logger.warn.apply(_global.console, args);
    },

    table: function() {

        var args = Array.prototype.slice.call(arguments);
        // table must have array or object as first parameter
        if ((typeof args[0] !== 'string') && (typeof args[0].length === 'undefined')) {
            if (typeof args[0] !== 'object') {
                return false;
            }
        }
        _logger.table.apply(_global.console, args);
    }

};

// The real module
var utils = {

    /*
     * Creates a wrapper div to hold various apps
     * @returns {object} jQuery object
     * @param {object} options
     * @param {object} options.wrapper string refering to a DOM element
     * @param {boolean} options.prepend prepend the new element to the wrapper?
     */
    createWrapper: function(options) {

        var defaults = {
            el: '<div></div>',
            prepend: false,
            wrapper: 'body'
        };

        var $el;

        if (typeof options !== 'object') {
            options = defaults;
        }

        if (typeof options.el !== 'string' || options.el === '') {
            options.el = defaults.el;
        }
        $el = $(options.el);

        if (typeof options.wrapper !== 'string' || options.wrapper === '') {
            options.wrapper = defaults.wrapper;
        }

        if (typeof options.prepend !== 'boolean') {
            options.prepend = defaults.prepend;
        }

        if (options.prepend === true) {
            $(options.wrapper).prepend($el);
        } else {
            $(options.wrapper).append($el);
        }

        return $el;
    },

    isArray: function(item) {
        return (typeof item === 'object' &&
                item.length !== 'undefined' &&
                typeof item.slice === 'function');
    },

     logger: (_logger.log.apply ? wrappedLogger : unWrappedLogger),

    /*
     * Title case a string
     * adapted from
     * http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
     * @param {string} text Text to perform a replacement on
     * @returns {string}
     */
    titleCase: function(text) {
        return text.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

};

module.exports = utils;
