// WHY THIS PARSER IS NEEDED
// --------------------------------------------------
// The contract with the business is that there are multiple types of content
// that they might want to use in each page. Some of those are very simple,
// like a heading and some text. Others get more complicated, like a heading,
//  a type of icon, then a large rich text fields that contains specific
// elements in a specific order.
//
// WCM via the REST api does not allow creation of nested content in a
// single item. For example, if you wanted to represent something like a
// group of tabs, you would normally define a top level tab title, then
// create the items in that tab directly under that title item. In WCM,
// you can only create the tab title and the tab items all at the top
// level.
//
// To work around this, the items in WCM are created all at that top level,
// but with a strict order and count of items. For the above example, we
// would create something called 'TabGroup-Title' as the first item, and
// then 'TabGroup-Content' IMMEDIATELY after that item. Now we can parse
// the content looking for the FIRST item that is named 'TabGroup-Title'
// and manually group the NEXT item with the first.
//
// In addition, if the 'TabGroup' content needs to have further nesting,
// we can do that with RegExp parsing of a known structure of HTML.
//
// Yes, this is true:
// http://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags/1732454#1732454

// However, we are not parsing arbitrary HTML. We are parsing a KNOWN set of
// elements. The downside to this is that ANY extra elements in the HTML will
// cause the parsing to fail and return back an empty object.
//
// The burder is on the business writing the content to make sure that the
// extra elements are not present in the HTML. The parser will merely throw
// out the chunk of content and keep going if it does not match.
//
// In addition, the business needs to have certain chunks of content show or
// not show for certain kinds of users. For this particular app, that is a
// user type. To do this, we add 2 additional fields to each item, called
// permit and deny. Those fields contain a list of types that are permitted
// or denied viewing the content. We do not parse that out here, we just
// add the values to the manual ordering we create. Parsing is left to the
// views that will render it, since we don't have access to the current
// user role at this point.
//
// HOW IT WORKS
// --------------------------------------------------
// The WCM REST service returns back (nested a few layers deep)
// an array of objects like this:
// {
//     "name" : "BasicContent-Heading 1",
//     "title" :
//     {
//         "lang" : "en",
//         "value" : "Basic Content Heading 1"
//     },
//     "type" : "ShortTextComponent",
//     "data" :
//     {
//         "type" : "text/plain",
//         "value" : "Welcome to OneAmerica<sup>Â®<\/sup> Employee Benefits Online"
//     }
// }
//
// For what we need, the key fields are the 'name' and the 'data.value' fields.
// There are some items, like the 'image' item, that contain additional
// nested values. But the above example is the most common.

// This code walks through the returned content and parses out the content
// into the appropriate object so that the content templates can loop
// through and render it out.
//
// There is one entry here for each type of content that might be in the
// WCM JSON returned. These types are defined in WCM with known names as
// found below. For each item that is returned, we look for a match on
// the name of the item, and then parse through the next N number of
// elements to group them together.
//
// We only need the first 'part' of each item that will be returned
// For example, BasicContent is made up of BasicContent-Heading
// and BasicContent-Text
// but we only have to match the first BasicContent-Heading item
// then we know that the next item should be the BasicContent-Text item

// Our parsed content, manually ordered and created to match what the
// templates need
var contentToReturn;

// Permit/deny fields should have names that start with:
// ItemType-Permit or ItemType-Deny so we can match them.
// Not every content block has these properties
var permitDenyField = /^[a-zA-Z]+-(permit|deny)/;

//////////////////////////////////////////////////
// WCM Cleanup RegExps and Functions
//////////////////////////////////////////////////

// Wcm inserts whitespace in the JSON like this
var reWcmWhitespace = /[\n\t]+/gm;
var reWcmNbsp = /&nbsp;/gim;

// When authors write content, the internal linking of WCM creates URLs with
// myconnect. This implies a session and forces the user to login. Using just
// connect allows anonymous access.
var reWcmLink = /\/myconnect/gim;

// Want to match on \/
// Wcm puts HTLM into JSON like this: <p \"foo\"=\"bar\">baz<\/p>
// We need just <p \"foo\"=\"bar\">baz</p>
var reWcmHtmlClosingEscape = /\\\//;


// WCM spacing tags automatically inserted
var reWcmBlankPara = /<p dir="ltr"><\/p>/gim;

// Clean up the data before trying to match
// Using the objects above.
function rewriteWcmWhitespace(text) {
    return text.replace(reWcmWhitespace, '');
}

function rewriteWcmNbsp(text, replaceWith) {
    if (!replaceWith) {
        replaceWith = '';
    }
    return text.replace(reWcmNbsp, replaceWith);
}

function rewriteWcmLinks(text) {
    return text.replace(reWcmLink, '/connect');
}

function rewriteWcmHtmlClosingEscapes(text) {
    return text.replace(reWcmHtmlClosingEscape, '');
}

function rewriteWcmBlankParas(text) {
    // Because you need to match w/o the weird escaped slash
    // always run this to be safe
    text = text.replace(reWcmHtmlClosingEscape, '');
    text = text.replace(reWcmNbsp, '');
    return text.replace(reWcmBlankPara, '');
}

//////////////////////////////////////////////////
// WCM cleanup End
//////////////////////////////////////////////////

//////////////////////////////////////////////////
// RegExp objects
//////////////////////////////////////////////////

// match anything that's an attribute of this element
// So given <p foo="bar" required baz="blah">
// would match ' foo="bar" required baz="blah"'
var reAttrs = '(?:[^>]+)?';

// Match the value of this element
// Given <h3 foo="bar">my value</h3>
// match 'my value'
var reValue = '([^<]+)';
// Same as above, but do not capture the match
var reValueNonCapturing = '(?:[^<]+)';

// a value that might also contain HTML. Like <p><a href="#foo">bar</a></p>
var reValueWithHtml = '(.+)';
// Same as above, but do not capture the match
var reValueWithHtmlNonCapturing = '(?:.+)';

// Match values that may have children
// Example:
// Given <ul><li>foo</li><ul>
// match '<li>foo</li>'
// NOTE - this is where additional HTML items will BLOW THINGS UP
var reValueWithSubItems = '(.+?)';
// Same as above, but do not capture the match
var reValueWithSubItemsNonCapturing = '(?:.+?)';

// Match opening/closing tags for the elements that we allow
// We nest the reAttrs RegExp here to match that part.
// These matches depend on you running the function reWriteWcmHtmlClosingEscape
// before trying to match

var reTagHeader = '</?h\\d+' + reAttrs + '>';
var reTagLink = '</?a' + reAttrs + '>';
var reTagPara = '</?p' + reAttrs + '>';
var reTagUl = '</?ul' + reAttrs + '>';
var reTagLi = '</?li' + reAttrs + '>';

// Now build up the full element RegExps for each kind of element.
var reHeader                = '(?:' + reTagHeader + reValue + reTagHeader + ')';
var reHeaderNonCapturing    = '(?:' + reTagHeader + reValueNonCapturing + reTagHeader + ')';
var reLink                  = '(?:' + reTagLink + reValue + reTagLink + ')';
var reLinkNonCapturing      = '(?:' + reTagLink + reValueNonCapturing + reTagLink + ')';
var reParagraph             = '(?:' + reTagPara + reValue + reTagPara + ')';
var reParagraphNonCapturing = '(?:' + reTagPara + reValueNonCapturing + reTagPara + ')';
var reUl                    = '(?:' + reTagUl + reValueWithSubItems + reTagUl + ')';
var reLi                    = '(?:' + reTagLi + reValueWithSubItems + reTagLi + ')';
var reUlNonCapturing        = '(?:' + reTagUl + reValueWithSubItemsNonCapturing + reTagUl + ')';

function getHref(link) {

    if (typeof link !== 'string') {
        return '';
    }

    var href='';
    var reMain = /href=['"]{1}([^'"]+)['"]{1}/;
    href = reMain.exec(link);
    if (href) {
        return href[1];
    }
    return '';
}

/*
 * returns an object containing the needed permit/deny fields
 * @input Object permitItem A WCM content item
 * @input Object denyItem A WCM content item
 * @returns Object An object with string values for each key of permit/deny
 */
function getPermitDeny(permitItem, denyItem) {

    // Our defaults. If we return defaults, the view parsing code will
    // look at these empty values and not apply any filtering at all.
    var permit = null;
    var deny = null;

    // Only try to parse this out of all the needed fields are there
    // and match what we expect.
    if (
        permitItem &&
            permitItem.name &&
            permitDenyField.test(permitItem.name.toLowerCase()) === true &&
            denyItem &&
            denyItem.name &&
            permitDenyField.test(denyItem.name.toLowerCase()) === true
    ) {
        // Everything is there, read the values and set them
        permit = permitItem.data.value;
        deny = denyItem.data.value;
    }
    return {
        permit: permit,
        deny: deny
    };
}

var contentParser = {

    // The link for the media block is in the header item, have to parse it out
    getMediaBlock: function(item) {

        if (typeof item !== 'string') {
            return '';
        }

        // Needed to make sure we still match
        item = rewriteWcmHtmlClosingEscapes(item);

        var reMain;
        var link = '';

        // P with a link inside it
        reMain = new RegExp([
            '(?:',
            reTagPara,
            reLink,
            // sometimes whitespace at the end of these?
            // No idea why.
            '(?:&nbsp;)?',
            reTagPara,
            ')'
        ].join(''), 'gim');
        link = reMain.exec(item);
        // return the link text and the href
        if (link) {
            return {
                href: getHref(item),
                text: rewriteWcmNbsp(link[1], ' ')
            };
        }

        // fail
        return {
            href: '',
            text: ''
        };
    },

    getTabContent: function(item, type) {

        // Fail fast.
        if (typeof item !== 'string') {
            return '';
        }

        var items = [];
        var itemCopy;

        // clear out extra chars so we do not have to parse them.
        // WCM escapes the new lines and whitespace in the REST api.
        item = rewriteWcmWhitespace(item);
        item = rewriteWcmNbsp(item);
        item = rewriteWcmLinks(item);
        item = rewriteWcmBlankParas(item);
        item = rewriteWcmHtmlClosingEscapes(item);

        itemCopy = item;

        // loop variables
        var i, len;

        // Now check for matches and start looping if they are there.
        var current;

        // We will build an master RegExp from pieces
        var reMain;
        var reP;
        var reH;
        var infoText;
        var title;
        var subTitle;
        var lists = [];

        var reCheck;
        var itemTempCopy;
        var list;
        var listItems;

        // loop test
        var moreData = (itemCopy.length > 0 ? true : false);

        // strip off the paragraph
        reP = '(' + reTagPara + reValueWithHtmlNonCapturing + reTagPara + ')';
        itemCopy = itemCopy.match(reP);
        infoText = itemCopy[1];
        itemCopy = itemCopy.input.split(itemCopy[0])[1];

        while (moreData === true) {

            // One p, one h2, one h3, multiple uls, repeat
            // As above.
            // Note that we match globally, case insensitive, across line breaks.
            // That's the 'gim' flags at the end.

            // strip off the header plus optional header
            reH = (reHeader + '+?' + reHeader + '?');
            itemCopy = itemCopy.match(reH);
            title = itemCopy[1];
            subTitle = (itemCopy[2] ? itemCopy[2] : '');
            itemCopy = itemCopy.input.split(itemCopy[0])[1];

            reMain = new RegExp([
                // Start only
                '^',
                // open the UL
                reTagUl,
                '(',
                // Open first LI
                reTagLi,
                // Open A
                reTagLink,
                // Link content
                reValueNonCapturing,
                // Close A
                reTagLink,
                // Close first LI
                reTagLi,
                // Open second LI
                reTagLi,
                // Content
                reValueNonCapturing,
                // Close second LI
                reTagLi,
                // capture
                ')',
                // Close UL
                reTagUl
            ].join(''), 'im');

            // strip off the ULs
            itemTempCopy = itemCopy;

            lists = [];

            while ((reCheck = itemTempCopy.match(reMain))) {
                list = reCheck[1];
                listItems = this.getListItems(list);
                lists.push(listItems[0]);
                itemTempCopy = itemTempCopy.split(reCheck[0])[1];
            }

            // Process specific items that need to be one massive LI versus
            // XX separate ULs each with a single LI
            // Specifically, the State Forms are 32+ individual forms
            // So manually build the string of all links
            var str = '';
            var filteredList = [];
            _.each(lists, function(list) {
                if (list.secondaryItem === 'DROPDOWN_STATE_ITEM') {
                    str += (list.primaryItem + ', ');
                } else {
                    filteredList.push(list);
                }
            });

            if (str.length > 0) {
                // Remove last comma and space
                str = str.substr(0, str.length -2);
                filteredList.push({ primaryItem: str, secondaryItem: 'State forms' });
                lists = filteredList;
            }
            // end processing of separate items

            items.push({
                sectionItems    : lists,
                sectionTitle    : title,
                sectionSubTitle : subTitle,
                // need the icon here
                type            : type
            });


            itemCopy = itemTempCopy;
            if (!itemCopy || itemCopy.length < 1) {
                moreData = false;
            }

        } // while

        return {
            info     : infoText,
            subItems : items
        };
    },

    // Similar to above, this parses out list items that appear in
    // pairs and gets the content.
    // <li>item 1</li><li>Item 2</li>
    // This allows the WCM authors to easily use the rick text editor to add
    // items with a known format.
    getListItems: function(item) {

        if (typeof item !== 'string') {
            return '';
        }

        // This should get called by any parent, but just in case
        // Needed to make sure we still match
        item = rewriteWcmHtmlClosingEscapes(item);

        // The items to return.
        var data = [];

        // Loop vars
        var i, len;

        // Might be a question and answer, or a link and some text, etc.
        // The list items are always expected to occur in sets of 2.
        var primaryItem, secondaryItem;
        var re;

        // Grab the matches, we'll clean them up later
        var listItems = item.match(/<li(?:[^>]+)?>(.+?)<\/li>/gi);

        if (!listItems) {
            return '';
        }

        len = listItems.length;

        // Use this to clean up the HTML to only leave the content
        re = /<\/?li(?:[^>]+)?>/gi;

        // Loop through each LI
        for (i = 0; i < len; i++) {

            // Remove markup
            primaryItem = listItems[i].replace(re, '');
            secondaryItem = listItems[i+1].replace(re, '');

            data.push({
                primaryItem: primaryItem,
                secondaryItem: secondaryItem
            });

            i++;
        }

        return data;
    },

    // This is the workhorse of this module. It takes any given set of content
    // and rearranges the sibling items into the nested structure that is needed
    // for the application.
    getContent: function(items) {

        if (!items) {
            return {};
        }

        // Loop variables
        var i;
        var len = items.length;
        var current;
        var currentName;
        var entry;
        var childEntry;
        var linkObj;

        // Handle the permit and deny fields.
        var permissionCount = 2;
        var currentPermissions = null;

        // A structure of what the content should look like. Each of these will
        // be cloned and the 'entries' array populated with the matching
        // content from the original data.
        // The 'siblingCount' property is to know how many fields in the
        // original data should be parsed through to capture the entire set
        // of this content block.
        // When there is a 'children' property, this will be used to hold nested
        // content that will be parsed out by the helper functions above.
        //
        // The names are IMPORTANT - the WCM content templates use these names
        // so we can match on each one to know what processing to apply.
        var contentTypes = {

            basicContent: {
                name: 'BasicContent-Heading',
                format: {
                    heading: '',
                    text: ''
                },
                siblingCount: 1,
                entries: []
            },

            faqContent: {
                name: 'FaqContent-Heading',
                format: {
                    heading: '',
                    text: ''
                },
                siblingCount: 1,
                entries: []
            },

            gridContent: {
                name: 'GridContent-Heading',
                format: {
                    heading: '',
                    text: ''
                },
                siblingCount: 1,
                entries: []
            },

            images: {
                name: 'Header-Image',
                format: {
                    image: ''
                },
                siblingCount: 0,
                entries: []
            },

            mediaBlock: {
                name: 'MediaBlock-Type',
                format: {
                    heading: '',
                    link: '',
                    text: '',
                    type: ''
                },
                siblingCount: 2,
                entries: []
            },


            quickLinks: {
                name: 'QuickLinks-Type',
                format: {
                    heading: '',
                    link: '',
                    text: '',
                    type: ''
                },
                siblingCount: 2,
                entries: []
            },

            sectionContent: {
                name: 'SectionContent-Heading',
                format: {
                    heading: '',
                    text: ''
                },
                siblingCount: 1,
                entries: []
            },

            subSectionContent: {
                name: 'SubSectionContent-Heading',
                format: {
                    heading: '',
                    text: ''
                },
                siblingCount: 1,
                entries: []
            },

            tabContent: {
                name: 'TabContent-Type',
                format: {
                    type: '',
                    title: '',
                    children: []
                },
                siblingCount: 2,
                entries: []
            }

        };

        // Loop through all the entries in the WCM elements array
        for (i = 0; i < len; i++) {

            current = items[i];

            // Name can be like MediaBlock-Type 1, clear out numbering for
            // easier matching. The numbers are only there for the content
            // authors to use.
            currentName = items[i].name.replace(/( ?\d+)?$/, '');

            // Reset the permissions each time
            currentPermissions = null;

            // Match on the name of the item to process.
            switch (currentName) {

                case contentTypes.basicContent.name:
                // Make a blank copy to hold the data
                entry = _.extend({}, contentTypes.basicContent.format);
                // Set the various properties
                entry.heading = current.data.value;
                // For however many properties we need, go grab them
                entry.text = items[i + 1].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 2], items[i + 3]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.heading && entry.text) {
                    contentTypes.basicContent.entries.push(entry);
                }
                // Increment i so we jump past the the entries we just processed
                i += contentTypes.basicContent.siblingCount;
                break;

                case contentTypes.faqContent.name:
                // Make a blank copy to hold the data
                entry = _.extend({}, contentTypes.faqContent.format);
                // Set the various properties
                entry.heading = current.data.value;
                entry.text = items[i + 1].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 2], items[i + 3]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.heading && entry.text) {
                    contentTypes.faqContent.entries.push(entry);
                }
                // Increment i so we jump past the the entries we just processed
                i += contentTypes.faqContent.siblingCount;
                break;

                case contentTypes.gridContent.name:
                entry = _.extend({}, contentTypes.gridContent.format);
                entry.heading = current.data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                entry.text = items[i + 1].data.value;
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 2], items[i + 3]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.heading && entry.text) {
                    contentTypes.gridContent.entries.push(entry);
                }
                i += contentTypes.gridContent.siblingCount;
                break;

                case contentTypes.images.name:
                entry = _.extend({}, contentTypes.images.format);
                if (
                    current.data &&
                        current.data.resourceUri &&
                        current.data.resourceUri.value
                ) {
                    entry.image = current.data.resourceUri.value;
                    // Fix any links that might be in this content
                    entry.image = rewriteWcmLinks(entry.image);
                }
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 1], items[i + 2]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.image) {
                    contentTypes.images.entries.push(entry);
                }
                i += contentTypes.images.siblingCount;
                break;

                case contentTypes.mediaBlock.name:
                entry = _.extend({}, contentTypes.mediaBlock.format);
                entry.type = current.data.value;
                // heading contains the link we need wrapped in a p
                linkObj = this.getMediaBlock(items[i + 1].data.value);
                entry.link = rewriteWcmLinks(linkObj.href);
                entry.heading = linkObj.text;
                entry.text = items[i + 2].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 3], items[i + 4]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.type && entry.heading && entry.text) {
                    contentTypes.mediaBlock.entries.push(entry);
                }
                i += contentTypes.mediaBlock.siblingCount;
                break;

                case contentTypes.sectionContent.name:
                // Make a blank copy to hold the data
                entry = _.extend({}, contentTypes.sectionContent.format);
                // Set the various properties
                entry.heading = current.data.value;
                // For however many properties we need, go grab them
                entry.text = items[i + 1].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 2], items[i + 3]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.heading && entry.text) {
                    contentTypes.sectionContent.entries.push(entry);
                }
                // Increment i so we jump past the the entries we just processed
                i += contentTypes.sectionContent.siblingCount;
                break;

                case contentTypes.subSectionContent.name:
                // Make a blank copy to hold the data
                entry = _.extend({}, contentTypes.subSectionContent.format);
                // Set the various properties
                entry.heading = current.data.value;
                // For however many properties we need, go grab them
                entry.text = items[i + 1].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 2], items[i + 3]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.heading && entry.text) {
                    contentTypes.subSectionContent.entries.push(entry);
                }
                // Increment i so we jump past the the entries we just processed
                i += contentTypes.subSectionContent.siblingCount;
                break;

                case contentTypes.quickLinks.name:
                entry = _.extend({}, contentTypes.quickLinks.format);
                entry.type = current.data.value;
                entry.heading = items[i + 1].data.value;
                entry.text = items[i + 2].data.value;
                // Fix any links that might be in this content
                entry.text = rewriteWcmLinks(entry.text);
                entry.link = items[i + 3].data.value.toLowerCase();
                if (/^#+/.test(entry.link) === true) {
                    // prevent any # on the link since we add that in the template
                    entry.link = entry.link.replace('#', '');
                }
                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 4], items[i + 5]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (entry.type && entry.heading && entry.text && entry.link) {
                    contentTypes.quickLinks.entries.push(entry);
                }
                i += contentTypes.quickLinks.siblingCount;
                break;

                case contentTypes.tabContent.name:
                // Make a blank copy to hold the data
                entry = _.extend({}, contentTypes.tabContent.format);
                // Set the various properties
                entry.type = current.data.value.toLowerCase();
                entry.title = items[i + 1].data.value;
                entry.children = this.getTabContent(items[i + 2].data.value, entry.type);

                // permit/deny check
                currentPermissions = getPermitDeny(items[i + 3], items[i + 4]);
                if (currentPermissions.permit !== null && currentPermissions.deny !== null) {
                    entry.permit = currentPermissions.permit;
                    entry.deny = currentPermissions.deny;
                    i += permissionCount;
                }
                // Save the thing we just created, only if there is content
                if (
                    entry.type &&
                        entry.title &&
                        entry.children &&
                        entry.children.length !== 0
                ) {
                    contentTypes.tabContent.entries.push(entry);
                }
                // Increment i so we jump past the the entries we just processed
                i += contentTypes.tabContent.siblingCount;
                break;

            } // switch

        }

        // Build an object to return that contains all the possible entries
        contentToReturn =  {
            basicContent      : contentTypes.basicContent.entries,
            faqContent        : contentTypes.faqContent.entries,
            gridContent       : contentTypes.gridContent.entries,
            images            : contentTypes.images.entries,
            mediaBlock        : contentTypes.mediaBlock.entries,
            sectionContent    : contentTypes.sectionContent.entries,
            subSectionContent : contentTypes.subSectionContent.entries,
            quickLinks        : contentTypes.quickLinks.entries,
            tabContent        : contentTypes.tabContent.entries
        };

        // And now get rid of the empties. Looking at this now, it's
        // sort of backwards - could have just grabbed the populated items.
        for (var key in contentToReturn) {
            if (contentToReturn.hasOwnProperty(key) === true) {
                if (contentToReturn[key].length === 0) {
                    delete contentToReturn[key];
                }
            }
        }

        return contentToReturn;
    }

};

module.exports = contentParser;
