var WcmContentModel = require('./wcm-content-m');
var WcmFaqModel     = require('./wcm-faq-m');

// Because we will have 2 kinds of model, don't specify here
var WcmContentCollection = Backbone.Collection.extend({

    model: function(attrs, options) {

        // Our collection can contain 2 kinds of models
        // Content model, and FAQ model
        if (typeof attrs.content !== 'undefined') {
            // content model
            return new WcmContentModel(attrs, options);
        } else {
            // FAQ model
            return new WcmFaqModel(attrs, options);
        }
    }

});

module.exports = WcmContentCollection;
