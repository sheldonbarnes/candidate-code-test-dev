var config               = require('./config');
var utils                = require('./utils');
var WcmContentCollection = require('./wcm-content-c');

var WcmContent = Marionette.Object.extend({

    logCacheHit: function(model) {
        utils.logger.log('Cache hit for the WCM Content collection for ', model.get('idAttribute'));
    },

    /*
     * @param {object} options - Options for the collection
     * @param {boolean} [options.fetchAll = false] - Fetch all model ids passed in via constructor?
     * @param {array} [options.ids = []] - Array of ids to setup in the constructure. Will not be fetched automatically unless fetchAll is true
     * @param {function} [options.success = null] - Success callback for each model when fetched, if fetchAll is true
     * @param {function} [options.error = null] - Error callback for each model when fetched, if fetchAll is true
     *
     */
    initialize: function(options){

        _.bindAll(this,
                 'getFaqItem',
                 'loadFaqData'
                 );

        options = options || {};

        if (typeof options.fetchAll === 'undefined' || typeof options.fetchAll !== 'boolean') {
            options.fetchAll = false;
        }

        if (typeof options.ids === 'undefined' || utils.isArray(options.ids) === false) {
            options.ids = [];
        }

        if (typeof options.success === 'undefined' || typeof options.success !== 'function') {
            options.success = function() {};
        }

        if (typeof options.error === 'undefined' || typeof options.error !== 'function') {
            options.error = function() {};
        }

        // Get the initial collection in place
        this.wcmContentCollection = new WcmContentCollection();
        this.wcmContentCollection.on('cachesync', this.logCacheHit);
        // On instantiation, never true
        // TODO expose this as some API? Right now giving direct access
        this.isFaqDataLoaded = false;

        var counter = 0;
        var i, len;
        var models = [];

        if (options.ids.length) {

            len = options.ids.length;

            for (i = 0; i < len; i++) {
                models.push({
                    idAttribute: options.ids[i],
                    content: null
                });
            }

            this.wcmContentCollection.add(models);

            if (options.fetchAll === true) {
                this.wcmContentCollection.each(function(model) {
                    model.fetch({
                        cache: true,
                        // https://github.com/madglory/backbone-fetch-cache/tree/v1.5.5#expires
                        // Set a 2 day cache expiration for WCM Content
                        // Value is in seconds as per docs above
                        expires: (2 * 24 * 60 * 60),
                        success: function() {
                            counter++;
                            if (counter === len) {
                                options.success.call(this);
                            }
                        },
                        error:  function() {
                            counter++;
                            if (counter === len) {
                                options.error.call(this);
                            }
                        }
                    });
                });

            }

        } // length

    },

    getFaqItem: function() {

        if (this.isFaqDataLoaded === true) {
            utils.logger.log('Found the FAQ item in the WCM collection');
            return this.wcmContentCollection.where({
                idAttribute: config.faqId
            });
        } else {
            utils.logger.error('Cannot find the FAQ item in the WCM collection');
            return false;
        }
    },

    loadFaqData: function(options) {

        // Prevent double loading
        if (this.isFaqDataLoaded === true) {
            return;
        }

        var self = this;
        var faqItem;

        options = options || {};

        if (typeof options.success === 'undefined' || typeof options.success !== 'function') {
            options.success = function() {};
        }

        if (typeof options.error === 'undefined' || typeof options.error !== 'function') {
            options.error = function() {};
        }

        faqItem = this.wcmContentCollection.add({
            idAttribute: config.faqId,
            items: null
        });

        faqItem.fetch({
            cache: true,
            error: function() {
                // Note that this item will NOT be added to the collection
                // So it should be safe to run this again.
                self.isFaqDataLoaded = false;
                self.wcmContentCollection.remove(faqItem);
                options.error.call();
            },
            success: function() {
                self.isFaqDataLoaded = true;
                options.success.call();
            }
        });

    }

});

module.exports = WcmContent;
