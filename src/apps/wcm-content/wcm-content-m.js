var config        = require('./config');
var contentParser = require('./contentParser');
var utils         = require('utils');

var WcmContentModel = Backbone.Model.extend({

    defaults: function() {
        return {
            content: null,
            // Maps directly to the id. Usually, we just use 'id'.
            // But we might be setting the ID of the model before a sync
            // happens. This lets us use the real ID we would have gotten back
            // when the sync finished before it happens.
            idAttribute: null
        };
    },

    parse: function(data) {
        var id = null;
        var content = {};

        // Strip the returned value down to the actual id used in WCM
        // WCM seems to do HATEOS, or something close to is, so there
        // are lots of extra elements returned
        if (data && data.entry && data.entry.id) {
            id = data.entry.id.replace(/wcmrest:/, '');
        }

        if (
            data &&
            data.entry &&
            data.entry.content &&
            data.entry.content.content &&
            data.entry.content.content.elements &&
            data.entry.content.content.elements.element
        ) {
            content = data.entry.content.content.elements.element;
        }

        // See the contentParser.js file for documentation on this
        try {
            content = contentParser.getContent(content);
        }
        catch (e) {
            utils.logger.error('Error parsing WCM content for item ', id);
            // Set content to be empty so further functions will continue
            content = {};
        }

        return {
            idAttribute: id,
            content: content
        };
    },

    // The url is defined here instead of on the collection because collections
    // are used when the collection itself is hitting a REST route and getting
    // back data. In this case, we are using the collection just to hold our
    // data since it's a convenient place to do so. But we aren't getting data
    // via the collection.For WCM, we are getting single content items each
    // time.
    url: function() {
        return [
            config.wcmBaseUrl,
            this.get('idAttribute'),
            '?mime-type=',
            encodeURIComponent('application/json')
        ].join('');
    }

});

module.exports = WcmContentModel;
