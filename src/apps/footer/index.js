var FooterView = require('./views/footer-v');

var Footer = Marionette.Application.extend({

    initialize: function(options) {

        return new FooterView({
            el: options.$rootEl
        }).render();
    }

});

module.exports = Footer;
