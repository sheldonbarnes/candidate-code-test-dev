var footerViewTemplate = require('../templates/footer');

var FooterView = Marionette.ItemView.extend({

    template: footerViewTemplate

});

module.exports = FooterView;
