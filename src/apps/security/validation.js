// Validation regex and parameters

// Strings to build regexps from
var alpha                 = 'A-Za-z';
var nums                  = '0-9';
var special               = '~!@#%^&*()\-_+=';

var _passwordAllowedChars = alpha + nums + special;

// There's no good way to render these in the template with the english name, so
// not even going to bother. Update them there is they change, please.
var _userIdAllowedSpecial  = '\-_.@';
var _userIdAllowedChars    = alpha + nums + _userIdAllowedSpecial;
var _userIdAllowedEndChars = alpha + nums;

// Min/max constraints on some fields
var _sizes = {
    // security question answer
    answer: {
        min: 1,
        max: 20
    },
    password: {
        min: 8,
        max: 20
    },
    // security question
    question: {
        min: 1,
        max: 50
    },
    userId: {
        min: 6,
        max: 64
    }
};

// given 'foo', return '[foo]'
// Used to build a character range regexp
function _charRange(chars) {
    if (typeof chars !== 'string') {
        return '';
    }
    return '[' + chars + ']';
}

function _charRangeNegated(chars) {
    if (typeof chars !== 'string') {
        return '';
    }
    return '[^' + chars + ']';
}

// Build the error messages from the passed in values
function _maxMsg(max) {
    return [
        'Please enter no more than ',
        max,
        ' characters.'
    ].join('');
}

function _minMsg(min) {
    return [
        'Please enter at least ',
        min,
        ' characters.'
    ].join('');
}

var validation = {

    chars: {

        special: special
    },
    
    messages: {
        
        answerTooLong                 : _maxMsg(_sizes.answer.max),

        badCaptcha                    : 'You must fill out the captcha to continue.',

        confirmEmailAddress           : 'The email addresses you provided do not match.',

        confirmNewPassword            : 'The passwords you provided do not match. Please confirm.',
        confirmNewUserId              : 'The User IDs you provided do not match.',

        email                         : 'An email address such as email@example.com is required.',
        
        newPasswordHasInvalidChars    : 'Your password contains invalid characters. Please see the allowed list of alphabetical, numeric, and special characters above.',
        newPasswordHasWhitespace      : 'Your password may not contain spaces, tabs, or other similar blank characters.',
        newPasswordHasUserId          : 'Your password cannot contain the user ID. Please enter a valid password.',
        newPasswordNeedsTwoCategories : 'Your password must contain characters from at least two of the above categories.',
        newPasswordSameAsOld          : 'Your new password cannot be the same as current password.  Please enter a valid password.',
        newPasswordTooLong            : _maxMsg(_sizes.password.max),
        newPasswordTooShort           : _minMsg(_sizes.password.min),

        newUserIdInvalidChars      : 'Your User ID contains invalid characters or spaces.',
        newUserIdInvalidEndChars      : 'Your User ID may not end in special characters.',
        newUserIdOnlyNumbers          : 'Your User ID cannot contain 9 consecutive numbers.',
        newUserIdHasPassword          : 'Your User ID cannot contain the password.',
        newUserIdHasWhitespace        : 'Your User ID cannot contain spaces, tabs, or other similar blank characters.',
        newUserIdTooLong              : _maxMsg(_sizes.userId.max),
        newUserIdTooShort             : _minMsg(_sizes.userId.min),

        questionTooLong               : _maxMsg(_sizes.question.max),

        required                      : 'This field is required.',

        userIdHasWhitespace           : 'Your User ID cannot contain spaces, tabs, or other similar blank characters.'
        
        
    },
    
    re: {
        
        // validation courtesy of the $ validation plugin
        // https://github.com/jzaefferer/jquery-validation/blob/master/src/core.js#L1165
        email: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,

        // Used in testing passwords for meeting complexity requirements
        hasAlpha      : (function() { return new RegExp(_charRange(alpha)); })(),
        hasNumber     : (function() { return new RegExp(_charRange(nums)); })(),
        hasWhitespace : /\s/,
        hasSpecial    : (function() { return new RegExp(_charRange(special)); })(),
        
        passwordAllowedChars: (function() { return new RegExp(_charRange(_passwordAllowedChars)); })(),
        passwordInvalidChars: (function() { return new RegExp(_charRangeNegated(_passwordAllowedChars)); })(),

        userIdInvalidChars: (function() { return new RegExp(_charRangeNegated(_userIdAllowedChars)); })(),
        userIdInvalidEndChars: (function() { return new RegExp(_charRangeNegated(_userIdAllowedEndChars)); })(),

        userIdOnlyNumbers: /^[0-9]{9}$/

    },

    sizes: _sizes
    
};

module.exports = validation;
