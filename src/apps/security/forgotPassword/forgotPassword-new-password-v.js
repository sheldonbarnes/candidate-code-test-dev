var _global = (function() { return this;})();

var CONST                             = require('../const');
var config                            = require('../../content/config');
var SEL                               = require('../selectors');
var forgotPasswordNewPasswordTemplate = require('./forgotPassword-new-password-t');
var utils                             = require('../../content/utils');

require('../partials');

var ForgotPasswordNewPasswordView = Marionette.ItemView.extend({

    // These be set on the containing element that Backbone automatically
    // creates to hold this view
    attributes: {
        'class'  : 'form-horizontal ' + SEL.FORGOTPASSWORD.FORM.substr(1),
        'method' : 'post',
        'role'   : 'form'
    },

    // Submittal of form will kick everything off
    events: {
        'click @ui.submit, submit': 'onSubmitPreventDuplicates',
        'click @ui.reset'         : 'onReset'
    },

    initialize: function(options) {

        // These are called as part of an async callback, so set their
        // scope manually here.
        _.bindAll(this,
                 'onSaveError',
                 'onSaveSuccess'
                 );

        // The model should be defined when instantiated
        this.model = (options.model ? options.model : null);

        // What commands we should issue
        this.comm = options.comm;
        
    },

    // When a model is validated, Backbone triggers an 'invalid' event if
    // it does not pass. This is our indication that we should rerender, because
    // we want to show the errors.
    modelEvents: {
        'invalid' : 'onInvalid'
    },

    // Handle an invalid event triggered by the model not passing validation
    onInvalid: function() {
        // We read the errors and then redisplay, with the errors rendering
        // this time. model.validationError holds the return value of the
        // model.validate function when it was called.
        this.model.set('errors', this.model.validationError, { silent: true });

        if (this.preventSubmit === true) {
            this.preventSubmit = false;
        }

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        this.render();
    },

    onReset: function(e) {

        this.$el.find('.error').hide();
        this.$el.find(SEL.INPUTS).attr('value', '');
        
    },
    
    // Callback if the save to the server fails.
    onSaveError: function(model, response, options){

        var tempObj;
        
        utils.logger.error('Model save to server failed.');

        // Process any 400 error code
        if (
            response &&
                response.responseJSON &&
                response.responseJSON.errors &&
                /400/.test(response.status) === true
        ) {

            // Gets the errors into a state we can mixin to the model
            // This just returns an object, but lets use use the same method
            // in parse to get the errors into place
            tempObj = this.model.parse(response.responseJSON);
            model.set(tempObj, { silent: true });
            
            // we need to allow submittals again
            this.preventSubmit = false;

            this.comm.channel.command(this.comm.commandNameHideLoading);
            
            this.render();
            
        } else {

            // Set a view property that will be a flag for showing an error message
            this.__generalError = [
                'There was an error sending your information. Please try again',
                ' by clicking the submit button again.',
                ' If you see this error again, please contact us for help.'
            ].join('');

            // trigger invalid, which will cause a rerender
            this.model.trigger('invalid');

        }

    },
    
    // This is where we tell the parent view to render a success message
    onSaveSuccess: function(){

        utils.logger.log('Model save to server succeeded.');

        // this is just cleanup, the view should be destroyed before this matters again
        this.preventSubmit = false;
        
        // Parent layout view listens to these events
        this.model.trigger('change', this.model);
    },

    onSubmitPreventDuplicates: function(e) {

        // Make sure we cannot submit if a request is already in flight
        
        e.preventDefault();

        if (this.preventSubmit === true) {
            
            // prevents another submittal
            return false;
            
        } else {
        
            this.preventSubmit = true;
            this.toggleSubmit(false);
            this.submitForm();
        }

        return true;

    },

    // Then manually trigger an update of the model
    submitForm: function() {

        utils.logger.log('Forgot password new password submitted');

        var userInput;

        // Grab the inputs
        userInput = this.$el.serializeArray() || [];
        if (userInput.length === 0) {
            return false;
        }

        this.updateModel(this.model, userInput);

        return true;
    },

    // element that will hold this view
    tagName: 'form',

    template: forgotPasswordNewPasswordTemplate,

    templateHelpers: function(){

        return {
            __generalError         : this.__generalError,
            RESET                  : SEL.CHANGEINFO.RESET.substr(1),
            SUBMIT                 : SEL.CHANGEINFO.SUBMIT.substr(1),
            SUBMIT_DISABLED        : this.preventSubmit
        };
    },

    toggleSubmit: function(state) {

        if (
                state === null ||
                typeof state !== 'boolean'
        ) {
            utils.logger.error('Cannot toggle state of submit button, bad argument passed.');
            return false;
        }

        utils.logger.log('Toggling submit on the ForgotPassword new password screen');
        
        if (state === true) {
            this.ui.submit.prop('disabled', '');
        } else if (state === false) {
            this.ui.submit.prop('disabled', 'disabled');
        }
        
    },
    
    ui: {
        reset  : SEL.CHANGEINFO.RESET,
        submit : SEL.CHANGEINFO.SUBMIT
    },

    // Take the user info and apply to the model, then try a save
    updateModel: function(model, data) {

        // placeholder var for when we iterate over the questions
        var child;

        // loop through the data and update the model
        data.forEach(function(obj){
            if (typeof model.attributes[obj.name] !== 'undefined') {
                model.set(obj.name, obj.value, { silent: true });
            }
        });

        // Remove any existing errors that might have been there from a
        // previous attempt at a save.
        if (model.get('errors')) {
            model.set('errors', null, { silent: true });
        }
        
        this.comm.channel.command(this.comm.commandNameShowLoading);
        
        // Triggering a save will automatically run validation, and stop the
        // save if it fails. When you do a save, you have to be explicit about
        // what data to save. So even though we already updated the model with this
        // data, we have to pass it again.
        model.save(model.toJSON(), {
            // set some fields to not be validated
            checkCaptcha  : false,
            checkQuestions: false,
            checkUserId   : false,
            error         : this.onSaveError,
            silent        : true,
            success       : this.onSaveSuccess,
            // do not change model until server gives a success
            wait          : true
        });

    }

});

module.exports = ForgotPasswordNewPasswordView;
