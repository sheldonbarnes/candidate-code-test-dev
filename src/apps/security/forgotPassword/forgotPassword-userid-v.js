var _global = (function() { return this;})();

// called when recaptcha script loads
_global.captchaCallback = function() {
     utils.logger.log('captchaCallback');
     _global.recaptchaLoaded = true;
     _global.initRecaptcha();
};

var CONST                        = require('../const');
var config                       = require('../../content/config');
var SEL                          = require('../selectors');

var forgotPasswordUserIdTemplate = require('./forgotPassword-userid-t');
var utils                        = require('../../content/utils');

var ForgotPasswordUserIdView = Marionette.ItemView.extend({

    // These be set on the containing element that Backbone automatically
    // creates to hold this view
    attributes: {
        'class'  : 'form-horizontal ' + SEL.FORGOTPASSWORD.FORM.substr(1),
        'method' : 'post',
        'role'   : 'form'
    },

    // Submittal of form will kick everything off
    events: {
        'click @ui.submit, submit' : 'onSubmitPreventDuplicates',
        'click @ui.reset'          : 'onReset'
    },
    
    // Create the recaptcha on the page
    initRecaptcha: function() {
        // if doing local development and you want to see the recaptcha
        // remove the check for debug. The validation still won't actually
        // look for it unless you edit the model as well.

        // __RECAPTCHA__
        utils.logger.log('initRecaptcha: recaptchaLoaded = ' + _global.recaptchaLoaded);
        
        if (_global.recaptchaLoaded && this.recaptchaLoadInProcess === false) {

            this.recaptchaLoadInProcess = true;
            
            this.ui.loading.remove();

            _global.grecaptcha.render(
                SEL.FORGOTPASSWORD.RECAPTCHA_WRAPPER.substr(1),
                {'sitekey': CONST.RECAPTCHA.KEY_V2}
            );

            // We toggle this here, even though the recaptcha may not have been
            // fully rendered out. There is no callback for when the rendering is complete,
            // so this is the best we can do. The user might still be able to click 'Submit'
            // before the loading finishes, but short of detecting the google iframe on the
            // page, not much else we can do.
            this.preventSubmit = false;
            this.toggleSubmit(true);
            
        }

    },

    initialize: function(options) {

        // These are called as part of an async callback, so set their
        // scope manually here.
        _.bindAll(this,
                  'initRecaptcha',
                  'onSaveError',
                  'onSaveSuccess'
                 );

        // The model should be defined when instantiated
        this.model = (options.model ? options.model : null);

        // a check to prevent multiple submits
        // We could bind to change events on this, but for now we manually toggle
        // via another function.
        this.preventSubmit = true;

        // What commands we should issue
        this.comm = options.comm;
        
        // Needed in captcha callback for scoping
        var self = this;
        
        utils.logger.log('Loading captcha script');

        // We start loading recaptcha both in the render and the show events.
        // We need it onRender for when we reset the entire view with the reset button
        // we need it onShow for when the parent region shows this view
        // AND
        // we need it to NOT be onRender when repeating the process
        // So these are flags to prevent double loading
        // And prevent loading before the view is shown
        this.recaptchaLoadInProcess = false;
        this.canLoadRecaptcha = false;
        
        // If the user repeats this process, we don't need to load the script
        // again. Just allow things to go forward.
        if (
            // initial page load
            typeof _global.initRecaptcha == 'undefined'
        ) {

            // This global is used later by the captcha callback
            // and needs to be setup here since initRecaptha is only
            // defined by this view.
            _global.initRecaptcha = this.initRecaptcha;

            // Now load the actual script
            $.ajax({
                dataType : 'script',
                url      : CONST.RECAPTCHA.URL
            })
                .done(function(){
                    utils.logger.log('captcha script loaded');                
                })
                .fail(function() {
                    self.onRecaptchaFail();
                });
        } else {
            utils.logger.log('Recaptcha already loaded, skipping');
        }
    },

    // When a model is validated, Backbone triggers an 'invalid' event if
    // it does not pass. This is our indication that we should rerender, because
    // we want to show the errors.
    modelEvents: {
        'invalid' : 'onInvalid'
    },

    // Handle an invalid event triggered by the model not passing validation
    onInvalid: function() {

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        // We read the errors and then redisplay, with the errors rendering
        // this time. model.validationError holds the return value of the
        // model.validate function when it was called.
        this.model.set('errors', this.model.validationError, { silent: true });
        // allow submissions again
        if (this.preventSubmit === true) {
            this.preventSubmit = false;
        }
        this.render();
    },

    // If the recaptcha code fails to load (it is external)
    // show an error on the page and invite them to try again.

    // __RECAPTCHA__
    onRecaptchaFail: function() {
        utils.logger.error('Could not load RECAPTCHA code');

        // Set a view property that will be a flag for showing an error message
        this.__generalError = CONST.RECAPTCHA.ERROR_MSG;

        // trigger invalid, which will cause a rerender, displaying the above
        // error message
        this.model.trigger('invalid');
    },

    onRender: function() {

        utils.logger.log('In the onRender event of the ForgotPassword UserId View');

        // Render is always before show, so we can safely set this false here.
        this.recaptchaLoadInProcess = false;

        // Render should only handle this process when the page is reset using
        // the reset button, which only happens if all the other items are loaded.
        // This should prevent onRender from EVER kicking off the recaptcha on the
        // FIRST render.
        if (_global.recaptchaLoaded === true && this.canLoadRecaptcha === true) {
            this.initRecaptcha();
        }
        
    },

    onReset: function(e) {

        // We have to nuke the entire element to prevent having
        // a bad recaptcha value. The reset function previously
        // wiped out all input values, including the captcha, even
        // if it was already approved. Submittal then caused an error.
        // Re-rendering and init'ing the recaptcha again gives a
        // clean slate to work from.
        this.render();
        this.initRecaptcha();
        
    },
    
    // Callback if the save fails. This will just rerender the view
    // and give an error message at the top.
    // We don't expose anything about the userId, so just give a general error
    onSaveError: function(){

        utils.logger.error('Model save to server failed.');

        // Set a view property that will be a flag for showing an error message
        this.__generalError = [
            'There was an error sending your information to our systems. ',
            'Please try again. If you continue to ',
            'receive this error, please contact us for help.'
        ].join('');

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        // we need to allow submittals again
        this.preventSubmit = false;
        
        // trigger invalid, which will cause a rerender
        this.model.trigger('invalid');

    },

    // This is where we tell the parent view to render a success message
    onSaveSuccess: function(model){

        utils.logger.log('Model save to server succeeded.');

        // this is just cleanup, the view should be destroyed before this matters again
        this.preventSubmit = false;
        
        // Parent layout view listens to these events
        this.model.trigger('change', model);
        
     },

    // Note that onShow only applies to views that are being shown because a
    // region told them to be shown. A normal view would use onRender.
    
    onShow: function() {

        utils.logger.log('In the onShow event of the ForgotPassword UserId View');
        // This should ONLY run when the view is shown by the region
        // The rest of the time, the view itself just renders.
        // We need this flag to make sure the DOM elements exist
        this.canLoadRecaptcha = true;
        this.initRecaptcha();
        
    },

    onSubmitPreventDuplicates: function(e) {

        // Make sure we cannot submit if a request is already in flight
        
        e.preventDefault();

        if (this.preventSubmit === true) {
            
            // prevents another submittal
            return false;
            
        } else {
        
            this.preventSubmit = true;
            this.toggleSubmit(false);
            this.submitForm();
        }

        return true;

    },

    // Then manually trigger an update of the model
    submitForm: function() {

        utils.logger.log('Forgot Password user id submitted');

        var userInput;
        
        // Grab the inputs
        userInput = this.$el.serializeArray() || [];
        if (userInput.length === 0) {
            return false;
        }

        this.updateModel(this.model, userInput);

        return true;
    },

    // element that will hold this view
    tagName: 'form',

    template: forgotPasswordUserIdTemplate,

    templateHelpers: function(){

        // We need to add some attributes to each of the questions
        // to be able to use our helpers
        var current;
        var self = this;

        return {
            __generalError         : self.__generalError,
            RECAPTCHA_LOADING      : SEL.FORGOTPASSWORD.RECAPTCHA_LOADING.substr(1),
            RECAPTCHA_WRAPPER      : SEL.FORGOTPASSWORD.RECAPTCHA_WRAPPER.substr(1),
            RESET                  : SEL.FORGOTPASSWORD.RESET.substr(1),
            SUBMIT                 : SEL.FORGOTPASSWORD.SUBMIT.substr(1),
            SUBMIT_DISABLED        : this.preventSubmit
        };
    },

    toggleSubmit: function(state) {

        if (
                state === null ||
                typeof state !== 'boolean'
        ) {
            utils.logger.error('Cannot toggle state of submit button, bad argument passed.');
            return false;
        }

        utils.logger.log('Toggling submit on the ForgotPassword UserID screen');

        if (this.ui.submit) {
            if (state === true) {
                this.ui.submit.prop('disabled', '');
            } else if (state === false) {
                this.ui.submit.prop('disabled', 'disabled');
            }
        } else {
            return false;
        }
        
    },
    
    ui: {

        loading: SEL.FORGOTPASSWORD.RECAPTCHA_LOADING,
        reset  : SEL.FORGOTPASSWORD.RESET,
        submit : SEL.FORGOTPASSWORD.SUBMIT

    },

    // Take the user info and apply to the model, then try a save
    updateModel: function(model, data) {

        // placeholder var for when we iterate over the questions
        var child;

        // loop through the data and update the model
        // For this view, we ONLY need the captcha info to send,
        // but we need the UserID to know the URL
        // It will be removed on save.
        data.forEach(function(obj){
            if (typeof model.attributes[obj.name] !== 'undefined') {
                model.set(obj.name, obj.value, { silent: true });
            }

            // __RECAPTCHA__
            if (obj.name === 'g-recaptcha-response') {
                model.set('captchaResponse', obj.value, { silent: true });
            }
        });

        // Remove any existing errors that might have been there from a
        // previous attempt at a save.
        if (model.get('errors')) {
            model.set('errors', null, { silent: true });
        }
        
        // Triggering a save will automatically run validation, and stop the
        // save if it fails. When you do a save, you have to be explicit about
        // what data to save. So even though we already updated the model with this
        // data, we have to pass it again.

        // For this particular iteration, we only need to send the captcha information

        this.comm.channel.command(this.comm.commandNameShowLoading);
        
        model.save(model.toJSON(), {
            // set some fields to not be validated.
            checkPassword  : false,
            checkQuestions : false,

            // __RECAPTCHA__
            // added to prevent validation from checking those attributes
            checkCaptcha   : true,
            
            error          : this.onSaveError,
            silent         : true,
            success        : this.onSaveSuccess,
            // do not change model until server gives a success
            wait           : true
        });

    }

});

module.exports = ForgotPasswordUserIdView;
