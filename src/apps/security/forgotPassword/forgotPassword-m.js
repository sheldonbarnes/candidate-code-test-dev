var BaseSecurityModel = require('../models/base-security-m');
var CONST             = require('../const');
var config            = require('../../content/config');
var utils             = require('../../content/utils');
var validation        = require('../validation');

// This model has some custom features stemming from my poor decision not to use a
// MVVM architecture. I thought it would be simpler for this item to just override
// sync as needed. However, turns out that Backbone FetchCache, a plugin we use, has
// issues trying to do that, because it ALSO overrides that method.

// In the end, there is a 'save' override defined in the BaseSecurityModel
// that is called in the view.
// Running a 'save' versus running a sync means we don't hit those issues with
// FetchCache and can still use this approach of keeping errors on the model.

// Unfortunately, this also means that the 4 other flows like this one all have to
// do this same process and define save on each of their own models to reuse this
// flow. Lesson learned - roll with a view model approach for validation and
// then sync the real model if it passes.

var ForgotPasswordModel = BaseSecurityModel.extend({

    defaults: function() {

        return {
            
            captchaResponse     : null,

            confirmNewPassword  : null,

            // Note that we remove errors on save, see initialize below
            // and the custom 'save' function we declare
            errors              : null,
            
            newPassword         : null,

            // Technically, in this flow, we KNOW that these are going to be the questions
            // However, we will actually ASK the system for these in the first step.
            // But in future changes, we will NOT know what these questions are.
            securityQuestions   : [],

            // Note that we remove userId on save, see initialize below
            // and the custom 'save' function we declare
            userId: null
            
        };

    },

    initialize: function(attrs, options) {

        // If you set this, you are effectively saying you want to do a PUT
        // versus doing a POST.
        var defaultIdAttribute = 'userId';
        
        this.serverAttrs = [
            'captchaResponse',
            'newPassword',
            'confirmNewPassword',
            'securityQuestions'
        ];
        var defaultUrlSuffix   = CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.CAPTCHA;

        this.urlSuffix   = defaultUrlSuffix;

        if (options && options.idAttribute) {
            this.idAttribute = options.idAttribute;
        }
        if (options && options.urlSuffix) {
            this.urlSuffix = options.urlSuffix;
        }

        this.hasErrors = false;
    },

    // We can't use the super class definition b/c of the response of the API
    parse: function(response) {

        var data = {};
        var key;
        var testModel = this.defaults();
        var self = this;
        
        if (
            response &&
                response.items &&
                _.isArray(response.items) === true
        ) {

            // We don't really get back a model so pound it into one
            // Make sure whatever we got back matches our definition
            // of the model
            _.each(response.items, function(item, index) {
                
                if (_.isObject(item) === true && _.isArray(item) === false) {
                    
                    for (key in item)  {
                        
                        if (_.has(testModel, key) === true) {
                            data[key] = item[key];
                        }
                        
                    } // for
                    
                } // if

            }); // each

        }

        // Need to put the error messages in the correct place
        if (
            response.errors &&
                _.isArray(response.errors) === true
        ) {

            data.errors = {};
            _.each(response.errors, function(element, index, list) {
                if (element.field === '') {
                    data.errors.__SERVICE = element.code;
                } else {
                    data.errors[element.field] = element.code;
                }
            });
        }
        
        return $.extend(true, {}, data);

    },
    
    url: function() {

        var url = [
            config.securityBasePublicUrl,
            '/user/',
            this.get('userId'),
            '/',
            this.urlSuffix
        ].join('');

        // Prevents needing captcha value passed
        // __RECAPTCHA
        if (config.debug === true) {
            url +='?debug=true';
        }
        
        return url;
    },

    validate: function(attributes, options) {

        // Using a single model here, but depending on the step, only certain fields
        // are required.
        var checkCaptcha   = true;
        var checkPassword  = true;
        var checkQuestions = true;
        var checkUserId    = true;
        
        this.hasErrors = false;

        // what to check
        if (options.checkUserId === false) {
            checkUserId = false;
        }
        if (options.checkCaptcha === false) {
            checkCaptcha = false;
        }
        if (options.checkPassword === false) {
            checkPassword = false;
        }
        if (options.checkQuestions === false) {
            checkQuestions = false;
        }
        
        var current;
        
        // Store the errors per field
        var errors = {};

        if (checkCaptcha === true) {
            if (
                typeof attributes.captchaResponse !== 'string' ||
                    attributes.captchaResponse === ''
            ) {

                this.populateError(errors, 'captcha', validation.messages.required);

            }
        }

        if (checkPassword === true) {

            // comes from the BaseSecurityModel
            this.validatePassword(attributes, errors, validation);
            
        }

        if (checkQuestions === true) {
            
            _.each(attributes.securityQuestions, function(question) {
                current = _.findWhere(attributes.securityQuestions, { id : question.id });
                if (
                    typeof current.answer !== 'string' ||
                        current.answer === ''
                ) {
                    this.populateError(errors, question.id, validation.messages.required);
                }
            });
        }

        if (checkUserId === true) {
            if (
                typeof attributes.userId !== 'string' ||
                    attributes.userId === ''
            ) {
                this.populateError(errors, 'userId', validation.messages.required);
            }

        }

        // no whitespace in userId
        if (
                validation.re.hasWhitespace.test(attributes.userId) === true
        ) {
            this.populateError(errors, 'userId', validation.messages.userIdHasWhitespace);
        }        
        
        if (this.hasErrors) {
            return errors;
        }
    }

});

module.exports = ForgotPasswordModel;
