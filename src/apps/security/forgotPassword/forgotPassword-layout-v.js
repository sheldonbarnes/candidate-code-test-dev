var _global = (function() { return this;})();

var CONST                        = require('../const');
var SEL                          = require('../selectors');

var config                       = require('../../content/config');

var ForgotPasswordErrorView       = require('./forgotPassword-error-v');
var ForgotPasswordModel           = require('./forgotPassword-m');
var ForgotPasswordUserIdView      = require('./forgotPassword-userid-v');
var ForgotPasswordQuestionsView   = require('./forgotPassword-questions-v');
var ForgotPasswordNewPasswordView = require('./forgotPassword-new-password-v');
var ForgotPasswordSuccessView     = require('./forgotPassword-success-v');

var forgotPasswordLayoutTemplate = require('./forgotPassword-layout-t');
var utils                        = require('../../content/utils');

// Backbone will not reload a view if you click on a link to the same hash
// To allow starting the process again, we'll have a link that starts the
// process all over again.
var repeatForgotPasswordSelector = 'click ' + SEL.FORGOTPASSWORD.REPEAT;

var ForgotPasswordLayoutView = Marionette.LayoutView.extend({

    events: function() {
        var hash = {};

        // Will appear after a successful process so the user
        // can repeat it, if desired
        hash[repeatForgotPasswordSelector] = 'repeatForgotPassword';

        return hash;
    },

    hideSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.empty();
    },
    
    // Finally put in the new values
    initForgotPasswordNewPassword: function(data) {

        // Note that we create a new model with the right URL and properties
        // the old one should get cleaned up automatically
        var newPasswordModel = new ForgotPasswordModel(data, {
            urlSuffix: CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.NEW_PASSWORD
        });
        
        // Show a view to get the answers
        this.forgotPasswordNewPasswordView = new ForgotPasswordNewPasswordView({
            comm: {
                channel     : this.forgotPasswordChannel,
                commandNameHideLoading : this.forgotPasswordCommandHideLoading,
                commandNameShowLoading : this.forgotPasswordCommandShowLoading
            },
            model: newPasswordModel
        });

        // This does a listenOnce, so should clean itself up
        this.initModelListener(newPasswordModel);

        // Note that this should destroy the view automatically that was already there
        // and clean everything up.
        this.subSectionRegion.show(this.forgotPasswordNewPasswordView);
        
    },

    // Next step is to anwser the questions we got back
    initForgotPasswordQuestions: function(data) {

        // Note that we create a new model with the right URL and properties
        // the old one should get cleaned up automatically
        var questionsModel = new ForgotPasswordModel(data, {
            urlSuffix: CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.ANSWERS
        });
        
        // Show a view to get the answers
        this.forgotPasswordQuestionsView = new ForgotPasswordQuestionsView({
            comm: {
                channel     : this.forgotPasswordChannel,
                commandNameHideLoading : this.forgotPasswordCommandHideLoading,
                commandNameShowLoading : this.forgotPasswordCommandShowLoading
            },
            model: questionsModel
        });

        // This does a listenOnce, so should clean itself up
        this.initModelListener(questionsModel);

        // Note that this should destroy the view automatically that was already there
        // and clean everything up.
        this.subSectionRegion.show(this.forgotPasswordQuestionsView);
        
    },
    
    initForgotPasswordUserId: function() {

        var userIdModel = new ForgotPasswordModel(null, {
            urlSuffix: CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.CAPTCHA
        });

        // Show a view to get the userId needed, and bring back the questions
        this.forgotPasswordUserIdView = new ForgotPasswordUserIdView({
            model: userIdModel,
            comm: {
                channel     : this.forgotPasswordChannel,
                commandNameHideLoading : this.forgotPasswordCommandHideLoading,
                commandNameShowLoading : this.forgotPasswordCommandShowLoading
            }
        });

        this.initModelListener(userIdModel);
        
        this.subSectionRegion.show(this.forgotPasswordUserIdView);
        
    },

    initModelListener: function(model) {
        // Listen for sync events on the model, and proceed to
        // the next step as needed.
        // Note that only 1 change event will be honored
        this.listenToOnce(model, 'change', this.onModelChange);
    },
    
    initialize: function(options) {

        // These events are callbacks and need scope adjusted
        _.bindAll(this,
                  'onChangeSuccess',
                  'hideSubSectionLoadingView',
                  'showSubSectionLoadingView'
                 );

        // Needed while we get initial data
        if (this.options.LoadingView) {
            this.LoadingView = this.options.LoadingView;
        }

        // initialize the regions for this layout view
        this.regions = {};

        // Set up a channel to allow child views to trigger a sub loading view
        this.forgotPasswordChannel = Backbone.Radio.channel(config.namespace + '-securityforgotpassword');
        this.forgotPasswordCommandShowLoading = CONST.SECURITY.SHOW_LOADING_MSG;
        this.forgotPasswordCommandHideLoading = CONST.SECURITY.HIDE_LOADING_MSG;

        this.forgotPasswordChannel.comply(this.forgotPasswordCommandShowLoading, this.showSubSectionLoadingView);
        this.forgotPasswordChannel.comply(this.forgotPasswordCommandHideLoading, this.hideSubSectionLoadingView);

    },

    // Success callback for the process
    // Triggered by a comply to a command from the child view
    onChangeSuccess: function(msg) {
        
        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();
        
        var data = {
                msg         : CONST.SECURITY.FORGOTPASSWORD.SUCCESS_TXT,
                repeatClass : SEL.FORGOTPASSWORD.REPEAT.substr(1),
                repeatText  : CONST.SECURITY.FORGOTPASSWORD.REPEAT_LINK_TEXT
            };

        this.subSectionRegion.show(new ForgotPasswordSuccessView({ data: data }));

    },

    // Listen for change events caused by saves, then kick off the next step
    // Pass the existing model data around as well.
    onModelChange: function(model) {

        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();
        
        var data = model.toJSON();
        
        if (model && model.urlSuffix) {

            switch (model.urlSuffix) {
            case (CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.CAPTCHA):
                // move to the security answer step
                utils.logger.log('Moving to security questions step');
                this.initForgotPasswordQuestions(data);
                break;

            case (CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.ANSWERS):
                // move to the security answer step
                utils.logger.log('Moving to new password step');
                this.initForgotPasswordNewPassword(data);
                break;

            case (CONST.SECURITY.FORGOTPASSWORD.URL_SUFFIXES.NEW_PASSWORD):
                // move to the security answer step
                utils.logger.log('Successfully changed password');
                this.onChangeSuccess();
                break;
            }
        }
    },
    
    // The main content app will show this view 
    // Once this view has been shown, then various subviews can be rendered for
    // the different processes
    onShow: function(){

        utils.logger.log('ForgotPassword Layout view has been shown');

        // Set up the region that will hold the different views
        this.addRegion('subSectionRegion', SEL.REGIONS.SUBSECTION_JS);
        this.addRegion('subSectionLoadingRegion', SEL.REGIONS.SUBSECTION_LOADING);
        this.showLoadingView();
        this.initForgotPasswordUserId();

    },

    repeatForgotPassword: function(e) {
        e.preventDefault();
        this.subSectionRegion.empty();
        this.showLoadingView();
        this.initForgotPasswordUserId();
    },

    showLoadingView: function() {
        this.subSectionRegion.show(new this.LoadingView());
    },

    showSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.show(new this.LoadingView());
    },

    template: forgotPasswordLayoutTemplate,

    templateHelpers: function(){

        return {

            SUBSECTION_CSS : SEL.REGIONS.SUBSECTION_CSS.substr(1),
            SUBSECTION_JS  : SEL.REGIONS.SUBSECTION_JS.substr(1),
            SUBSECTION_LOADING  : SEL.REGIONS.SUBSECTION_LOADING.substr(1)

        };

    }

});

module.exports = ForgotPasswordLayoutView;
