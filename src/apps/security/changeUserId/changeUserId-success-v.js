var _global = (function() { return this;})();

var changeUserIdSuccessTemplate = require('./changeUserId-success-t');

var ChangeUserIdSuccessView      = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = (options.data ? options.data : null);
    },

    template: changeUserIdSuccessTemplate,

    templateHelpers: function() {

        return {
            MSG          : this.data.msg,
            REPEAT_CLASS : this.data.repeatClass,
            REPEAT_TEXT  : this.data.repeatText
        };
    }

});

module.exports = ChangeUserIdSuccessView;
