var _global = (function() { return this;})();

var changeUserIdErrorTemplate = require('./changeUserId-error-t');
var ChangeUserIdErrorView     = Marionette.ItemView.extend({

    tagName: 'div',

    template: changeUserIdErrorTemplate,

    templateHelpers: function() {

        return {

            // TODO
            // Really need a communication channel to ask for and get back the 
            // needed routes so we don't have to keep passing data or hardcoding 
            // like this
            CONTACT_US: '#contact-us'

        };

    }

});

module.exports = ChangeUserIdErrorView;
