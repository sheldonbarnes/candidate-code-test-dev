var BaseSecurityModel = require('../models/base-security-m');
var CONST             = require('../const');
var config            = require('../../content/config');
var validation        = require('../validation');

var ChangeUserIdModel = BaseSecurityModel.extend({

    defaults: function() {

        return {
            confirmNewUserId    : null,
            errors              : null,
            newUserId           : null,
            password            : null
        };

    },

    initialize: function(attrs, options) {

        // For some reason, this service does NOT want the confirmNewUserId
        // send along.
        var defaultServerAttrs = [
            'newUserId',
            'password'
        ];

        this.serverAttrs = defaultServerAttrs;

        if (options && options.serverAttrs) {
            this.serverAttrs = options.serverAttrs;
        }

        this.hasErrors = false;

    },

    parse: function(response) {

        return this.parseWithReturnObject(response);
    },
    
    url: function() {

        var url = [
            config.securityBaseUrl,
            '/changeUserId.json'
        ].join('');

        return url;
    },

    validate: function(attributes, options) {

        this.hasErrors = false;

        // Store the errors per field
        var errors = {};
        
        // USER ID
        // For each check, only run the check if the field is not populated already

        if (
            !errors.newUserId &&
            typeof attributes.newUserId !== 'string' ||
                attributes.newUserId === ''
        ) {
            this.populateError(errors, 'newUserId', validation.messages.required);
        }

        if (
            !errors.newUserId &&
                // cannot end in special chars
                validation.re.userIdInvalidEndChars.test(attributes.newUserId) === true
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdInvalidEndChars);
        }

        if (
            !errors.newUserId &&
                // cannot be 9 consecutive numbers
                validation.re.userIdOnlyNumbers.test(attributes.newUserId) === true
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdOnlyNumbers);
        }

        if (
            !errors.newUserId &&
                // not allowed chars
                validation.re.userIdInvalidChars.test(attributes.newUserId) === true
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdInvalidChars);
        }
        
        if (
            !errors.newUserId &&
                // cannot be same as password
                attributes.newUserId === attributes.password
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdHasPassword);
        }

        if (
            !errors.newUserId &&
                attributes.newUserId.length < validation.sizes.userId.min
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdTooShort);
        }

        if (
            !errors.newUserId &&
                attributes.newUserId.length > validation.sizes.userId.max
        ) {
            this.populateError(errors, 'newUserId', validation.messages.newUserIdTooLong);
        }

        // CONFIRM NEW USER ID
        if (
            !errors.newUserId &&
                (
                    typeof attributes.confirmNewUserId !== 'string' ||
                        attributes.confirmNewUserId !== attributes.newUserId
                )

        ) {
            this.populateError(errors, 'confirmNewUserId', validation.messages.confirmNewUserId);
        }
        
        // PASSWORD
        if (
            typeof attributes.password !== 'string' ||
                attributes.password === ''
        ) {
            this.populateError(errors, 'password', validation.messages.required);
        }

        
        if (this.hasErrors) {
            return errors;
        }
    }

});

module.exports = ChangeUserIdModel;
