var _global = (function() { return this;})();

var CONST                  = require('../const');
var config                 = require('../../content/config');
var SecurityErrorView      = require('./security-error-v');
var SecurityInfoModel      = require('./security-info-m');
var SecurityInfoView       = require('./security-info-v');
var securityLayoutTemplate = require('./security-layout-t');
var SecuritySuccessView    = require('./security-success-v');

var SEL                    = require('../selectors');
var utils                  = require('../../content/utils');

// Backbone will not reload a view if you click on a link to the same hash
// To allow starting the process again, we'll have a link that starts the
// process all over again.
var repeatChangeSecurityInfoSelector = 'click ' + SEL.CHANGEINFO.REPEAT;

var SecurityLayoutView = Marionette.LayoutView.extend({

    events: function() {
        var hash = {};

        // Will appear after a successful change process to the user
        // can repeat it, if desired
        hash[repeatChangeSecurityInfoSelector] = 'repeatChangeSecurityInfo';

        return hash;
    },

    hideSubSectionLoadingView: function() {
        if (this.subSectionLoadingRegion) {
            this.subSectionLoadingRegion.empty();
        }
    },

    initChangeSecurityInfo: function() {

        // Get the needed data, populate the model and view, then show it
        this.secInfoModel = new SecurityInfoModel();
        this.secInfoModel.fetch({
            cache   : false,
            error   : this.onDataError,
            // Needed to prevent 404 on the route currently
            contentType: 'application/json',
            success : this.onDataReady
        });

    },

    initialize: function(options) {

        // These events are callbacks and need scope adjusted
        _.bindAll(this,
                  'onChangeSuccess',
                  'onDataError',
                  'onDataReady',
                  'hideSubSectionLoadingView',
                  'showSubSectionLoadingView'
                 );

        // Needed while we get initial data
        if (this.options.LoadingView) {
            this.LoadingView = this.options.LoadingView;
        }

        // initialize the regions for this layout view
        this.regions = {};

        // Set up a channel to allow child views to trigger a sub loading view
        this.securityInfoChannel = Backbone.Radio.channel(config.namespace + '-securityinfo');
        this.securityInfoCommandShowLoading = CONST.SECURITY.SHOW_LOADING_MSG;
        this.securityInfoCommandHideLoading = CONST.SECURITY.HIDE_LOADING_MSG;

        this.securityInfoChannel.comply(this.securityInfoCommandShowLoading, this.showSubSectionLoadingView);
        this.securityInfoChannel.comply(this.securityInfoCommandHideLoading, this.hideSubSectionLoadingView);
        
    },

    // Success callback for changing security info
    // Triggered by a comply to a command from the child view
    onChangeSuccess: function(msg) {

        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();
        
        var data = null;

        if (msg && msg === CONST.SECURITY.CHANGEINFO.SUCCESS_MSG) {
            data = {
                msg         : CONST.SECURITY.CHANGEINFO.SUCCESS_TXT,
                repeatClass : SEL.CHANGEINFO.REPEAT.substr(1),
                repeatText  : CONST.SECURITY.CHANGEINFO.REPEAT_LINK_TEXT
            };
            this.subSectionRegion.show(new SecuritySuccessView({ data: data }));
        }

    },

    // Error callback for loading the initial data of existing security questions
    onDataError: function() {

        utils.logger.warn('Change security info could not be fetched, in error callback');
        
        this.hideSubSectionLoadingView();
        
        this.subSectionRegion.show(new SecurityErrorView());
    },

    // Success callback for loading the initial security info data
    onDataReady: function() {

        utils.logger.log('Change security info fetched, in callback');

        this.addRegion('subSectionLoadingRegion', SEL.REGIONS.SUBSECTION_LOADING);
        
        // Needed to allow the change process to trigger success/error callbacks
        // on this view
        var securityInfoCommand = 'securityinfo:change';

        this.securityInfoChannel.comply(securityInfoCommand, this.onChangeSuccess);

        var secInfoView = new SecurityInfoView({
            comm: {
                channel     : this.securityInfoChannel,
                commandName : securityInfoCommand,
                commandNameHideLoading : this.securityInfoCommandHideLoading,
                commandNameShowLoading : this.securityInfoCommandShowLoading
                
            },
            model: this.secInfoModel
        });

        // The actual view that allows changing of the info
        this.subSectionRegion.show(secInfoView);

    },

    // The main content app will show this view for any security related process.
    // Once this view has been shown, then various subviews can be rendered for
    // the different processes, like change id/pw, change security info, etc
    onShow: function(){

        utils.logger.log('Security Layout view has been shown');

        // Set up the region that will hold the different views
        this.addRegion('subSectionRegion', SEL.REGIONS.SUBSECTION_JS);
        
        this.showLoadingView();
        this.initChangeSecurityInfo();

    },

    repeatChangeSecurityInfo: function(e) {
        e.preventDefault();
        this.subSectionRegion.empty();
        this.showLoadingView();
        this.initChangeSecurityInfo();
    },

    showLoadingView: function() {
        this.subSectionRegion.show(new this.LoadingView());
    },

    showSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.show(new this.LoadingView());
    },

    template: securityLayoutTemplate,

    templateHelpers: function(){

        return {

            SUBSECTION_CSS: SEL.REGIONS.SUBSECTION_CSS.substr(1),
            SUBSECTION_JS: SEL.REGIONS.SUBSECTION_JS.substr(1),
            SUBSECTION_LOADING  : SEL.REGIONS.SUBSECTION_LOADING.substr(1)

        };

    }

});

module.exports = SecurityLayoutView;
