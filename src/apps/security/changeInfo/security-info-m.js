var BaseSecurityModel = require('../models/base-security-m');
var config            = require('../../content/config');
var utils             = require('../../content/utils');
var validation        = require('../validation');

// This model has some custom features stemming from my poor decision not to use a
// MVVM architecture. I thought it would be simpler for this item to just override
// sync as needed. However, turns out that Backbone FetchCache, a plugin we use, has
// issues trying to do that, because it ALSO overrides that method.

// In the end, there is a 'save' override defined here that is called in the view.
// Running a 'save' versus running a sync means we don't hit those issues with
// FetchCache and can still use this approach of keeping errors on the model.

// Unfortunately, this also means that the 4 other flows like this one all have to
// do this same process and define save on each of their own models to reuse this
// flow. Lesson learned - roll with a view model approach for validation and
// then sync the real model if it passes.

var SecurityInfoModel = BaseSecurityModel.extend({

    defaults: function() {

        return {
            confirmEmailAddress : null,
            emailAddress        : null,
            // Note that we remove errors on save, see initialize below
            // and the custom 'save' function we declare in the base-m
            // we have extended from.
            errors              : null,
            securityQuestions   : [
                {
                    'id'       : 'mothersMaidenName',
                    'question' : 'Mother\'s Maiden Name',
                    'answer'   : ''
                },
                {
                    'id'       : 'birthState',
                    'question' : 'Birth State',
                    'answer'   : ''
                },
                {
                    'id'       : 'KeywordPhrase',
                    'question' : 'Forgotten Password Question',
                    'answer'   : ''
                },
                {
                    'id'       : 'Keyword',
                    'question' : 'Your Answer',
                    'answer'   : ''
                }
            ]
        };

    },

    initialize: function() {

        // Items we want to send to the server when we do a save
        // See the base-m we extend from.
        this.serverAttrs = [
            'confirmEmailAddress',
            'emailAddress',
            'securityQuestions'
        ];

        // Used in the validation
        this.hasErrors = false;

    },

    parse: function(response) {

        return this.parseBasic(response);

    }, 

    url: function() {
        return config.securityBaseUrl + '/user/securityinfo.json';
    },

    validate: function(attributes, options) {

        this.hasErrors = false;

        var current;
        var errors = {};
        var self = this;

        // EMAIL
        if (
            typeof attributes.emailAddress !== 'string' ||
                attributes.emailAddress === '' ||
                validation.re.email.test(attributes.emailAddress) === false    
        ) {
            this.populateError(errors, 'emailAddress', validation.messages.email);
        }
        
        if (
            !errors.emailAddress &&
                attributes.confirmEmailAddress === ''
        ) {
            this.populateError(errors, 'confirmEmailAddress', validation.messages.required);
        }

        if (
            !errors.confirmEmailAddress &&
                 attributes.emailAddress !== attributes.confirmEmailAddress
        ) {
            this.populateError(errors, 'confirmEmailAddress', validation.messages.confirmEmailAddress);
        }
        
        _.each(attributes.securityQuestions, function(question) {

            // Note that scope changes inside this function, so we're using
            // self here.
            current = _.findWhere(attributes.securityQuestions, { id : question.id });

            if (
                typeof current.answer !== 'string' ||
                    current.answer === '' ||
                    // states list
                    current.answer === '---'
            ) {
                self.populateError(errors, current.id, validation.messages.required);
            }

            if (
                !errors[current.id] &&
                    current.id === 'KeywordPhrase' &&
                    current.answer &&
                    current.answer.length > 50
            ) {
                self.populateError(errors, current.id, validation.messages.questionTooLong);
            }

            if (
                !errors[current.id] &&
                    (
                        current.id === 'Keyword' ||
                            current.id === 'mothersMaidenName'
                    ) &&
                    current.answer &&
                    current.answer.length > 20
            ) {
                self.populateError(errors, current.id, validation.messages.answerTooLong);
            }
            
        });

        if (this.hasErrors) {
            return errors;
        }
    }

});

module.exports = SecurityInfoModel;
