var _global = (function() { return this;})();

var CONST                = require('../const');
var config               = require('../../content/config');
var SEL                  = require('../selectors');
var SecurityInfoModel    = require('./security-info-m');
var securityInfoTemplate = require('./security-info-t');
var utils                = require('../../content/utils');
var validation           = require('../validation');

var SecurityInfoView = Marionette.ItemView.extend({

    // These be set on the containing element that Backbone automatically
    // creates to hold this view
    attributes: {
        'class'  : 'form-horizontal ' + SEL.CHANGEINFO.FORM.substr(1),
        'method' : 'post',
        'role'   : 'form'
    },

    // Submittal of form will kick everything off
    events: {
        'click @ui.submit, submit': 'onSubmit',
        'click @ui.reset         ': 'onReset'
    },

    initialize: function(options) {

        // These are called as part of an async callback, so set their
        // scope manually here.
        _.bindAll(this,
                 'onSaveError',
                 'onSaveSuccess'
                 );

        // The model should be defined when instantiated
        this.model = (options.model ? options.model : null);

        // Setup the Backbone.Radio channel and command to run
        if (
            options &&
                options.comm &&
                options.comm.channel &&
                options.comm.commandName
        ) {
            // Communications channel to talk back to the application
            this.comm = options.comm;
        } else {
            utils.logger.error('No comm object passed to the SecurityInfo view');
        }

    },

    // When a model is validated, Backbone triggers an 'invalid' event if
    // it does not pass. This is our indication that we should rerender, because
    // we want to show the errors.
    modelEvents: {
        'invalid' : 'onInvalid'
    },

    // Handle an invalid event triggered by the model not passing validation
    onInvalid: function() {
        // We read the errors and then redisplay, with the errors rendering
        // this time. model.validationError holds the return value of the
        // model.validate function when it was called.
        this.model.set('errors', this.model.validationError);

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        this.render();
    },

    onReset: function(e) {

        this.$el
            .find('select')
            .first()
            .prop('selectedIndex', 0);
       
        this.$el.find('.error').hide();
        this.$el.find(SEL.INPUTS).attr('value', '');
        
    },
    
    // Callback if the save to the server fails.
    // Could be that we got back errors in the model, could be we got back
    // server level error
    onSaveError: function(model, response, option) {

        var tempObj;
        
        utils.logger.error('Model save to server failed.');

        // Process any 400 error code
        if (
            response &&
                response.responseJSON &&
                response.responseJSON.errors &&
                /400/.test(response.status) === true
        ) {

            // Gets the errors into a state we can mixin to the model
            // This just returns an object, but lets use use the same method
            // in parse to get the errors into place
            tempObj = this.model.parse(response.responseJSON);
            model.set(tempObj, { silent: true });
            
            // we need to allow submittals again
            this.preventSubmit = false;

            this.comm.channel.command(this.comm.commandNameHideLoading);
            
            this.render();
            
        } else {

            // Set a view property that will be a flag for showing an error message
            this.__generalError = [
                'We\'re sorry. The system is unable to process your request.',
                ' Please try submitting your information again. ',
                ' If the problem persists, please contact us for help.'
            ].join('');

            // trigger invalid, which will cause a rerender
            this.model.trigger('invalid');

        }
        
    },

    // This is where we tell the parent view to render a success message
    // The service brings back a 200, even if there is an error
    onSaveSuccess: function(model){

        utils.logger.log('Model save to server succeeded.');
        // trigger an event so the parent layout view can render a success view
        this.comm.channel.command(this.comm.commandName, CONST.SECURITY.CHANGEINFO.SUCCESS_MSG);

    },

    // Then manually trigger an update of the model
    onSubmit: function(e) {

        utils.logger.log('Change security Info submitted');

        var userInput;
        e.preventDefault();

        // Grab the inputs
        userInput = this.$el.serializeArray() || [];
        if (userInput.length === 0) {
            return false;
        }

        this.updateModel(this.model, userInput);

        return true;
    },

    // element that will hold this view
    tagName: 'form',

    template: securityInfoTemplate,

    templateHelpers: function(){

        // We need to add some attributes to each of the questions
        // to be able to use our helpers
        var current;
        var securityQuestionsClone = [];
        var self = this;

        this.model.attributes.securityQuestions.forEach(function(question) {

            // make a clean copy so as to not munge the original data
            question = $.extend(true, {}, question);

            // add any errors that are specific to this item so they can render
            question.ERROR = (self.model.get('errors') ? self.model.get('errors')[question.id] : null);
            // some properties needed for the helpers
            question.FOR = 'for:' + question.id;
            question.NAME = 'name:' + question.id;
            
            if (question.id === 'KeywordPhrase') {
                question.MAXLENGTH = 'maxLength:' + validation.sizes.question.max;
            }
            
            if (question.id === 'birthState') {
                question.states = CONST.STATES;
            }

            if (question.id === 'Keyword' || question.id === 'mothersMaidenName') {
                question.MAXLENGTH = 'maxLength:' + validation.sizes.answer.max;
            }
            
            // add to the array so we can pass it below
            securityQuestionsClone.push(question);
        });

        return {
            __generalError         : self.__generalError,
            QUESTIONS_WRAPPER      : SEL.CHANGEINFO.QUESTIONS_WRAPPER.substr(1),
            RESET                  : SEL.CHANGEINFO.RESET.substr(1),
            SUBMIT                 : SEL.CHANGEINFO.SUBMIT.substr(1),
            SUBMIT_DISABLED        : this.preventSubmit,
            securityQuestionsClone : securityQuestionsClone
        };
    },

    ui: {
        reset  : SEL.CHANGEINFO.RESET,
        submit : SEL.CHANGEINFO.SUBMIT
    },

    // Take the user info and apply to the model, then try a save
    updateModel: function(model, data) {

        // placeholder var for when we iterate over the questions
        var child;

        // loop through the data and update the model
        data.forEach(function(obj){
            if (typeof model.attributes[obj.name] !== 'undefined') {
                model.set(obj.name, obj.value, { silent: true });
            }  else {
                child = _.findWhere(model.attributes.securityQuestions, { id : obj.name });
                if (child) {
                    child.answer = obj.value;
                }
            }
        });

        // Remove any existing errors that might have been there from a
        // previous attempt at a save.
        if (model.get('errors')) {
            model.set('errors', null, { silent: true });
        }
        
        this.comm.channel.command(this.comm.commandNameShowLoading);

        // Triggering a save will automatically run validation, and stop the
        // save if it fails. When you do a save, you have to be explicit about
        // what data to save. So even though we already updated the model with this
        // data, we have to pass it again.
        model.save(model.toJSON(), {
            error   : this.onSaveError,
            success : this.onSaveSuccess,
            wait    : false
        });

    }


});

module.exports = SecurityInfoView;
