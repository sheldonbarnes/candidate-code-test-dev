var BaseCollection        = require('./base-c');
var SecurityQuestionModel = require('./security-question-m');

var SecurityQuestionsCollection = BaseCollection.extend({

    model: SecurityQuestionModel

});

module.exports = SecurityQuestionsCollection;
