// This file should contain any values that might be used in more than one place in the app.
// Rule of thumb - if you have to write it out in more than one place, it belongs here.
// It should always be in alphabetical order.

var CONSTANTS = {

    HELPERS: {

        FORMS: {

            ATTR_DELIMITER: ':'

        }
    },

    RECAPTCHA: {
        
        ERROR_MSG: ' There was an error loading this page. Please try again by refreshing the page. If you continue to see this error, please contact us for help.',

        KEY: '6LemfuMSAAAAANp3Ry4T_0LwQddEyJyo1X9yo3IQ',
        
        KEY_V2: '6LeedAYTAAAAAKUy64bKNPB-pQ0sJd2wHArInhON',

        URL: 'https://www.google.com/recaptcha/api.js?onload=captchaCallback&render=explicit'
        
    },

    SECURITY: {

        CHANGEINFO: {

            REPEAT_LINK_TEXT: 'Change your security information again',

            SUCCESS_MSG: 'changeinfo:save:success',

            SUCCESS_TXT: 'The changes to your account have been received successfully.'

        },

        CHANGEPASSWORD: {

            REPEAT_LINK_TEXT: 'Repeat the "Change your Password" process',

            SECURITY_BLOCK_TEXT: 'Your password was changed successfully. You may now continue browsing.',
            
            SUCCESS_MSG: 'changepassword:save:success',

            SUCCESS_TXT: 'The changes to your account have been received successfully.'

        },
        
        CHANGEUSERID: {

            REPEAT_LINK_TEXT: 'Repeat the "Change your User ID" process',

            SUCCESS_MSG: 'changeuserid:save:success',

            SUCCESS_TXT: 'The changes to your account have been received successfully.'

        },
        
        FORGOTPASSWORD: {

            URL_SUFFIXES: {

                ANSWERS: 'forgotpassword/answers.json',
                
                CAPTCHA: 'forgotpassword/captcha2.json',

                NEW_PASSWORD: 'forgotpassword.json'
            },
            
            REPEAT_LINK_TEXT: 'Repeat the "Forgot your password" process',

            SUCCESS_MSG: 'forgotpassword:save:success',

            SUCCESS_TXT: 'The changes to your account have been received successfully.'

        },

        FORGOTUSERID: {

            REPEAT_LINK_TEXT: 'Repeat the "Forgot your User ID" process',

            SUCCESS_MSG: 'forgotuserid:save:success',

            SUCCESS_TXT: 'If the provided email address matches the one in our system, your User ID will be sent to you.'

        },
        
        // Commands to send to show/hide the subsection loading views
        SHOW_LOADING_MSG: 'security:show:loading',
        
        HIDE_LOADING_MSG: 'security:hide:loading'
        
    },

    STATES: [
        {
            key: '',
            value: '--- Please select a state ---'
        },
        {
            key: "AL",
            value: "Alabama"
        },
        {
            key: "AK",
            value: "Alaska"
        },
        {
            key: "AZ",
            value: "Arizona"
        },
        {
            key: "AR",
            value: "Arkansas"
        },
        {
            key: "CA",
            value: "California"
        },
        {
            key: "CO",
            value: "Colorado"
        },
        {
            key: "CT",
            value: "Connecticut"
        },
        {
            key: "DE",
            value: "Delaware"
        },
        {
            key: "DC",
            value: "District of Columbia"
        },
        {
            key: "FL",
            value: "Florida"
        },
        {
            key: "GA",
            value: "Georgia"
        },
        {
            key: "HI",
            value: "Hawaii"
        },
        {
            key: "ID",
            value: "Idaho"
        },
        {
            key: "IL",
            value: "Illinois"
        },
        {
            key: "IN",
            value: "Indiana"
        },
        {
            key: "IA",
            value: "Iowa"
        },
        {
            key: "KS",
            value: "Kansas"
        },
        {
            key: "KY",
            value: "Kentucky"
        },
        {
            key: "LA",
            value: "Louisiana"
        },
        {
            key: "ME",
            value: "Maine"
        },
        {
            key: "MD",
            value: "Maryland"
        },
        {
            key: "MA",
            value: "Massachusetts"
        },
        {
            key: "MI",
            value: "Michigan"
        },
        {
            key: "MN",
            value: "Minnesota"
        },
        {
            key: "MS",
            value: "Mississippi"
        },
        {
            key: "MO",
            value: "Missouri"
        },
        {
            key: "MT",
            value: "Montana"
        },
        {
            key: "NE",
            value: "Nebraska"
        },
        {
            key: "NV",
            value: "Nevada"
        },
        {
            key: "NH",
            value: "New Hampshire"
        },
        {
            key: "NJ",
            value: "New Jersey"
        },
        {
            key: "NM",
            value: "New Mexico"
        },
        {
            key: "NY",
            value: "New York"
        },
        {
            key: "NC",
            value: "North Carolina"
        },
        {
            key: "ND",
            value: "North Dakota"
        },
        {
            key: "OH",
            value: "Ohio"
        },
        {
            key: "OK",
            value: "Oklahoma"
        },
        {
            key: "OR",
            value: "Oregon"
        },
        {
            key: "PA",
            value: "Pennsylvania"
        },
        {
            key: "RI",
            value: "Rhode Island"
        },
        {
            key: "SC",
            value: "South Carolina"
        },
        {
            key: "SD",
            value: "South Dakota"
        },
        {
            key: "TN",
            value: "Tennessee"
        },
        {
            key: "TX",
            value: "Texas"
        },
        {
            key: "UT",
            value: "Utah"
        },
        {
            key: "VT",
            value: "Vermont"
        },
        {
            key: "VA",
            value: "Virginia"
        },
        {
            key: "WA",
            value: "Washington"
        },
        {
            key: "WV",
            value: "West Virginia"
        },
        {
            key: "WI",
            value: "Wisconsin"
        },
        {
            key: "WY",
            value: "Wyoming"
        },
        {
            key: "OTHER",
            value: "Other"
        }
        
    ]

};

module.exports = CONSTANTS;
