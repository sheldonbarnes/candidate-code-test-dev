var selectors = {

    _SELF: '.js-contentapp-security',

    INPUTS : 'input[type="password"], input[type="text"], input[type="email"]',
    
    REGIONS: {

        CHANGEINFO     : '.js-contentapp-security-changeinfo',
        FORGOTUSERID   : '.js-contentapp-security-forgotuserid',
        
        FORGOTPASSWORD : '.js-contentapp-security-forgotpassword',

        // SUBSECTIONS are regions that can exist in the SECTION items above
        // Example - Security can have several different views
        SUBSECTION_CSS: '.contentapp-region-subsection',
        SUBSECTION_JS: '.js-contentapp-region-subsection',

        // In SUBSECTIONS, might want a loading indicator as well
        SUBSECTION_LOADING: '.js-contentapp-region-subsection-loading'

    },

    CHANGEINFO: {

        FORM              : '.js-contentapp-security-changeinfo-form',

        QUESTIONS_WRAPPER : '.js-contentapp-security-changeinfo-questionsWrapper',

        REPEAT            : '.js-contentapp-security-changeinfo-repeat',

        RESET             : '.js-contentapp-security-changeinfo-reset',
        
        SUBMIT            : '.js-contentapp-security-changeinfo-submit'

    },

    CHANGEPASSWORD: {

        FORM              : '.js-contentapp-security-changepassword-form',

        REPEAT            : '.js-contentapp-security-changepassword-repeat',

        RESET             : '.js-contentapp-security-changepassword-reset',
        
        SUBMIT            : '.js-contentapp-security-changepassword-submit'

    },
    
    CHANGEUSERID: {

        FORM              : '.js-contentapp-security-changeuserid-form',

        QUESTIONS_WRAPPER : '.js-contentapp-security-changeuserid-questionsWrapper',

        REPEAT            : '.js-contentapp-security-changeuserid-repeat',

        RESET             : '.js-contentapp-security-changeuserid-reset',

        SUBMIT            : '.js-contentapp-security-changeuserid-submit'

    },

    FORGOTPASSWORD: {

        FORM              : '.js-contentapp-security-forgotpassword-form',

        QUESTIONS_WRAPPER : '.js-contentapp-security-forgotpassword-questionsWrapper',

        // Recaptcha requires an id versus class
        RECAPTCHA_WRAPPER : '#recaptchaWrapper',
        
        RECAPTCHA_LOADING : '.js-contentapp-security-forgotpassword-recaptchaLoadingMsg',

        REPEAT            : '.js-contentapp-security-forgotpassword-repeat',

        RESET             : '.js-contentapp-security-forgotpassword-reset',
        
        SUBMIT            : '.js-contentapp-security-forgotpassword-submit'

    },

    FORGOTUSERID: {

        FORM              : '.js-contentapp-security-forgotuserid-form',

        QUESTIONS_WRAPPER : '.js-contentapp-security-forgotuserid-questionsWrapper',

        REPEAT            : '.js-contentapp-security-forgotuserid-repeat',

        RESET             : '.js-contentapp-security-forgotuserid-reset',

        SUBMIT            : '.js-contentapp-security-forgotuserid-submit'

    }

};

module.exports = selectors;


