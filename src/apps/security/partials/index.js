var Handlebars = require("hbsfy/runtime");

// Home template
Handlebars.registerPartial('passwordReqs', require('./password-reqs'));

module.exports = Handlebars;
