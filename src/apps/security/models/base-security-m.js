var BaseModel = require('base-m');

function hasTwoCategories(newPassword, validation) {
    // Used for special validation
    var _hasAlpha;
    var _hasNumber;
    var _hasSpecial;

    _hasAlpha = Number(validation.re.hasAlpha.test(newPassword));
    _hasNumber = Number(validation.re.hasNumber.test(newPassword));
    _hasSpecial = Number(validation.re.hasSpecial.test(newPassword));
    
    return (_hasAlpha + _hasNumber + _hasSpecial >= 2);
}

var BaseSecurityModel = BaseModel.extend({

    // The normal REST API interface
    parseBasic: function(response) {

        var data = {};

        if (
            response &&
                response.items &&
                _.isArray(response.items) === true &&
                response.items[0]
        ) {
            data = response.items[0];
        }

        // Need to put the error messages in the correct place
        if (
            response.errors &&
                _.isArray(response.errors) === true
        ) {

            data.errors = {};
            _.each(response.errors, function(element, index, list) {
                if (element.field === '') {
                    data.errors.__SERVICE = element.code;
                } else {
                    data.errors[element.field] = element.code;
                }
            });
        }
        
        return $.extend(true, {}, data);

    },

    // REST API has different interface in some cases
    // Sometimes wraps things in a returnObject item
    parseWithReturnObject: function(response) {

        var obj = null;
        var data = {};
        
        if (
            response &&
                response.returnObject
        ) {

            // Just a mapping to make code shorter
            obj = response.returnObject;

            // This is any actual data
            if (
                obj.items &&
                    _.isArray(obj.items) === true &&
                    obj.items[0]
            ) {

                data = response.returnObject.items[0];

            }

            // Need to put the error messages in the correct place
            if (
                obj.errors &&
                    _.isArray(obj.errors) === true
            ) {

                data.errors = {};
                _.each(obj.errors, function(element, index, list) {
                    if (element.field === '') {
                        data.errors.__SERVICE = element.code;
                    } else {
                        data.errors[element.field] = element.code;
                    }
                });
            }

            return data;
        }

        return {};
        
    },
    
    // Populate a common errors object
    populateError: function(errors, fieldName, msg) {

        errors[fieldName] = msg;

        // Update the scope-appropriate flag for errors existing
        this.hasErrors = true;
        
        return errors;
    },

    // Adam is one of the three smartest people I've ever met
    // http://stackoverflow.com/a/15915111
    save: function (attrs, options) {

        attrs = attrs || this.toJSON();
        options = options || {};

        // If model defines serverAttrs, replace attrs with trimmed version
        if (this.serverAttrs) {
            attrs = _.pick(attrs, this.serverAttrs);
        }

        // Move attrs to options
        options.attrs = attrs;

        // Call super with attrs moved to options
        BaseModel.prototype.save.call(this, attrs, options);
    },

    // Multiple models need to validate passwords, so centralize
    // this here.
    // Note that it returns the errors object so the model gets
    // back the updated version.
    validatePassword: function(attributes, errors, validation) {

        if (
            typeof attributes !== 'object' ||
                typeof errors !== 'object' ||
                typeof validation !== 'object'
        ) {
            return false;
        }

        // User ID might be on the model for the forgotPassword flow
        // or on `this` for the changePassword flow
        var userId = null;

        if (typeof this.userId !== 'undefined') {
            userId = this.userId;
        } else {
            userId = this.get('userId');
        }
        
        // NEW PASSWORD
        // For each check, only run the check if the field is not populated already

        // Empty
        if (
            !errors.newPassword &&
                typeof attributes.newPassword !== 'string' ||
                attributes.newPassword === ''
        ) {
            this.populateError(errors, 'newPassword', validation.messages.required);
            return errors;
        }

        // Length
        if (
            !errors.newPassword &&
                attributes.newPassword.length < validation.sizes.password.min
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordTooShort);
        }

        if (
            !errors.newPassword &&
                attributes.newPassword.length > validation.sizes.password.max
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordTooLong);
        }
        
        // Same as old password
        if (
            !errors.newPassword &&
                attributes.newPassword === attributes.currentPassword
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordSameAsOld);
        }

        // Has whitespace
        if (
            !errors.newPassword &&
                validation.re.hasWhitespace.test(attributes.newPassword) === true
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordHasWhitepace);
        }

        // Has characters that are not allowed
        if (
            !errors.newPassword &&
                validation.re.passwordInvalidChars.test(attributes.newPassword) === true
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordHasInvalidChars);
        }
        
        // Doesn't have 2 of the 3 categories
        if (
            !errors.newPassword &&
                // allowed chars
                (
                    validation.re.passwordAllowedChars.test(attributes.newPassword) === true &&
                        // must match 2 categories
                        hasTwoCategories(attributes.newPassword, validation) !== true
                )
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordNeedsTwoCategories);
        }

        // Has userID
        if (
            !errors.newPassword &&
                new RegExp(userId).test(attributes.newPassword) === true
        ) {
            this.populateError(errors, 'newPassword', validation.messages.newPasswordHasUserId);
        }

        // Don't match
        if (
            attributes.newPassword !== '' &&
                (
                    typeof attributes.confirmNewPassword !== 'string' ||
                        attributes.confirmNewPassword !== attributes.newPassword
                )
        ) {
            this.populateError(errors, 'confirmNewPassword', validation.messages.confirmNewPassword);
        }
        
        // CURRENT PASSWORD
        if (
            // if they forgot the password, this field is not present
            // So only check if it's in the attributes
            typeof attributes.currentPassword !== 'undefined' &&
                // If we get here, then there is a current password field to check
                (
                    typeof attributes.currentPassword !== 'string' ||
                        attributes.currentPassword === ''
                )
        ) {
            this.populateError(errors, 'currentPassword', validation.messages.required);
        }

        return errors;
    }

});

module.exports = BaseSecurityModel;
