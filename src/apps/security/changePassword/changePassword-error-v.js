var _global = (function() { return this;})();

var changePasswordErrorTemplate = require('./changePassword-error-t');

var ChangePasswordErrorView     = Marionette.ItemView.extend({

    tagName: 'div',

    template: changePasswordErrorTemplate,

    templateHelpers: function() {

        return {

            // TODO
            // Really need a communication channel to ask for and get back the 
            // needed routes so we don't have to keep passing data or hardcoding 
            // like this
            CONTACT_US: '#contact-us'

        };

    }

});

module.exports = ChangePasswordErrorView;
