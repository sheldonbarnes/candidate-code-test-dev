var _global = (function() { return this;})();

var changePasswordSuccessTemplate = require('./changePassword-success-t');

var ChangePasswordSuccessView      = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = (options.data ? options.data : null);
    },

    template: changePasswordSuccessTemplate,

    templateHelpers: function() {

        return {
            MSG                 : this.data.msg,
            REPEAT_CLASS        : this.data.repeatClass,
            REPEAT_TEXT         : this.data.repeatText
        };
    }

});

module.exports = ChangePasswordSuccessView;
