var BaseSecurityModel = require('../models/base-security-m');
var config            = require('../../content/config');
var CONST             = require('../const');
var validation        = require('../validation');

var ChangePasswordModel = BaseSecurityModel.extend({

    defaults: function() {

        return {
            confirmNewPassword : null,
            currentPassword    : null,
            newPassword        : null
        };

    },

    initialize: function(attrs, options) {

        var defaultServerAttrs = [
            'confirmNewPassword',
            'currentPassword',
            'newPassword'
        ];

        this.serverAttrs = defaultServerAttrs;
        this.userId = null;
        
        if (options && options.serverAttrs) {
            this.serverAttrs = options.serverAttrs;
        }

        if (options && options.userId) {
            this.userId = options.userId;
        }

        this.hasErrors = false;
        
    },

    parse: function(response) {

        return this.parseWithReturnObject(response);
        
    },
    
    url: function() {

        var url = [
            config.securityBaseUrl,
            '/changePassword.json'
        ].join('');

        return url;
    },

    validate: function(attributes, options) {

        this.hasErrors = false;

        // Store the errors per field
        var errors = {};

        this.validatePassword(attributes, errors, validation);
            
        // DONE
        if (this.hasErrors) {
            return errors;
        }
    }

});

module.exports = ChangePasswordModel;
