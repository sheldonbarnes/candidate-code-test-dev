var _global = (function() { return this;})();

var CONST                      = require('../const');
var SEL                        = require('../selectors');
var config                     = require('../../content/config');

var changePasswordLayoutTemplate = require('./changePassword-layout-t');
var ChangePasswordModel          = require('./changePassword-m');
var ChangePasswordView           = require('./changePassword-password-v');
var ChangePasswordSuccessView    = require('./changePassword-success-v');
var utils                        = require('../../content/utils');

// Backbone will not reload a view if you click on a link to the same hash
// To allow starting the process again, we'll have a link that starts the
// process all over again.
var repeatChangePasswordSelector = 'click ' + SEL.CHANGEPASSWORD.REPEAT;

var ChangePasswordLayoutView = Marionette.LayoutView.extend({

    events: function() {
        var hash = {};

        // Will appear after a successful process so the user
        // can repeat it, if desired
        hash[repeatChangePasswordSelector] = 'repeatChangePassword';

        return hash;
    },

    hideSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.empty();
    },

    initChangePassword: function() {

        var passwordModel = new ChangePasswordModel(null, { userId: this.userId });

        this.changePasswordView = new ChangePasswordView({
            comm: {
                channel     : this.changePasswordChannel,
                commandNameHideLoading : this.changePasswordCommandHideLoading,
                commandNameShowLoading : this.changePasswordCommandShowLoading
            },
            model: passwordModel
        });

        this.initModelListener(passwordModel);
        
        this.subSectionRegion.show(this.changePasswordView);
        
    },
    
    initModelListener: function(model) {
        // Note that only 1 change event will be honored
        this.listenToOnce(model, 'change', this.onModelChange);
    },
    
    initialize: function(options) {

        // These events are callbacks and need scope adjusted
        _.bindAll(this,
                  'onRequestSuccess',
                  'hideSubSectionLoadingView',
                  'showSubSectionLoadingView'
                 );

        this.state = options.state;
        // Needed while we get initial data
        if (this.options.LoadingView) {
            this.LoadingView = this.options.LoadingView;
        }

        if (this.options.user) {
            // Needed for checking the password change against it
            this.userId = this.options.user.get('userId');
        }
        
        // initialize the regions for this layout view
        this.regions = {};

        // Set up a channel to allow child views to trigger a sub loading view
        this.changePasswordChannel = Backbone.Radio.channel(config.namespace + '-securitychangepassword');
        this.changePasswordCommandShowLoading = CONST.SECURITY.SHOW_LOADING_MSG;
        this.changePasswordCommandHideLoading = CONST.SECURITY.HIDE_LOADING_MSG;

        this.changePasswordChannel.comply(this.changePasswordCommandShowLoading, this.showSubSectionLoadingView);
        this.changePasswordChannel.comply(this.changePasswordCommandHideLoading, this.hideSubSectionLoadingView);

        
    },

    // Success callback for the process
    // Triggered by a comply to a command from the child view
    onRequestSuccess: function(msg) {
        
        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();
        
        var data = {
                msg         : CONST.SECURITY.CHANGEPASSWORD.SUCCESS_TXT,
                repeatClass : SEL.CHANGEPASSWORD.REPEAT.substr(1),
                repeatText  : CONST.SECURITY.CHANGEPASSWORD.REPEAT_LINK_TEXT
            };

        if (this.state && this.state.securityBlock) {
            this.$el.find('.error').remove();
            delete data.repeatClass;
            delete data.Text;
            data.msg = CONST.SECURITY.CHANGEPASSWORD.SECURITY_BLOCK_TEXT;
        }
        
        this.subSectionRegion.show(new ChangePasswordSuccessView({ data: data }));
        

    },

    // Listen for change events caused by saves, then kick off the next step
    // Pass the existing model data around as well.
    onModelChange: function(model) {

        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();

        utils.logger.log('Successfully completed change password process');
        this.onRequestSuccess();
        
    },
    
    // The main content app will show this view 
    // Once this view has been shown, then various subviews can be rendered for
    // the different processes
    onShow: function(){

        utils.logger.log('ChangePassword Layout view has been shown');

        // Set up the region that will hold the different views
        this.addRegion('subSectionRegion', SEL.REGIONS.SUBSECTION_JS);
        this.addRegion('subSectionLoadingRegion', SEL.REGIONS.SUBSECTION_LOADING);
        this.showLoadingView();
        this.initChangePassword();

    },

    repeatChangePassword: function(e) {
        e.preventDefault();
        this.subSectionRegion.empty();
        this.showLoadingView();
        this.initChangePassword();
    },

    showLoadingView: function() {
        this.subSectionRegion.show(new this.LoadingView());
    },

    showSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.show(new this.LoadingView());
    },

    template: changePasswordLayoutTemplate,

    templateHelpers: function(){

        return {

            __generalError     : (this.state.__generalError ? this.state.__generalError : null),
            SUBSECTION_CSS     : SEL.REGIONS.SUBSECTION_CSS.substr(1),
            SUBSECTION_JS      : SEL.REGIONS.SUBSECTION_JS.substr(1),
            SUBSECTION_LOADING : SEL.REGIONS.SUBSECTION_LOADING.substr(1)

        };

    }

});

module.exports = ChangePasswordLayoutView;
