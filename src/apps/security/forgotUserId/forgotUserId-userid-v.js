var _global = (function() { return this;})();

var CONST                = require('../const');
var config               = require('../../content/config');
var SEL                  = require('../selectors');
var forgotUserIdTemplate = require('./forgotUserId-userid-t');
var utils                = require('../../content/utils');

var ForgotUserIdView = Marionette.ItemView.extend({

    // These be set on the containing element that Backbone automatically
    // creates to hold this view
    attributes: {
        'class'  : 'form-horizontal ' + SEL.FORGOTUSERID.FORM.substr(1),
        'method' : 'post',
        'role'   : 'form'
    },

    // Submittal of form will kick everything off
    events: {
        'click @ui.submit, submit': 'onSubmitPreventDuplicates',
        'click @ui.reset'         : 'onReset'
    },

    initialize: function(options) {

        // These are called as part of an async callback, so set their
        // scope manually here.
        _.bindAll(this,
                  'onSaveError',
                  'onSaveSuccess'
                 );

        // The model should be defined when instantiated
        this.model = (options.model ? options.model : null);

        // a check to prevent multiple submits
        // We could bind to change events on this, but for now we manually toggle
        // via another function.
        this.preventSubmit = false;

        this.comm = options.comm;

    },

    // When a model is validated, Backbone triggers an 'invalid' event if
    // it does not pass. This is our indication that we should rerender, because
    // we want to show the errors.
    modelEvents: {
        'invalid' : 'onInvalid'
    },

    // Handle an invalid event triggered by the model not passing validation
    onInvalid: function() {

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        // We read the errors and then redisplay, with the errors rendering
        // this time. model.validationError holds the return value of the
        // model.validate function when it was called.
        this.model.set('errors', this.model.validationError, { silent: true });

        // allow submissions again
        if (this.preventSubmit === true) {
            this.preventSubmit = false;
        }
        this.render();
    },

    onReset: function(e) {

        this.$el.find('.error').hide();
        this.$el.find(SEL.INPUTS).attr('value', '');
        
    },
    
    // Callback if the call to SM fails. This will just rerender the view
    // and give an error message at the top.
    // Note that this means that SM didn't respond, or threw a non 2xx/3xx
    // Normal SM response is a 2xx with an error code if it fails
    onSaveError: function(){

        utils.logger.error('POST to server failed.');

        // Set a view property that will be a flag for showing an error message
        this.__generalError = [
            'The system was unable to provide the information you requested. ',
            'If the problem persists, please contact us using the link in the navigation menu.'
        ].join('');

        this.comm.channel.command(this.comm.commandNameHideLoading);
        
        // we need to allow submittals again
        this.preventSubmit = false;
        
        // trigger invalid, which will cause a rerender
        this.model.trigger('invalid');

    },

    // This could be a successful change, or it could be a failure
    // The service always returns 200 even if there are validation errors
    // Because it is a security flaw to tell the user if the email was valid
    // just show a success message
    onSaveSuccess: function(data){

        utils.logger.log('Forgot userID request completed successfully');

        // this is just cleanup, the view should be destroyed before this matters again
        this.preventSubmit = false;
        
        // Parent layout view listens to these events
        this.model.trigger('change');
        
     },

    onSubmitPreventDuplicates: function(e) {

        // Make sure we cannot submit if a request is already in flight
        
        e.preventDefault();

        if (this.preventSubmit === true) {
            
            // prevents another submittal
            return false;
            
        } else {
        
            this.preventSubmit = true;
            this.toggleSubmit(false);
            this.submitForm();
        }

        return true;

    },

    // Then manually trigger an update of the model
    submitForm: function() {

        utils.logger.log('Forgot user id submitted');

        var userInput;
        
        // Grab the inputs
        userInput = this.$el.serializeArray() || [];
        if (userInput.length === 0) {
            return false;
        }

        this.updateModel(this.model, userInput);

        return true;
    },

    // element that will hold this view
    tagName: 'form',

    template: forgotUserIdTemplate,

    templateHelpers: function(){

        return {
            __generalError         : this.__generalError,
            RESET                  : SEL.FORGOTUSERID.RESET.substr(1),
            SUBMIT                 : SEL.FORGOTUSERID.SUBMIT.substr(1),
            SUBMIT_DISABLED        : this.preventSubmit
        };
    },

    toggleSubmit: function(state) {

        if (
                state === null ||
                typeof state !== 'boolean'
        ) {
            utils.logger.error('Cannot toggle state of submit button, bad argument passed.');
            return false;
        }

        utils.logger.log('Toggling submit on the Forgot UserID screen');

        if (this.ui.submit) {
            if (state === true) {
                this.ui.submit.prop('disabled', '');
            } else if (state === false) {
                this.ui.submit.prop('disabled', 'disabled');
            }
        } else {
            return false;
        }
        
    },
    
    ui: {
        
        reset  : SEL.FORGOTUSERID.RESET,
        submit : SEL.FORGOTUSERID.SUBMIT

    },

    // Take the user info and apply to the model
    // Then just directly send the data along.
    // Doesn't really make a lot of sense to use a model here
    // But we have the existing flow in place
    updateModel: function(model, data) {

        var url;

        // loop through the data and update the model
        data.forEach(function(obj){
            if (typeof model.attributes[obj.name] !== 'undefined') {
                model.set(obj.name, obj.value, { silent: true });
            }
        });

        if (model.isValid() === true) {
            
            this.comm.channel.command(this.comm.commandNameShowLoading);

            url = [
                // not sure why this doesn't use /rest
                config.securityBasePublicUrl.split('/rest')[0],
                '/forgotUserId.json?emailAddress=',
                encodeURIComponent(model.get('emailAddress'))
            ].join('');
            
            $.ajax({
                dataType: 'json',
                error   : this.onSaveError,
                success : this.onSaveSuccess,
                url     : url
            });
            
        }
        
    }

});

module.exports = ForgotUserIdView;
