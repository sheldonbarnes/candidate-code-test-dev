var _global = (function() { return this;})();

var securityErrorTemplate = require('./forgotUserId-error-t');
var SecurityErrorView     = Marionette.ItemView.extend({

    tagName: 'div',

    template: securityErrorTemplate,

    templateHelpers: function() {

        return {

            // TODO
            // Really need a communication channel to ask for and get back the 
            // needed routes so we don't have to keep passing data or hardcoding 
            // like this
            CONTACT_US: '#contact-us'

        };

    }

});

module.exports = SecurityErrorView;
