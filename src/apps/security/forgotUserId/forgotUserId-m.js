var BaseSecurityModel = require('../models/base-security-m');
var validation        = require('../validation');

// This isn't really a useful model, but with a service like this, we don't
// want to expose whether the email address submitted was a valid email or not.
// Eventually, we should change the status codes returned here.
var ForgotUserIdModel = BaseSecurityModel.extend({

    defaults: function() {

        return {
            emailAddress: null
        };

    },

    validate: function(attributes, options) {

        this.hasErrors = false;

        var current;
        var errors = {};

            if (
                typeof attributes.emailAddress !== 'string' ||
                    attributes.emailAddress === '' ||
                    validation.re.email.test(attributes.emailAddress) === false    
            ) {
                this.populateError(errors, 'emailAddress', validation.messages.email);
            }
        
        if (this.hasErrors) {
            return errors;
        }
    }

});

module.exports = ForgotUserIdModel;
