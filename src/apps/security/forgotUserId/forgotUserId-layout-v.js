var _global = (function() { return this;})();

var CONST                      = require('../const');
var SEL                        = require('../selectors');
var config                     = require('../../content/config');

var ForgotUserIdModel          = require('./forgotUserId-m');
var ForgotUserIdView           = require('./forgotUserId-userid-v');
var forgotUserIdLayoutTemplate = require('./forgotUserId-layout-t');
var ForgotUserIdSuccessView    = require('./forgotUserId-success-v');
var utils                      = require('../../content/utils');

// Backbone will not reload a view if you click on a link to the same hash
// To allow starting the process again, we'll have a link that starts the
// process all over again.
var repeatForgotUserIdSelector = 'click ' + SEL.FORGOTUSERID.REPEAT;

var ForgotUserIdLayoutView = Marionette.LayoutView.extend({

    events: function() {
        var hash = {};

        // Will appear after a successful process so the user
        // can repeat it, if desired
        hash[repeatForgotUserIdSelector] = 'repeatForgotUserId';

        return hash;
    },

    hideSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.empty();
    },

    initForgotUserId: function() {

        var userIdModel = new ForgotUserIdModel();

        this.forgotUserIdView = new ForgotUserIdView({
            comm: {
                channel     : this.forgotUserIdChannel,
                commandNameHideLoading : this.forgotUserIdCommandHideLoading,
                commandNameShowLoading : this.forgotUserIdCommandShowLoading
            },
            model: userIdModel
        });

        this.initModelListener(userIdModel);
        
        this.subSectionRegion.show(this.forgotUserIdView);
        
    },
    
    initModelListener: function(model) {
        // Note that only 1 change event will be honored
        this.listenToOnce(model, 'change', this.onModelChange);
    },
    
    initialize: function(options) {

        // These events are callbacks and need scope adjusted
        _.bindAll(this,
                  'onRequestSuccess',
                  'hideSubSectionLoadingView',
                  'showSubSectionLoadingView'
                 );

        // Needed while we get initial data
        if (this.options.LoadingView) {
            this.LoadingView = this.options.LoadingView;
        }

        // initialize the regions for this layout view
        this.regions = {};

        // Set up a channel to allow child views to trigger a sub loading view
        this.forgotUserIdChannel = Backbone.Radio.channel(config.namespace + '-securityforgotuserid');
        this.forgotUserIdCommandShowLoading = CONST.SECURITY.SHOW_LOADING_MSG;
        this.forgotUserIdCommandHideLoading = CONST.SECURITY.HIDE_LOADING_MSG;

        this.forgotUserIdChannel.comply(this.forgotUserIdCommandShowLoading, this.showSubSectionLoadingView);
        this.forgotUserIdChannel.comply(this.forgotUserIdCommandHideLoading, this.hideSubSectionLoadingView);

        
    },

    // Success callback for the process
    // Triggered by a comply to a command from the child view
    onRequestSuccess: function(msg) {
        
        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();
        
        var data = {
                msg         : CONST.SECURITY.FORGOTUSERID.SUCCESS_TXT,
                repeatClass : SEL.FORGOTUSERID.REPEAT.substr(1),
                repeatText  : CONST.SECURITY.FORGOTUSERID.REPEAT_LINK_TEXT
            };

        this.subSectionRegion.show(new ForgotUserIdSuccessView({ data: data }));

    },

    // Listen for change events caused by saves, then kick off the next step
    // Pass the existing model data around as well.
    onModelChange: function(model) {

        // hide any loading messages from the previous step
        this.hideSubSectionLoadingView();

        utils.logger.log('Successfully completed forgot userid process');
        this.onRequestSuccess();
        
    },
    
    // The main content app will show this view 
    // Once this view has been shown, then various subviews can be rendered for
    // the different processes
    onShow: function(){

        utils.logger.log('ForgotUserId Layout view has been shown');

        // Set up the region that will hold the different views
        this.addRegion('subSectionRegion', SEL.REGIONS.SUBSECTION_JS);
        this.addRegion('subSectionLoadingRegion', SEL.REGIONS.SUBSECTION_LOADING);
        this.showLoadingView();
        this.initForgotUserId();

    },

    repeatForgotUserId: function(e) {
        e.preventDefault();
        this.subSectionRegion.empty();
        this.showLoadingView();
        this.initForgotUserId();
    },

    showLoadingView: function() {
        this.subSectionRegion.show(new this.LoadingView());
    },

    showSubSectionLoadingView: function() {
        this.subSectionLoadingRegion.show(new this.LoadingView());
    },

    template: forgotUserIdLayoutTemplate,

    templateHelpers: function(){

        return {

            SUBSECTION_CSS : SEL.REGIONS.SUBSECTION_CSS.substr(1),
            SUBSECTION_JS  : SEL.REGIONS.SUBSECTION_JS.substr(1),
            SUBSECTION_LOADING  : SEL.REGIONS.SUBSECTION_LOADING.substr(1)

        };

    }

});

module.exports = ForgotUserIdLayoutView;
