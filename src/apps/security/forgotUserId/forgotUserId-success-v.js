var _global = (function() { return this;})();

var securitySuccessTemplate = require('./forgotUserId-success-t');

var SecuritySuccessView      = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = (options.data ? options.data : null);
    },

    template: securitySuccessTemplate,

    templateHelpers: function() {

        return {
            MSG          : this.data.msg,
            REPEAT_CLASS : this.data.repeatClass,
            REPEAT_TEXT  : this.data.repeatText
        };
    }

});

module.exports = SecuritySuccessView;
