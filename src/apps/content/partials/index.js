var Handlebars = require("hbsfy/runtime");

// TODO
// Should be able to write this file out using a gulp task
// So that it's created on build, and just reads the directory

// Home template
Handlebars.registerPartial('content-image', require('./content-image'));
Handlebars.registerPartial('content-basic-content', require('./content-basic-content'));
Handlebars.registerPartial('content-quicklinks', require('./content-quicklinks'));

// Product template
Handlebars.registerPartial('content-section-content', require('./content-section-content'));
Handlebars.registerPartial('content-subsection-content', require('./content-subsection-content'));
Handlebars.registerPartial('content-media-content', require('./content-media-content'));

// Forms template

// Faq template

// Training Template


module.exports = Handlebars;
