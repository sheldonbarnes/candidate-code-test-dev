var _global = (function() { return this;})();

var CONST               = require('../const');
var config              = require('../config');

var BenefitsStartView      = require('./benefits-start-v');
var benefitsLayoutTemplate = require('./benefits-layout-t');

var SEL   = require('../selectors');
var utils = require('../utils');

var BenefitsLayoutView = Marionette.LayoutView.extend({

    initialize: function(options) {

        // Needed while we get initial data
        if (this.options.LoadingView) {
            this.LoadingView = this.options.LoadingView;
        }

        // initialize the regions for this layout view
        this.regions = {};

    },

    initBenefits: function() {

        var BenefitsExView = new BenefitsStartView();
        this.subSectionRegion.show(BenefitsExView);

    },

    onShow: function(){

        utils.logger.log('Benefits Layout view has been shown');

        // Set up the region that will hold the different views
        this.addRegion('subSectionRegion', SEL.REGIONS.SUBSECTION_JS);
        this.showLoadingView();
        this.initBenefits();

    },

    showLoadingView: function() {
        this.subSectionRegion.show(new this.LoadingView());
    },

    template: benefitsLayoutTemplate,

    templateHelpers: function(){

        return {

            SUBSECTION_CSS : SEL.REGIONS.SUBSECTION_CSS.substr(1),
            SUBSECTION_JS  : SEL.REGIONS.SUBSECTION_JS.substr(1)

        };

    }

});

module.exports = BenefitsLayoutView;
