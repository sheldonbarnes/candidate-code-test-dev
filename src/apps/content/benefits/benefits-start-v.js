var _global = (function() { return this;})();

var benefitsStartTemplate = require('./benefits-start-t');

var BenefitsStartView = Marionette.ItemView.extend({


    onShow: function() {
        // use the scrollX option, don't do vertical scrolling
        // let the table layout on the page as needed.
        // http://datatables.net/examples/basic_init/scroll_x.html
        this.ui.dt.dataTable({
            scrollX: true
        });
    },
    
    template: benefitsStartTemplate,

    ui: {
        dt: '.exampleTable'
    }

});

module.exports = BenefitsStartView;
