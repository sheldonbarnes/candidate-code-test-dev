var selectors = {

    BENEFITS: {

        _SELF: '.js-contentapp-benefits',

        REGIONS: {

            HOME: '.js-contentapp-benefits-home'

        }

    },
    
    CONTENT: '.js-contentapp',

    LOGIN: {

        LOGIN_FORM: '.js-contentapp-login-form',

        RESET: '.js-contentapp-login-reset',
        
        SUBMIT: '.js-contentapp-login-submit'
        
    },

    REGIONS: {

        // SECTION is a main content section
        SECTION_CSS: '.contentapp-region-section',
        SECTION_JS: '.js-contentapp-region-section',

        // SUBSECTIONS are regions that can exist in the SECTION items above
        // Example - Security can have several different views
        SUBSECTION_CSS: '.contentapp-region-subsection',
        SUBSECTION_JS: '.js-contentapp-region-subsection',

        // In SUBSECTIONS, might want a loading indicator as well
        SUBSECTION_LOADING: '.js-contentapp-region-subsection-loading'

    },

    SELECT_PRODUCER: {

        PRODUCER_LINK: '.js-contentapp-selectproducer-link',
        
        WRAPPER: '.js-contentapp-selectproducer-wrapper'
        
    }

};

module.exports = selectors;
