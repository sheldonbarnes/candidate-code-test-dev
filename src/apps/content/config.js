var _global = (function() { return this;})();

// Will be overridde by preprocessing in browserify
// Can check for a value of 'true' to mean we are in DEV mode
var debugValue = '/* @echo DEV */';
debugValue = (debugValue === 'true' ? true : false );

// Only place values in here that you will allow to be overridden
// For other values, place in the protectedConfig
var config = {

    debug   : debugValue,
    // SiteMinder params
    SMAUTHREASON: null

};

// Values placed here will not be overridden by query params
var protectedConfig = {

    console               : !!_global.console,
    namespace             : 'EBEN_CONTENT',
    securityBaseUrl       : (config.debug ? '/employeebenefits' : '') + '/oasec/secure/rest',
    securityBasePublicUrl : (config.debug ? '/employeebenefits' : '') + '/oasec/public/rest'

};

// We don't want to drop just any query param value into our config
// So build a list of the values we will permit, consisting of the config and protectedConfig
// keys
var permitted = _.keys(protectedConfig);
permitted = permitted.concat(_.keys(config));


function parseQueryParams(params) {

    // return value
    var config = {};
    // looping vars
    var i, len, current;
    // temp variables for dealing with name/value pairs
    var name, value;

    if (!params) {
        return config;
    }

    if (/^\?/.test(params) === true) {
        params = params.substr(1);
    }

    // get a list of all values that were passed
    if (params.length > 0) {

        // check for encoded html
        if (/&amp;/.test(params) === true) {
            params = params.replace('&amp;', '&');
        }

        // break up into name/value pairs
        params = params.split('&');
        len = params.length;
        for (i = 0; i < len; i++) {

            // now split into name/value
            current = params[i];
            current = current.split('=');

            if (current.length > 0) {

                name = current[0];
                value = current[1];

                // convert to real booleans
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                } else {
                    // Don't want to re-encode here, but set to strings again
                    // just in case of references to scoped variables
                    // TODO need to really understand security implications here
                    value = value + '';
                }

                // Do not allow properties to be overridden
                if (
                    typeof config[value] === 'undefined' &&
                        _.has(config, value) === false
                ) {
                    config[name] = value;
                }

            } else {

                // If here, wasn't a valid foo=bar
                continue;

            } // if/else

        } // for loop

    } // for loop over params.length

    return config;

}

// Singleton
module.exports = (function() {
    var runtimeConfig = _.pick(parseQueryParams(_global.location.search), permitted);
    config = _.extend({}, config, runtimeConfig, protectedConfig);
    return config;
})();
