// Returns a boolean to determine if this content item
// should be seen by the passed role
module.exports = function(dataSource, role) {

    if (_.isArray(dataSource) === false) {
        return false;
    }

    if (typeof role !== 'string') {
        return false;
    }

    var filteredData = [];

    var itemCheck = function(item) {

        var rolesList = null;

        // This is the convention
        var permitList = item.permit;
        var denyList = item.deny;

        // These fields do not exist, so show it all
        if (!permitList && !denyList) {
            return true;
        }

        // Fields exist, but no entries in either
        if (permitList.length === denyList.length === 0) {
            return true;
        }

        // Explicit deny for this role
        if (denyList.length && denyList.indexOf(role) !== -1) {
            return false;
        }
        // Explicit permit for this role
        if (permitList.length) {

            rolesList = permitList.split(' ');

            // if you are in the list, you can see it
            if (rolesList.indexOf(role) !== -1) {
                return true;
            } else {
                // and if you aren't in the list, GET OUT
                return false;
            }

        }


        // If we got to here, there is no explicit rule in place for this role
        // so we can just allow it to show.
        return true;
    };

    _.each(dataSource, function(item) {
        if (itemCheck(item) === true) {
            filteredData.push(item);
        }
    });

    return filteredData;
};
