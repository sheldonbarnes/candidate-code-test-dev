var CONST      = require('./const');
var DEL        = CONST.HELPERS.FORMS.ATTR_DELIMITER;
var Handlebars = require("hbsfy/runtime");

/*
 * @param {String} value Value for the HTML input that will be returned
 * @param {String} optional Pass in additional strings like 'foo:bar'
 * @returns String
 *
 * Given a value, and then optional strings like 'foo:bar', will return
 * <input value="VALUE" type="TYPE" foo="bar" />
 */
function genericInput(type, value) {
    var args = Array.prototype.slice.call(arguments);
    // remove the type and value
    args.splice(0,2);
    
    return [
        '<input type="' + type + '"',
        (value ? 'value="' + value + '"' : ''),
        parseAttributes(args),
        ' />'
    ].join('');
}

/*
 * @param {Array or arguments} attrs Either the arguments object or an array of values
 * @returns String
 *
 * This function takes a set of values like 'id:foo' and writes out html like 'id="foo"'
 * The ':' is the DEL required in above
 *
 * For some special values like 'required', just writes out 'required'
 *
 */
function parseAttributes (attrs) {
    var str = '';
    if (!attrs) {
        return str;
    }
    // Can be called with either an array of values, or the arguments object
    if (_.isArray(attrs) !== true) {
        attrs = Array.prototype.slice.call(attrs);
    }

    // get rid of Handlebars data object
    attrs.pop();
    attrs.sort();

    // List any special values here that  are of the
    // form 'foo' vs 'foo="bar"'
    var specialAttrs = [ 'required' ];

    var i;
    var len = attrs.length;
    var propName;
    var propValue;
    var value;

    // for each string like 'foo:bar', write out 'foo="bar"' in the html
    for (i = 0; i < len; i++) {

        // null or missing values
        if (!attrs[i]) {
            continue;
        }

        value = attrs[i].split(DEL);
        propName = value.shift();
        propValue = value.join('');
        if (propValue === '' && specialAttrs.indexOf(propName) !== -1) {
            str += ' '  + propName + ' ';
        } else {
            str += ' ' + propName + '="' + propValue + '"';
        }
    }
    return str;
}


Handlebars.registerHelper({

    // Use this when in an #each loop when you need a conditional class
    activeCheck: function(criteria) {
        if (criteria === true) {
            return ' active';
        }
        return '';
    },

    mediaBlocks : function(blocks, options) {


        if (!blocks) {
            return '';
        }

        if (_.isArray(blocks) !== true) {
            return '';
        }

        var str = '';
        var row = '<div class="row">';
        var rowEnd = '</div>';

        var i;
        var len = blocks.length;

        for (i = 0; i < len; i++) {
            str += row;
            str += options.fn(blocks[i]);
            if ((i + 1) < len) {
                str += options.fn(blocks[i + 1]);
            }
            str += rowEnd;
            i++;
        }

        return str;
    },

    // Use this when in an #each loop when you need either a first or last class
    positionCheck: function(first, last) {
        if (first === true) {
            return ' first';
        } else if (last === true) {
            return ' last';
        }
        return '';
    },

    quickLinksGlyphicons: function(type){

        var glyphiconsType = '';
        if (type && typeof type === 'string') {
            glyphiconsType = type;
        }
        return new Handlebars.SafeString([
            '<i class="glyphicons x2 dark glyphicons-',
            glyphiconsType,
            '"></i>'
        ].join(''));
    },

    quickLinksFA: function(type){

        var fontType = '';
        if (type && typeof type === 'string') {
            switch(type.toLowerCase()) {
                case 'money':
                fontType = 'money';
                break;

                case 'download':
                fontType = 'download';
                break;

                case 'info-circle':
                fontType = 'info-circle';
                break;

                case 'file-open':
                fontType = 'file-open';
                break;

                // mapping glyphicons against FA here
                case 'file-import':
                fontType = 'download';
                break;

                default:
                fontType = '';
                break;

            }
        }
        return new Handlebars.SafeString([
            '<i class="fa fa-5x dark fa-',
            fontType,
            '"></i>'
        ].join(''));
    },

    // If link begins with http, make it a full URL
    // If not, make it a fragment
    linkCheck: function(link) {
        return (link.substr(0,4) === 'http' ? link : '#' + link);
    },
    
    label: function(value) {
        var args = Array.prototype.slice.call(arguments);
        // If we need to mark this as required, will replace this value
        var reqMarker = '';
        var reqIndex = -1;

        // remove passed in argument 'value'
        args.shift();

        // mark this field as required?
        reqIndex = args.indexOf('required');
        if (reqIndex !== -1) {
            args.splice(reqIndex, 1);
            reqMarker = '&nbsp;<span>*</span>';
        }
        return [
            '<label',
            parseAttributes(args),
            '>',
            value,
            reqMarker,
            '</label>'
        ].join('');
    },

    /*
     * @param {Array} options Array of the options and their values for the select
     * @param {String} optional values
     * Value of the currently selected option can be first, like 'myValue'
     * Or multiple strings can be passed like 'foo:bar'
     * Code checks for match on '{string}:{string}' and treats the arguments
     * appropriately
     *
     * @returns String
     */
    select: function(options) {

        if (_.isArray(options) !== true) {
            throw new Error('select helper must have Array of option objects as first argument');
        }

        var args = Array.prototype.slice.call(arguments);

        // Determine is a value was previously selected
        var selectedValue = null;
        var selectedByData = false;

        // Remove the array of options
        args.shift();

        // current value
        selectedValue = args[0];
        args.shift();

        var i;
        var len = options.length;
        var str = '';

        // If we have a selected value, clear out any other settings
        if (!!selectedValue) {
            _.each(options, function(option) {
                if (option.selected === true) {
                    delete option.selected;
                }
            });
        }

        for (i = 0; i < len; i++) {
            if (!!selectedValue && options[i].key === selectedValue) {
                options[i].selected = true;
            }
            str += [
                '<option value="',
                options[i].key,
                '"',
                (
                    selectedValue && options[i].selected === true ? ' selected ' : ''
                ),
                '>',
                options[i].value,
                '</option>'
            ].join('');
        }

        str = [
            '<select',
            parseAttributes(args),
            '>',
            str,
            '</select>'
        ].join('');

        return str;
    },

    emailInput: function() {
        var args = Array.prototype.slice.call(arguments);
        args.unshift('email');
        return genericInput.apply(this, args);
    },

    passwordInput: function() {
        var args = Array.prototype.slice.call(arguments);
        args.unshift('password');
        return genericInput.apply(this, args);
    },
    
    textInput: function() {
        var args = Array.prototype.slice.call(arguments);
        args.unshift('text');
        return genericInput.apply(this, args);
    }

});

module.exports = Handlebars;
