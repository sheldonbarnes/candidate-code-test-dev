var producerViewTemplate = require('./selectProducer-producer-t');
var SEL                  = require('../selectors');

var ProducerView = Marionette.ItemView.extend({

    template: producerViewTemplate,

    templateHelpers: function(){

        return {
            PRODUCER_LINK: SEL.SELECT_PRODUCER.PRODUCER_LINK.substr(1)
        };
    }


});

module.exports = ProducerView;
