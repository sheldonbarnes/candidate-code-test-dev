var _global = (function() { return this;})();

var ProducerView           = require('./selectProducer-producer-v');
var SEL                    = require('../selectors');
var selectProducerTemplate = require('./selectProducer-t');

var producerClickSelector = 'click ' + SEL.SELECT_PRODUCER.PRODUCER_LINK;

var SelectProducerView = Marionette.CompositeView.extend({

    childView: ProducerView,
    
    childViewContainer: SEL.SELECT_PRODUCER.WRAPPER,

    events: function() {
        var hash = {};
        hash[producerClickSelector] = 'selectProducer';
        return hash;
    },
    
    initialize: function(options) {

        if (options.user) {
            this.collection = options.user.get('delegateProducerAccess');
        }

    },

    selectProducer: function(e) {

        e.preventDefault();

        var id =  parseInt(e.target.getAttribute('data-id'), 10);
        var current = this.collection.findWhere({ delegateProducerRelationshipID: id });
        var isActive = this.collection.where({ active: true });

        if (isActive && isActive.length) {
            _.each(isActive, function(element, index, list) {
                element.set({ active: false });
            });
        }
        
        if (current) {
            current.set({ active: true });
        }
        
        this.render();
    },

    template: selectProducerTemplate,

    templateHelpers: function() {

        return {
            WRAPPER: SEL.SELECT_PRODUCER.WRAPPER.substr(1)
        };
    },

    updateActive: function(){
        this.collection.findWhere({ active: true }).set({ active: false });
    }

});

module.exports = SelectProducerView;
