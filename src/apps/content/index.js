var config      = require('./config');
var ContentView = require('./views/content-v');
var SEL         = require('./selectors');
var utils       = require('./utils');
var WcmContent  = require('../../apps/wcm-content');

// Views
var views = {
    BenefitsView               : require('./benefits/benefits-layout-v'),
    AlertsView                 : require('../alerts/views/alerts-v'),
    HomeView                   : require('./views/home-v'),
    //UserInfoView               : require('../userinfo/views/userinfo-v'),
    ProductsView               : require('./views/products-v'),
    FaqView                    : require('./views/faq-v'),
    FormsView                  : require('./views/forms-v'),
    LoginView                  : require('./views/login-v'),
    NewWindowView              : require('./views/new-window-v'),
    SecurityChangeInfoView     : require('../security/changeInfo/security-layout-v'),
    SecurityChangePasswordView : require('../security/changePassword/changePassword-layout-v'),
    SecurityChangeUserIdView   : require('../security/changeUserId/changeUserId-layout-v'),
    SecurityForgotPasswordView : require('../security/forgotPassword/forgotPassword-layout-v'),
    SecurityForgotUserIdView   : require('../security/forgotUserId/forgotUserId-layout-v'),
    SelectProducerView         : require('./selectProducer/selectProducer-v'),
    TrainingView               : require('./views/training-v')
};

require('./content-helpers');
require('./partials');

var Content = Marionette.Application.extend({

    _showErrorView: function() {
        // Error view, no data, give a title of Error
        this.viewReady('error', this.ErrorView, [ 'Error'], null);
    },

    initialize: function(options) {

        _.bindAll(this,
                  '_showErrorView',
                  'onViewToRender',
                  'onRequest',
                  'viewReady'
                 );

        this.LoadingView = (options.loadingView ? options.loadingView : null);
        this.ErrorView = (options.errorView ? options.errorView : null);
        this.hosts     = (options.hosts ? options.hosts : null);

        this.user = null;
        this.userRole = null;

        console.log('Well well ' + JSON.stringify(options));
        console.log('Well done');
        if (options.user && options.user instanceof Backbone.Model) {
            this.user = options.user;
            this.alerts = options.alertmessages;
            this.userRole = this.user.get('role');
        }

        // Spin up the content collection
        // Although WcmContent allows doing a prefetch, the way this application
        // is currently structured does not allow for easy prefetching.
        // To enable it, would need to have some listener for the success/error
        // callback of the fetch that would catch incoming routing events and
        // handle them once the data was loaded.
        // Otherwise, you would not be able to respond to routing events at the
        // start of this application.
        this.WcmContent = new WcmContent();

        // A LayoutView will hold the regions we want to manipulate
        // But we will do all logic here, in the Application.
        this.RootView = new ContentView({
            el: options.$rootEl
        });

        // Render out the container
        this.RootView.render();
        // Set up the region that will hold the different views
        this.RootView.addRegion('sectionRegion', SEL.REGIONS.SECTION_JS);

        // Render a loading view in the content area until we have content to show
        if (this.LoadingView) {
            this.showLoadingView();
        }

        // Set up what we will respond to. These are routing events that come
        // from the sidebar app.
        this.comm = options.comm;
        if (
            this.comm &&
                this.comm.channel &&
                this.comm.commandName
        ) {
            this.comm.channel.comply(this.comm.commandName, this.onRequest);
        }

        // For each section that we will render out in the sectionRegion
        // we will need to match the route that was passed in the above routing event.
        // When we match, then we use this information to look up the view to render
        // and also the WCM content to fetch via REST.
        this.routes = options.routes;

    },

    // This is triggered by a Radio Request from the router
    // It should pass a route name along so we know what to render
    onRequest: function(routeName, navStructure) {



        console.log('I am in the onRequest');
        // Set defaults
        routeName    = routeName || null;
        navStructure = navStructure || null;

        // used to check for a public role and what it can see
        var roleCheck = false;
        var self = this;
        var route;

        // populate the titlespan element on first call
        // if it already exists and has been populated as a jquery object
        // this will be truthy
        if (!this.ui.titleSpan.jquery) {
            this.ui.titleSpan = $(this.ui.titleSpan);
        }

        // clear out any currently displayed title
        this.updateTitle();

        // show a loading view while we spin up
        if (this.LoadingView) {
            this.showLoadingView();
        }

        // Default to home page if a null or unsupported route is passed
        // Should only happen on initial page load
        if (!routeName) {
            utils.logger.error('No route passed, will route to the home page.');
            routeName = this.routes.HOME.LINK;
        }

        // Make sure we support the route that was called
        // Find the route object that contains all the needed information
        var routedObj = _.findWhere(this.routes, { LINK: routeName });
        if (!routedObj) {
            utils.logger.error(routeName + ' route does not exist.');
            self._showErrorView();
            return false;
        }

        if (this.userRole === 'public') {

            // Can this use see this content?
            roleCheck = (
                routedObj.OPTIONS &&
                    routedObj.OPTIONS.isPublic &&
                    routedObj.OPTIONS.isPublic === true
            );

            // If they don't pass the rolecheck
            // If they can see it, but are trying to go to home page
            // force to login page
            // This almost feels like it should live in the router versus here
            // Or even at another layer before getting into this part of the Content app
            if (!roleCheck || (routeName === this.routes.HOME.LINK)) {
                // force to the login page
                routeName = this.routes.LOGIN.LINK;
                routedObj = _.findWhere(this.routes, { LINK: routeName });
            }

        }

        // If we get to here, then there is a view to show, and we can
        // render it out for this user.

        console.log(routedObj);
        var viewToRender = views[routedObj.CONTENTVIEW];

        if (!viewToRender) {
            // There was no view specified to render
            utils.logger.error('A view to render was not specified, or could not be found.');
            utils.logger.error('The requested view was ', routedObj.CONTENTVIEW);
            self._showErrorView();
            return false;
        }

        if (routedObj.OPTIONS && routedObj.OPTIONS.isFaq === true) {

            // If this view is an FAQ view, we need to make sure we have that data
            // already loaded.
            if (this.WcmContent.isFaqDataLoaded === true) {

                // Everything is ready to go, can render it out
                this.onViewToRender(viewToRender, routeName, navStructure, routedObj);

            } else {

                // Load the FAQ items, then we can continue to render things out
                this.WcmContent.loadFaqData({
                    error: function() {
                        utils.logger.error('FAQ data could not be loaded.');
                        self._showErrorView();
                        return false;
                    },
                    success: function() {
                        // Everything is ready to go, can render it out
                        self.onViewToRender(viewToRender, routeName, navStructure, routedObj);
                    }
                });

            }

        } else {

            // Everything is ready to go, can render it out
            self.onViewToRender(viewToRender, routeName, navStructure, routedObj);

        }

        return true;

    },

    onViewToRender: function(viewToRender, routeName, navStructure, routedObj) {

        var self = this;

        // have the region render the correct view now
        if (viewToRender) {


            //console.log(app.user);
            var wcmContentNeeded = routedObj.WCMID;

            // No content needed here
            if (!wcmContentNeeded) {

                utils.logger.log('No WCM content needed for ', routeName);
                utils.logger.log('Will render using view called ', routedObj.CONTENTVIEW);
                return this.viewReady(routeName, viewToRender, navStructure, null);

            }

            // If we get to here, we need content to render this out

            // Can we find the content this view needs in our collection
            wcmContentNeeded = this.WcmContent.wcmContentCollection.findWhere({
                idAttribute: routedObj.WCMID
            });

            // We have the ID of this item in the wcm content collection.
            // But do we have content for this item?
            if (wcmContentNeeded && wcmContentNeeded.get('content')) {

                // We have the content we need, and can now render things out.
                utils.logger.log('WCM id ' + routedObj.WCMID + ' already in the collection and populated, rendering');
                this.viewReady(routeName, viewToRender, navStructure, wcmContentNeeded.get('content'));

            } else if (wcmContentNeeded) {

                // We have this in the collection, but no content.
                // So lets fetch it and then we can render out.
                utils.logger.log('WCM id ' + routedObj.WCMID + ' in the collection BUT not populated, fetching');

                wcmContentNeeded.fetch({
                    cache: true,

                    success: function() {
                        self.viewReady(routeName, viewToRender, navStructure, wcmContentNeeded.get('content'));
                    },

                    error: function() {
                        utils.logger.error('Error fetching the wcm content');
                        utils.logger.error('wcmContentNeeded is ', routedObj.WCMID);
                        self._showErrorView();
                    }

                });

            } else {

                // We do not have this in the collection, so add, fetch, callback
                utils.logger.log('WCM id ' + routedObj.WCMID + ' NOT in the collection, fetching');
                wcmContentNeeded = this.WcmContent.wcmContentCollection.add({
                    idAttribute: routedObj.WCMID,
                    content: null
                });

                wcmContentNeeded.fetch({
                    cache: true,

                    success: function() {
                        self.viewReady(routeName, viewToRender, navStructure, wcmContentNeeded.get('content'));
                    },

                    error: function() {
                        utils.logger.error('Error fetching the wcm content');
                        utils.logger.error('wcmContentNeeded is ', routedObj.WCMID);
                        self._showErrorView();
                    }
                });

            } // inner if

        }

    }, // onRequest

    showLoadingView: function() {
        this.RootView.sectionRegion.show(new this.LoadingView({
            el: utils.createWrapper()
        }));
    },

    ui: {
        // used to update the title when a new view is rendered
        titleSpan: '.titleSpan'
    },

    updateTitle: function(navStructure) {

        // Call with no args to empty the title out
        if (!navStructure || navStructure.length === 0) {
            this.ui.titleSpan.html('');
            return true;
        }

        if (navStructure && (_.isArray(navStructure) !== true)) {
            utils.logger.error('navStruture must be an array');
            return false;
        }

        var templateStr = '<span>__TEXT__</span>&nbsp';

        var templateStrSeparator = '<i class="ace-icon fa fa-angle-double-right"></i>&nbsp;';

        var html = '';
        var i;
        var len = navStructure.length;
        for (i = 0; i < len; i++) {
            html += templateStr.replace('__TEXT__', navStructure[i]);
            if (i < (len -1)) {
                html += templateStrSeparator;
            }
        }
        this.ui.titleSpan.html(html);
        return true;
    },

    viewReady: function(routeName, ViewConstructor, navStructure, data) {

        var faqData = null;
        var reRouteName = null;
        var title;
        var key;
        var updatedKey;
        var updatedFaqData = {};
        var viewOptions = null;

        this.updateTitle(navStructure);

        // Is this an FAQ view? If so, mix in the data from the FAQs
        if (ViewConstructor === views.FaqView) {

            // It is a single model with all questions
            faqData = this.WcmContent.getFaqItem();
            // If we got data, drill down to what should be a single entry
            faqData = (faqData[0] ? faqData[0] : null);

            // If we have data, now can reduce to what we need
            if (faqData) {

                // Last value of the structure is the current title
                if (navStructure.length) {
                    title = navStructure[navStructure.length - 1];
                } else {
                    title = null;
                }

                if (title) {

                    faqData = faqData.get('items');
                    reRouteName = new RegExp('^' + title.toLowerCase() + '\-', 'im');

                    if (faqData) {
                        // Keep the needed items only
                        faqData = _.filter(faqData, function(item) {
                            return reRouteName.test(item.Categories.toLowerCase());
                        });
                    }

                    if (faqData) {
                        // Now sort into groups
                        faqData = _.groupBy(faqData, 'Categories');
                    }

                    // Reduce the category name to everything to the right of the last hyphen
                    if (faqData) {
                        for (key in faqData) {
                            if (
                                faqData.hasOwnProperty(key) === true &&
                                    /-/.test(key) === true
                            ) {
                                // Life-Continuity becomes just Continuity, for example
                                updatedKey = key.substr(key.lastIndexOf('-') + 1);
                                // Make sure we're not blowing away an existing category
                                if (typeof updatedFaqData[updatedKey] === 'undefined') {
                                    updatedFaqData[updatedKey] = $.extend([], faqData[key]);
                                } else {
                                    updatedFaqData[updatedKey + _.uniqueId()] = $.extend([], faqData[key]);
                                }
                            } // if
                        } // for
                        // replace the object
                        faqData = updatedFaqData;
                    }

                    if (faqData) {
                        faqData = _.mapObject(faqData, function(val, key) {
                            return {
                                id: _.uniqueId(config.namespace + '_id_'),
                                items: val
                            };
                        });
                    }

                } // title check

            } // faqData check

            data.faqData = faqData;
        }

        // What does the view need to have to render out
        viewOptions = {
            data        : data,
            hosts       : this.hosts,
            LoadingView : this.LoadingView,
            routes      : this.routes,
            routeName   : routeName,
            state       : (this.options && this.options.state ? this.options.state : null),
            //user_123        : this.user,
            user        : this.user,
            alerts_1 : this.alerts,
            // This existing before the entire user was being passed, but should really
            // just read it in the child views since we have it now
            userRole    : this.userRole
        };

        this.RootView.sectionRegion.show(new ViewConstructor(viewOptions));

    }

});

module.exports = Content;
