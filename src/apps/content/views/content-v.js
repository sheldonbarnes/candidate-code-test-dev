// This is the LayoutView used to hold the other views
var contentViewTemplate = require('../templates/content');
var SEL                 = require('../selectors');

var ContentView = Marionette.LayoutView.extend({

    initialize: function(options) {

        this.analyticsChannel = Backbone.Radio.channel('analytics');

    },

    template: contentViewTemplate,

    templateHelpers: function(){

        return {

            SECTION_CSS: SEL.REGIONS.SECTION_CSS.substr(1),
            SECTION_JS: SEL.REGIONS.SECTION_JS.substr(1)

        };

    },
     events: {

        // Capture ALL clicks/touches of <a> tags
        // 'mousedown' works for touchstart as well.
        'mousedown a' : 'logClick',
        'mousedown button, input[type=submit]' : 'logClick'
    },

     /**
     * Log all clicks for site metrics
     * @param event
     */
    logClick: function(event) {

        this.analyticsChannel.command('trackAction', {
            event : event
        });
    }


});

module.exports = ContentView;
