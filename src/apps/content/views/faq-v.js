var _global = (function() { return this;})();
var config = require('../config');
var faqTemplate = require('../templates/faq-t');
var permitCheck = require('../permit-deny');

var FaqView = Marionette.ItemView.extend({


    initialize: function(options) {

        this.data = options.data;
        this.role = options.userRole;

    },

    template: faqTemplate,


    templateHelpers: function() {

        // We need to modify the data as we check permit/deny
        var data = _global.$.extend(true, {}, (this.data));
        var self = this;

        var count;
        var averageLength = 0;
        
        // parse out permit/deny
        data.faqContent = permitCheck(data.faqContent, self.role);

        // we need IDs on some elements for the expand/collapse to work
        _.each(data.faqData, function(faqObj) {
            _.each(faqObj.items, function(faqItem) {
                faqItem.answerId = _.uniqueId(config.namespace + '_id_');
            });
        });

        // Should only ever be one of these
        data.faqContent = data.faqContent[0];

        // DO a programmatic check for the # of tabs, and the average length
        // If too big, set a property on the data
        count = _.keys(data.faqData);
        _.each(count, function(element, index, list) {
            averageLength += element.length;
        });
        averageLength = averageLength / count.length;
        if ((count.length > 5) && (averageLength > 20)) {
            data.shortTabs = 'shorttabs';
        } else {
            data.shortTabs = '';
        }
        
        
        return {
            data: data
        };

    }

});

module.exports = FaqView;
