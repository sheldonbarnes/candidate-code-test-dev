var _global = (function() { return this;})();

var permitCheck   = require('../permit-deny');
var loginTemplate = require('../templates/login-t');
var SEL           = require('../selectors');

var LoginView = Marionette.ItemView.extend({

    events: {
        'click @ui.submit, submit @ui.form': 'onSubmit'
    },
    
    initialize: function(options) {
        _.bindAll(this,
               'onSubmit'
              );
        
    	this.data = options.data;
        this.errors = {};
        this.hosts = (options.hosts ? options.hosts : null);
    	this.role = options.userRole;
        this.routes = options.routes;
        this.state = (options.state ? options.state : null);
    },

    onSubmit: function(e) {

        var _keys;
        var self = this;
        self.errors = {};
        
        e.preventDefault();
        var attrs = this.ui.form.serializeArray();
        // Remove the hidden input
        attrs.pop();
        
        _.each(attrs, function(element, index, list) {
            if (!element.value) {
                self.errors.general = 'User ID and Password are required fields.';
            }
        });

        _keys = _.keys(self.errors);
        if (_keys.length > 0) {
            this.render();
        } else {
            this.undelegateEvents();
            this.ui.form.submit();
        }
    },
    
    template: loginTemplate,

    templateHelpers: function() {
    	// We need to modify the data as we check permit/deny
    	var data = _global.$.extend(true, {}, (this.data));
        var self = this;

        //parse out permit/deny
        data.quickLinks = permitCheck(data.quickLinks, self.role);
        data.basicContent = permitCheck(data.basicContent, self.role);

        return {
            __generalError       : this.state.__generalError,
            COL_SIZE             : 4,
            data                 : data,
            errors               : this.errors,
            FORGOT_USERID_LINK   : this.routes.FORGOT_USERID.LINK,
            FORGOT_PASSWORD_LINK : this.routes.FORGOT_PASSWORD.LINK,
            LOGIN_FORM           : SEL.LOGIN.LOGIN_FORM.substr(1),
            REGISTER_LINK        : this.routes.REGISTER.LINK,
            RESET                : SEL.LOGIN.RESET.substr(1),
            SM_TARGET            : (this.hosts ? this.hosts.SM_TARGET : ''),
            SUBMIT               : SEL.LOGIN.SUBMIT.substr(1)
        };
    },

    ui: {
        form   : SEL.LOGIN.LOGIN_FORM,
        reset  : SEL.LOGIN.RESET,
        submit : SEL.LOGIN.SUBMIT
    }

});

module.exports = LoginView;
