var _global = (function() { return this; })();

var trainingTemplate = require('../templates/training-t');

var TrainingView = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = options.data;
    },

    onShow: function(){
        _global.brightcove.createExperiences();
    },

    template: trainingTemplate,

    templateHelpers: function() {
        return {
            data: this.data
        };
    }

});

module.exports = TrainingView;
