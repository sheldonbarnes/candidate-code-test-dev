var _global = (function() { return this;})();
var config = require('../config');
var formsTemplate = require('../templates/forms-t');
var permitCheck = require('../permit-deny');

var FormsView = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = options.data;
        this.role = options.userRole;
    },

    template: formsTemplate,

    templateHelpers: function() {
        // We need to modify the data as we check permit/deny
        var data = _global.$.extend(true, {}, (this.data));
        var self = this;

        // parse out permit/deny
        data.tabContent = permitCheck(data.tabContent, self.role);

        // we need IDs on some elements for the tabs to work
        _.each(data.tabContent, function(tabItem) {
            // need an id on the header link and the target
            tabItem.linkId = _.uniqueId(config.namespace + '_id_');
            tabItem.id = _.uniqueId(config.namespace + '_id_');
        });

        return {
            data: data
        };
    }

});

module.exports = FormsView;
