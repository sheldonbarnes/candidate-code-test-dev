var _global = (function() { return this;})();
var permitCheck = require('../permit-deny');
var productsTemplate = require('../templates/products-t');

var ProductsView = Marionette.ItemView.extend({

    initialize: function(options) {
        this.data = options.data;
        this.role = options.userRole;
    },

    template: productsTemplate,

    templateHelpers: function() {
        // We need to modify the data as we check permit/deny
        var data = _global.$.extend(true, {}, (this.data));
        var self = this;

        // parse out permit/deny
        data.subSectionContent = permitCheck(data.subSectionContent, self.role);

        return {
            data: data
        };
    }

});

module.exports = ProductsView;
