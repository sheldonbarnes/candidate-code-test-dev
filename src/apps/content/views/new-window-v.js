// This is the LayoutView used to hold the other views
var newWindowViewTemplate = require('../templates/new-window-t');
var SEL                   = require('../selectors');

var NewWindowView = Marionette.LayoutView.extend({

    template: newWindowViewTemplate

});

module.exports = NewWindowView;
