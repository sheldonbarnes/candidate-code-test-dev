var _global = (function() { return this;})();
var homeTemplate = require('../templates/home-t');
var permitCheck = require('../permit-deny');

var HomeView = Marionette.ItemView.extend({

    initialize: function(options) {

        console.log('This is the options' + options);

        this.data = (options.data ? options.data : null);
        this.user = (options.user ? options.user : null);
        this.routes = (options.routes ? options.routes : null);
        this.state = (options.state ? options.state : null);
    },

    template: homeTemplate,

    templateHelpers: function() {

        var colSize;
        // We need to modify the data as we check permit/deny
        var data = _global.$.extend(true, {}, (this.data));

        var permissions = (this.user.get('permissions') ? this.user.get('permissions') : null);

        var permissionsList = [];
        var role = (this.user.get('role') ? this.user.get('role') : null);

        // parse out permit/deny
        data.quickLinks = permitCheck(data.quickLinks, role);
        data.basicContent = permitCheck(data.basicContent, role);

        // now that we know what items this ROLE should see, verify
        // the permissions for this user
        if (permissions && typeof permissions.benefits !== 'undefined') {
            permissionsList.push({
                check: permissions.benefits,
                link: this.routes.BENEFITS_PAGES.LINK
            });
        }

        if (permissions && typeof permissions.commissions !== 'undefined') {
            permissionsList.push({
                check: permissions.commissions,
                link: this.routes.COMMISSIONS.LINK
            });
        }

        if (permissions && typeof permissions.ibill !== 'undefined') {
            permissionsList.push({
                check: permissions.ibill,
                link: this.routes.IBILL.LINK
            });
        }

        data.quickLinks = _.reject(data.quickLinks, function(quicklink) {
            // find this item
            var item = _.find(permissionsList, function(permission) {
                return permission.link === quicklink.link;
            });
            // ignore quickLinks items that do not have permissions, like forms
            if (!item) {
                return false;
            }

            // if it has permissions, get rid of those that are false
            return (item.check === false);
        });

        // set the size of the column so it looks correct
        colSize = (data.quickLinks.length ? Math.floor(12 / data.quickLinks.length) : 0);

        return {
            __generalError       : this.state.__generalError,
            data: data,
            COL_SIZE: colSize
        };
    }

});

module.exports = HomeView;
