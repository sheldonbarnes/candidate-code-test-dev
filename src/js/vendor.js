// This file is needed to properly browserify Backbone/Marionette
// and to do some basic setup to avoid using globals.

// Using a single file like this allows the unit tests to be built
// and run without doing this setup in each test file
//
// in browser context, will be the window
// prevents directly using 'window'
var _global = _global || (function() { return this; })();

// Get our $ that we will attach to Backbone
var $ = require('jquery');
_global.$ = _global.$ || (_global.jQuery = $);

var Cookies = require('js-cookie');
_global.Cookies = _global.Cookies || Cookies;

// datatables
require('datatables');

// Properly setup $ in Backbone
// bootstrap needs this as well
var _ = require('underscore');
_global._ = _global._ || (_global.underscore = _);

var Backbone = require('backbone');
_global.Backbone = _global.Backbone || Backbone;
Backbone.$ = Backbone.$ || $;

// localStorage caching
var fetchCache = require('backbone-fetch-cache');

// rest of infrastructure
var Marionette = require('backbone.marionette');
_global.Marionette = _global.Marionette || Backbone.Marionette;

// Use Marionette Radio instead of Wreqr
require('backbone.radio');
require('radio.shim');

var Spinner = require('spin');
_global.Spinner = _global.Spinner || Spinner;

var has = require('has');
_global.has = _global.has || has;

require('bootstrap');

// Ace requires jquery, but we've exposed it globally at this point
// so we don't need to use handlebars shim to do anything else
var ace = require('ace');
_global.ace = _global.ace || ace;

var brightcove = require('brightcove');
_global.brightcove = _global.brightcove || brightcove;

//webtrends framework downloaded from Tag Builder (https://tagbuilder.webtrends.com/v10)
var webtrends = require('webtrends');
_global.webtrends = _global.webtrends || webtrends;

// Return _global for use in the app
module.exports = _global;
