/*
var menuHomeOffice   = require('data/sidebar/sidebar-menu-HOMEOFFICE-d');
var menuPolicyHolder = require('data/sidebar/sidebar-menu-POLICYHOLDERS-d');
var menuProducer     = require('data/sidebar/sidebar-menu-PRODUCERS-d');
var menuPublic       = require('data/sidebar/sidebar-menu-PUBLIC-d');

// Add on menu items that are not known until we know the user
var menuItemBenefits       = require('data/sidebar/sidebar-menu-BENEFITS-d');
var menuItemIBill          = require('data/sidebar/sidebar-menu-IBILL-d');
var menuItemRegistration   = require('data/sidebar/sidebar-menu-REGISTRATION-d');
var menuItemCommissions    = require('data/sidebar/sidebar-menu-COMMISSIONS-d');
var menuItemDelegateAccess = require('data/sidebar/sidebar-menu-DELEGATEACCESS-d');*/

var ROLES  = require('./roles');

var logic = {

    /*
     * Return back the menu data to pass to the sidebar as a collection
     * @param {object} user User Backbone model
     * @returns Array menu
     */
    getMenuType: function(user) {


        var hasBenefits;
        var hasCommissions;
        var hasIBill;
        var hasRegistration;
        // The menu itself
        var menu = null;
        // Permissions object on the user object
        var permissions;
        // The role the user has
        var role = null;

        // flag items that are empty because of permission checks
        var toRemove = [];

        // Check the role and fail fast if needed
        role = user.get('role');
        if (!role) {
            menu = menuPublic;
            return menu;
        }

        // Read permissions and find out any specific items needed
        permissions = user.get('permissions');
        if (permissions && _.isBoolean(permissions.benefits) === true) {
            hasBenefits = permissions.benefits;
        }
        if (permissions && _.isBoolean(permissions.commissions) === true) {
            hasCommissions = permissions.commissions;
        }
        if (permissions && _.isBoolean(permissions.ibill) === true) {
            hasIBill = permissions.ibill;
        }
        if (permissions && _.isBoolean(permissions.registration) === true) {
            hasRegistration = permissions.registration;
        }

        if (role === ROLES.HOMEOFFICE || role === ROLES.HOMEOFFICEREMOTE) {

            //////////////////////////////////////////////////
            // HOMEOFFICE
            //////////////////////////////////////////////////
            menu = menuHomeOffice;

            if (hasBenefits === true) {
                menu[0].subItems.push(menuItemBenefits);
            }

            if (hasIBill === true) {
                menu[0].subItems.push(menuItemIBill);
            }

            if (hasRegistration === true) {
                menu[0].subItems.push(menuItemRegistration);
            }

            if (hasCommissions === true) {
                menu[0].subItems.push(menuItemCommissions);
            }

        } else if  (
            role === ROLES.POLICYHOLDER
        ) {

            //////////////////////////////////////////////////
            // POLICYHOLDERS
            //////////////////////////////////////////////////
            menu = menuPolicyHolder;

            if (hasBenefits === true) {
                menu[0].subItems.push(menuItemBenefits);
            }

            if (hasIBill === true) {
                menu[1].subItems.push(menuItemIBill);
                menu[2].subItems.push(menuItemIBill);
            }

        } else if  (
            role === ROLES.PRODUCER ||
            role === ROLES.PRODUCERDELEGATE ||
            role === ROLES.DELEGATE
        ) {

            //////////////////////////////////////////////////
            // PRODUCERS/DELEGATES/PRODUCERDELEGATES
            //////////////////////////////////////////////////
            menu = menuProducer;

            if (hasCommissions === true) {
                menu[0].subItems.push(menuItemCommissions);
            }

            if  (role === ROLES.PRODUCER) {
                menu[0].subItems.push(menuItemDelegateAccess);
            }

            if (hasRegistration === true) {
                menu[0].subItems.push(menuItemRegistration);
            }


            if (hasIBill === true) {
                menu[0].subItems.push(menuItemIBill);
            }

        } else {
            //////////////////////////////////////////////////
            // PUBLIC
            //////////////////////////////////////////////////
            menu = menuPublic;
        }

        _.each(menu, function(element, index, list) {
            // If there is a subItems property that is empty, remove the entire subItem
            if (element.subItems && element.subItems.length === 0) {
                delete element.subItems;
            }
            // Flag the entire item for removal if no subitems and no top level link
            if (!element.link && !element.subItems) {
                toRemove.push(element);
            }
        });

        // Remove the items we flagged earlier
        menu = _.reject(menu, function(item) {
            return _.find(toRemove, item);
        });

        return menu;

    }
};

module.exports = logic;
