module.exports = {

    _PUBLIC          : 'public',
    DELEGATE         : 'delegate',
    HOMEOFFICE       : 'homeoffice',
    POLICYHOLDER     : 'policyholder',
    PRODUCER         : 'producer'
    
};
