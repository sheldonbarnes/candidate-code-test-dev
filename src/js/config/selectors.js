var selectors = {

    INITIAL_LOADING: '.loading-message',

    LOADING: {

        WRAPPER: '.js-eben-loading'

    },

    FIRST_TIME_UX : {

        MODAL: '.js-eben-firsttimeux-modal'
        
    },

    WRAPPER: {

        _SELF: '.main-container'

    }

};

module.exports = selectors;
