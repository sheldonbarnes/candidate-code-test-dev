var _global = (function() { return this;})();

// Config file is a singleton so that runtime configuration is set on load.
// You can set values in the query parameters like this: debug=false
// Or in localStorage like this: localStorage.setItem('debug', 'false');

// Needed URLs
var hosts   = require('./hosts');

// Will be overridde by preprocessing in browserify
// Can check for a value of 'true' to mean we are in DEV mode
var debugValue = '/* @echo DEV */';
debugValue = (debugValue === 'true' ? true : false );

// Determine the environment we are in
// Values must match up against the hosts file listed above
var env = (function() {

    var env;
    // get the current URL, default to a localhost just in case
    var host = _global.location ? _global.location.hostname : 'localhost';
    // remove any prefix
    host = (/^www\./.test(host) === true ? host.split('www.')[1] : host);
    // Set the value
    switch (host.substr(0, 2).toUpperCase()) {
        case 'SB':
        env = 'SB';
        break;

        case 'ST':
        env = 'ST';
        break;

        case 'FT':
        env = 'FT';
        break;

        case 'LO':
        env = 'LOCAL';
        break;

        // 127.0.0.1
        case '12':
        env = 'LOCAL';
        break;

        default:
        env = 'PROD';
        break;
    }

    return env;
})();

// Only place values in here that you will allow to be overridden
// For other values, place in the protectedConfig
// Only values in one of these will be allowed to be set via
// query params or localStorage
var config = {

    debug        : debugValue,
    smPing       : null,
    smIdle       : null,
    // SiteMinder params
    SMAUTHREASON : null,
    smRsn        : null,
    // Used to determine if the user was trying to login or not
    TARGET       : null
    
};

// Values placed here will not be overridden by query params
// or localStoreage
var protectedConfig = {

    // In development, hit the dev server. In prod, REST url is based off of root
    apiBaseUrl : (config.debug ? '/employeebenefits' : '') + '/secure/rest',
    console    : !!_global.console,
    env        : env,
    hosts      : hosts[env],
    namespace  : 'EBEN'

};

// We don't want to drop just any query param value into our config
// So build a list of the values we will permit, consisting of the config and protectedConfig
// keys
var permitted = _.keys(protectedConfig);
permitted = permitted.concat(_.keys(config));


// This is NOT used below
// It is not vetted and I should just use a library for this instead
function parseLocalStorage(config) {

    var localStorageConfig = {};

    // loop variables, for iterating over object properties
    var current, prop;

    if (!_global.localStorage) {
        return localStorageConfig;
    }

    for (prop in config) {
        if (
            config.hasOwnProperty(prop) === true &&
                _global.localStorage.getItem(prop) !== null
        ) {
            current = _global.localStorage.getItem(prop);
            if (current === 'true') {
                current = true;
            } else if (current === 'false') {
                current = false;
            }
            localStorageConfig[prop] = current;
        }
    }
    return localStorageConfig;
}

/*
 * Turn any query parameters into a configuration object
 * @param {param} string query string
 * @returns {object}
 */

function parseQueryParams(params) {

    // return value
    var config = {};
    // looping vars
    var i, len, current;
    // temp variables for dealing with name/value pairs
    var name, value;

    if (!params) {
        return config;
    }

    if (/^\?/.test(params) === true) {
        params = params.substr(1);
    }

    // get a list of all values that were passed
    if (params.length > 0) {

        // check for encoded html
        if (/&amp;/.test(params) === true) {
            params = params.replace('&amp;', '&');
        }

        // break up into name/value pairs
        params = params.split('&');
        len = params.length;
        for (i = 0; i < len; i++) {

            // now split into name/value
            current = params[i];
            current = current.split('=');

            if (current.length > 0) {

                name = current[0];
                value = current[1];

                // convert to real booleans
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                } else {
                    // Don't want to re-encode here, but set to strings again
                    // just in case of references to scoped variables
                    // TODO need to really understand security implications here
                    value = value + '';
                }

                // Do not allow properties to be overridden
                if (
                    typeof config[name] === 'undefined' &&
                        _.has(config, name) === false
                ) {
                    config[name] = value;
                }
                
            } else {

                // If here, wasn't a valid foo=bar
                continue;

            } // if/else

        } // for loop

    } // for loop over params.length

    return config;

}

// Singleton
module.exports = (function() {
    var runtimeConfig = _.pick(parseQueryParams(_global.location.search), permitted);
    // Turning off the localStorageParsing till I can vet it better
    // runtimeConfig = _.extend({}, runtimeConfig, _.pick(parseLocalStorage(config), permitted));
    config = _.extend({}, config, runtimeConfig, protectedConfig);
    // Siteminder might return SMAUTHREASON or smRsn
    // Map them both to a single param here to save time
    if (typeof config.smRsn !== null && config.SMAUTHREASON === null) {
        config.SMAUTHREASON = config.smRsn;
    }
    return config;
})();

module.exports = config;
