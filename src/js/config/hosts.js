// Needed URLs and values per environment
// MUST match against the env check in the config file

module.exports = {

    LOCAL: {
        // in seconds
        AJAX_TIMEOUT              : 10000,
        BCENTER                   : 'https://www.oneamerica.com/businesses-employers/business-employers-landing-page',
        BENEFITS                  : 'http://localhost:3000/employeebenefitsWeb/secure/benefits/accountSearch.faces',
        BENEFITS_POLICYHOLDERS    : 'http://localhost:3000/employeebenefitsWeb/secure/benefits/benefits.faces',
        COMMISSIONS               : 'http://localhost:3000/commissions/index.faces',
        DELEGATES                 : 'http://localhost:3000/employeebenefitsWeb/secure/delegatemaint/delegateSearch.faces',
        DISABILITY_INSURANCE      : 'https//localhost:3000/products-services/business-disability-income-insurance',
        IBILL                     : 'http://localhost:3000/ibill',
        LOGIN                     : 'http://localhost:3000/public/index.html',
        POLICYHOLDER_REGISTRATION : 'http://localhost:3000/administration/Login',
        REGISTER                  : 'http://localhost:3000/registration/registrationType.faces',
        SM_TARGET                 : 'http://localhost:3000/secure/index.html',
        TALENT                    : 'https://www.oneamerica.com/financial-education/business-attract-and-retain-talent',
        WEBTRENDS            : {
            DCSID       : 'dcs222ahscq7gvpam85u0hp3m_4p5o',
            ONSITEDOMS  : 'sbemployeebenefits.aul.com',
            FPCDOM      : '.sbemployeebenefits.aul.com'
        }
    },

    SB : {
        AJAX_TIMEOUT              : 100000,
        BCENTER                   : 'https://www.sb.oneamerica.com/businesses-employers/business-employers-landing-page',
        BENEFITS                  : 'https://www.sbemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/accountSearch.faces',
        BENEFITS_POLICYHOLDERS    : 'https://www.sbemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/benefits.faces',
        COMMISSIONS               : 'https://www.sbemployeebenefits.aul.com/commissions/index.faces',
        DELEGATES                 : 'https://www.sbemployeebenefits.aul.com/employeebenefitsWeb/secure/delegatemaint/delegateSearch.faces',
        DISABILITY_INSURANCE      : 'https://www.sb.oneamerica.com/products-services/business-disability-income-insurance',
        IBILL                     : 'https://www.sbibill.aul.com/ibill/Login',
        LOGIN                     : 'https://www.sbemployeebenefits.aul.com/public/index.html',
        POLICYHOLDER_REGISTRATION : 'https://www.sbemployeebenefits.aul.com/administration/Login',
        REGISTER                  : 'https://www.sbemployeebenefits.aul.com/registration/registrationType.faces',
        SM_TARGET                 : 'https://www.sbemployeebenefits.aul.com/secure/index.html',
        TALENT                    : 'https://www.sb.oneamerica.com/financial-education/business-attract-and-retain-talent',
        WEBTRENDS            : {
            DCSID       : 'dcs222ahscq7gvpam85u0hp3m_4p5o',
            ONSITEDOMS  : 'sbemployeebenefits.aul.com',
            FPCDOM      : '.sbemployeebenefits.aul.com'
        }
    },
    
    ST : {
        AJAX_TIMEOUT              : 100000,
        BCENTER                   : 'https://www.st.oneamerica.com/businesses-employers/business-employers-landing-page',
        BENEFITS                  : 'https://www.stemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/accountSearch.faces',
        BENEFITS_POLICYHOLDERS    : 'https://www.stemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/benefits.faces',
        COMMISSIONS               : 'https://www.stemployeebenefits.aul.com/commissions/index.faces',
        DELEGATES                 : 'https://www.stemployeebenefits.aul.com/employeebenefitsWeb/secure/delegatemaint/delegateSearch.faces',
        DISABILITY_INSURANCE      : 'https://www.st.oneamerica.com/products-services/business-disability-income-insurance',
        IBILL                     : 'https://www.stibill.aul.com/ibill/Login',
        LOGIN                     : 'https://www.stemployeebenefits.aul.com/public/index.html',
        POLICYHOLDER_REGISTRATION : 'https://www.stemployeebenefits.aul.com/administration/Login',
        REGISTER                  : 'https://www.stemployeebenefits.aul.com/registration/registrationType.faces',
        SM_TARGET                 : 'https://www.stemployeebenefits.aul.com/secure/index.html',
        TALENT                    : 'https://www.st.oneamerica.com/financial-education/business-attract-and-retain-talent',
        WEBTRENDS            : {
            DCSID       : 'dcs222ahscq7gvpam85u0hp3m_4p5o',
            ONSITEDOMS  : 'stemployeebenefits.aul.com',
            FPCDOM      : '.stemployeebenefits.aul.com'
        }
    },

    FT : {
        AJAX_TIMEOUT              : 100000,
        BCENTER                   : 'https://www.ft.oneamerica.com/businesses-employers/business-employers-landing-page',
        BENEFITS                  : 'https://www.ftemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/accountSearch.faces',
        BENEFITS_POLICYHOLDERS    : 'https://www.ftemployeebenefits.aul.com/employeebenefitsWeb/secure/benefits/benefits.faces',
        COMMISSIONS               : 'https://www.ftemployeebenefits.aul.com/commissions/index.faces',
        DELEGATES                 : 'https://www.ftemployeebenefits.aul.com/employeebenefitsWeb/secure/delegatemaint/delegateSearch.faces',
        DISABILITY_INSURANCE      : 'https://www.ft.oneamerica.com/products-services/business-disability-income-insurance',
        IBILL                     : 'https://www.ftibill.aul.com/ibill/Login',
        LOGIN                     : 'https://www.ftemployeebenefits.aul.com/public/index.html',
        POLICYHOLDER_REGISTRATION : 'https://www.ftemployeebenefits.aul.com/administration/Login',
        REGISTER                  : 'https://www.ftemployeebenefits.aul.com/registration/registrationType.faces',
        SM_TARGET                 : 'https://www.ftemployeebenefits.aul.com/secure/index.html',
        TALENT                    : 'https://www.ft.oneamerica.com/financial-education/business-attract-and-retain-talent',
        WEBTRENDS            : {
            DCSID       : 'dcs222ahscq7gvpam85u0hp3m_4p5o',
            ONSITEDOMS  : 'ftemployeebenefits.aul.com',
            FPCDOM      : '.ftemployeebenefits.aul.com'
        }
    },

    PROD : {
        AJAX_TIMEOUT              : 50000,
        BCENTER                   : 'https://www.oneamerica.com/businesses-employers/business-employers-landing-page',
        BENEFITS                  : 'https://www.employeebenefits.aul.com/employeebenefitsWeb/secure/benefits/accountSearch.faces',
        BENEFITS_POLICYHOLDERS    : 'https://www.employeebenefits.aul.com/employeebenefitsWeb/secure/benefits/benefits.faces',
        COMMISSIONS               : 'https://www.employeebenefits.aul.com/commissions/index.faces',
        DELEGATES                 : 'https://www.employeebenefits.aul.com/employeebenefitsWeb/secure/delegatemaint/delegateSearch.faces',
        DISABILITY_INSURANCE      : 'https://www.oneamerica.com/products-services/business-disability-income-insurance',
        IBILL                     : 'https://www.billing.aul.com/ibill/Login',
        LOGIN                     : 'https://www.employeebenefits.aul.com/public/index.html',
        POLICYHOLDER_REGISTRATION : 'https://www.employeebenefits.aul.com/administration/Login',
        REGISTER                  : 'https://www.employeebenefits.aul.com/registration/registrationType.faces',
        SM_TARGET                 : 'https://www.employeebenefits.aul.com/secure/index.html',
        TALENT                    : 'https://www.oneamerica.com/financial-education/business-attract-and-retain-talent',
        WEBTRENDS            : {
            DCSID       : 'dcs222cmecr1brems6deg4q0p_1n4u',
            ONSITEDOMS  : 'employeebenefits.aul.com',
            FPCDOM      : '.employeebenefits.aul.com'
        }
    }

};
