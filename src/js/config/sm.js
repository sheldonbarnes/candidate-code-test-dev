// Codes returned by the SMAUTHREASON query param
// http://www.ca.com/us/support/ca-support-online/product-content/knowledgebase-articles/tec450869.aspx

var routes     = require('data/routes');

var baseMsg = [
    'We are unable to process your request, and are currently only able to show',
    ' you a limited amount of our site.',
    'Please <a href="#',
    routes.CONTACT_US.LINK,
    '">Contact Us</a> for help.'
].join('');

var disabledMsg = [
    'Your account has been disabled. ',
    'Please <a href="#',
    routes.CONTACT_US.LINK,
    '">contact</a> Customer Support.'
].join('');

var mustChangePasswordMsg = [
    'You must change your password immediately.',
    ' Please do so before proceeding. You will not ',
    'be able to view any other part of the site until ',
    'your password is changed.'
].join('');

var SiteMinderCodes = {
    __UNKNOWN: {
        originalReason: 'No code passed from SM',
        reason: baseMsg
    },
    '0'  : {
        originalReason: 'Sm_Api_Reason_None',
        reason: ''
    },
    '1'  : {
        originalReason: 'Sm_Api_Reason_PwMustChange',
        reason: mustChangePasswordMsg,
        route: routes.CHANGE_PASSWORD.LINK
    },
    '2'  : {
        originalReason: 'Sm_Api_Reason_InvalidSession',
        reason: ''
    },
    '3'  : {
        originalReason: 'Sm_Api_Reason_RevokedSession',
        reason: ''
    },
    '4'  : {
        originalReason: 'Sm_Api_Reason_ExpiredSession',
        reason: ''
    },
    '5'  : {
        originalReason: 'Sm_Api_Reason_AuthLevelTooLow',
        reason: ''
    },
    '6'  : {
        originalReason: 'Sm_Api_Reason_UnknownUser',
        reason: ''
    },
    '7'  : {
        originalReason: 'Sm_Api_Reason_UserDisabled',
        // Do not set an allowed route here, because they
        // probably can't login. So they should be able to
        // browse the public site.
        reason: disabledMsg
    },
    '8'  : {
        originalReason: 'Sm_Api_Reason_InvalidSessionId',
        reason: ''
    },
    '9'  : {
        originalReason: 'Sm_Api_Reason_InvalidSessionIp',
        reason: ''
    },
    '10' : {
        originalReason: 'Sm_Api_Reason_CertificateRevoked',
        reason: ''
    },
    '11' : {
        originalReason: 'Sm_Api_Reason_CRLOutOfDate',
        reason: ''
    },
    '12' : {
        originalReason: 'Sm_Api_Reason_CertRevokedKeyCompromised',
        reason: ''
    },
    '13' : {
        originalReason: 'Sm_Api_Reason_CertRevokedAffiliationChange',
        reason: ''
    },
    '14' : {
        originalReason: 'Sm_Api_Reason_CertOnHold',
        reason: ''
    },
    '15' : {
        originalReason: 'Sm_Api_Reason_TokenCardChallenge',
        reason: ''
    },
    '16' : {
        originalReason: 'Sm_Api_Reason_ImpersonatedUserNotInDir',
        reason: ''
    },
    '17' : {
        originalReason: 'Sm_Api_Reason_Anonymous',
        reason: ''
    },
    '18' : {
        originalReason: 'Sm_Api_Reason_PwWillExpire',
        reason: ''
    },
    '19' : {
        originalReason: 'Sm_Api_Reason_PwExpired',
        reason: mustChangePasswordMsg,
        route: routes.CHANGE_PASSWORD.LINK
    },
    '20' : {
        originalReason: 'Sm_Api_Reason_ImmedPWChangeRequired',
        reason: mustChangePasswordMsg,
        route: routes.CHANGE_PASSWORD.LINK
    },
    '21' : {
        originalReason: 'Sm_Api_Reason_PWChangeFailed',
        reason: ''
    },
    '22' : {
        originalReason: 'Sm_Api_Reason_BadPWChange',
        reason: ''
    },
    '23' : {
        originalReason: 'Sm_Api_Reason_PWChangeAccepted',
        reason: ''
    },
    '24' : {
        originalReason: 'Sm_Api_Reason_ExcessiveFailedLoginAttempts',
        // Do not set an allowed route here, because they
        // probably can't login. So they should be able to
        // browse the public site.
        reason: disabledMsg
    },
    '25' : {
        originalReason: 'Sm_Api_Reason_AccountInactivity',
        reason: ''
    },
    '26' : {
        originalReason: 'Sm_Api_Reason_NoRedirectConfigured',
        reason: ''
    },
    '27' : {
        originalReason: 'Sm_Api_Reason_ErrorMessageIsRedirect',
        reason: ''
    },
    '28' : {
        originalReason: 'Sm_Api_Reason_Next_Tokencode',
        reason: ''
    },
    '29' : {
        originalReason: 'Sm_Api_Reason_New_PIN_Select',
        reason: ''
    },
    '30' : {
        originalReason: 'Sm_Api_Reason_New_PIN_Sys_Tokencode',
        reason: ''
    },
    '31' : {
        originalReason: 'Sm_Api_Reason_New_User_PIN_Tokencode',
        reason: ''
    },
    '32' : {
        originalReason: 'Sm_Api_Reason_New_PIN_Accepted',
        reason: ''
    },
    '33' : {
        originalReason: 'Sm_Api_Reason_Guest',
        reason: ''
    },
    '34' : {
        originalReason: 'Sm_Api_Reason_PWSelfChange',
        reason: ''
    },
    '35' : {
        originalReason: 'Sm_Api_Reason_ServerException',
        reason: ''
    },
    '36' : {
        originalReason: 'Sm_Api_Reason_UnknownScheme',
        reason: ''
    },
    '37' : {
        originalReason: 'Sm_Api_Reason_UnsupportedScheme',
        reason: ''
    },
    '38' : {
        originalReason: 'Sm_Api_Reason_Misconfigured',
        reason: ''
    },
    '39' : {
        originalReason: 'Sm_Api_Reason_BufferOverflow',
        reason: ''
    },
    '40' : {
        originalReason: 'Sm_Api_Reason_SetPersistentSessionFailed',
        reason: ''
    },
    '41' : {
        originalReason: 'Sm_Api_Reason_UserLogout',
        reason: ''
    },
    '42' : {
        originalReason: 'Sm_Api_Reason_IdleSession',
        reason: ''
    },
    '43' : {
        originalReason: 'Sm_Api_Reason_PolicyServerEnforcedTimeout',
        reason: ''
    },
    '44' : {
        originalReason: 'Sm_Api_Reason_PolicyServerEnforcedIdle',
        reason: ''
    },
    '45' : {
        originalReason: 'Sm_Api_Reason_ImpersonationNotAllowed',
        reason: ''
    },
    '46' : {
        originalReason: 'Sm_Api_Reason_ImpersonationNotAllowedUser',
        reason: ''
    },
    '47' : {
        originalReason: 'Sm_Api_Reason_FederationNoLoginID',
        reason: ''
    },
    '48' : {
        originalReason: 'Sm_Api_Reason_FederationUserNotInDir',
        reason: ''
    },
    '49' : {
        originalReason: 'Sm_Api_Reason_FederationInvalidMessage',
        reason: ''
    },
    '50' : {
        originalReason: 'Sm_Api_Reason_FederationUnacceptedMessage',
        reason: ''
    },
    '51' : {
        originalReason: 'Sm_Api_Reason_ADnativeUserDisabled',
        reason: ''
    },
    '40000' : {
        originalReason: 'OA_EBEN_DISABLED',
        reason: disabledMsg
    }
};

_.each(SiteMinderCodes, function(value, key, list) {
    if (value.reason === '') {
        value.reason = baseMsg;
    }

});

module.exports = SiteMinderCodes;
