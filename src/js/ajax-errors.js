var logout = require('logout');

/**
 * Handle AJAX Error Responses
 *
 * main.js sets up this function as a global error handler for all jQuery
 * ajax requests (which includes Backbone fetch() and sync() calls).
 * This function is executed AFTER one-off error handlers.
 *
 * @param {Object} evt   - jQuery event object
 * @param {Object} jqXHR - jQuery XHR response object
 */
function globalAjaxErrorHandler(evt, jqXHR, status, error) {

    switch (jqXHR.status) {

    case 401: // "Unauthorized"
        // Siteminder session timeout will cause any REST request to return 401.
        // The user needs to be logged out.
        logout();
        break;

    default:
        // Don't do anything for any other error status codes.
    }
}

module.exports = globalAjaxErrorHandler;

