var _global = (function() { return this;})();

var frameBuster = function() {
    if (
        _global.top &&
            _global.top.location &&
            _global.top.location.href &&
            _global.self &&
            _global.self.location &&
            _global.self.location.href &&
            _global.top.location.href !== _global.self.location.href
    ) {
        _global.top.location.href = _global.self.location.href;
    }

    return false;
};

module.exports = frameBuster;
