/**
 * Utilities specific to WebTrends.
 *
 * Note: I initially created this because I was having scope issues within the 'comply' function
 * that logged actions. In that case, 'this' referred to the channel, and the closure scope did
 * not contain the methods on analyticsModule.js. I am considering moving all webtrends-specific
 * code out of the analytics module and have this module (and a google analytics module) act
 * as plugins for the analyticsModule.
 *
 * Created by Jason.Bell on 6/17/2015.
 */

var utils  = require('../utils');

module.exports = {

    /**
     * Abstract the WebTrends-specific event IDs and allow the app to use descriptive,
     * non-vendor-specific strings when calling the analytics module. The method accepts
     * a string and returns the equivalent WebTrends ID, defaulting to 99 if WebTrends
     * does not have a specific item.
     *
     * @param eventType A string representing the type of event
     * @returns {number} The WebTrends numeric ID for an event type
     */
    getWebTrendsEventDetails: function(event) {

        //parsing required info from event object
        var currentTarget = event.currentTarget;
        var uri;
        var title;
        var eventType;
        var eventDetails;

        //it can be A, BUTTON, INPUT etc...
        var tagName = currentTarget.tagName.toLowerCase();

        //for offsite and download
        var referenceUri = ''; 

        //to understand whether its rightClick
        if(event.which === 3){
            eventType = 'rightClick';
        }

        //if anchor find out final event type which match one eventIds
        if(tagName === 'a' && !eventType ){

            //uri of anchor link
            uri = currentTarget.href;
            title = $(currentTarget).data('title') || currentTarget.text.trim();
            
            eventDetails = this.getEventDetails(currentTarget, tagName);
            eventType = eventDetails.eventType;
            if(eventDetails.title){
                title = eventDetails.title;
            }

        }

        var eventIds = {
            pageView: 0,
            download: 20, 
            anchor: 21, 
            javascript: 22, 
            mailto: 23, 
            external: 24, 
            rightClick: 25, 
            
            formGet: 26,
            formPost: 27,
            formInput: 28,
            formButton: 29,
            imageMap: 30,
            youTubeImpression: 40,
            videoEvent: 41,
            onSiteAd: 50,
            mobileEvent: 60,
            mobileStateEvent: 61,
            linkClick: 99,
            facebook: 111,
            heatmap: 125
        };

        // 99 is a default 'unknown' event id (conclusion from AS2 code)
        var id = 99;
        if (eventIds.hasOwnProperty(eventType)) {
            id = eventIds[eventType];
        }

        return  {
                    uri          : uri,
                    title        : title,
                    eventId      : id,
                    referenceURI : window.location.href
                };
    },

    // get event type based on the target element attributes
    getEventDetails: function(currentTarget, tagName){
        
        //for checking if its a file
        var fileExtensionPattern = /\.(xls|xslx|doc|docx|pdf|txt|csv|zip|rar|gzip)/i;

        //get host of current element
        var hostPattern = new RegExp(currentTarget.host, 'i');

        //http://help.webtrends.com/en/jstag/tracking_selectors.html
        //get hostname and pathname of the current element
        var hostName    = currentTarget.hostname?( currentTarget.hostname.split(":")[0] ):"";
        var protocol    = currentTarget.protocol||"";
        var query       = currentTarget.search?currentTarget.search.substring(currentTarget.search.indexOf("?")+1,currentTarget.search.length):"";
        var pathName    = currentTarget.pathname?((currentTarget.pathname.indexOf("/")!==0)?"/"+currentTarget.pathname:currentTarget.pathname):"/";

        var eventType;
        var title;       

        //it A tag
        if(tagName === 'a'){

            var href = currentTarget.href;

            //javascript event as inline
            if(/^javascript:/i.test(href)){
                return 'javascript';
            }

            //mailto
            else if(/^mailto:/i.test(href)){
               return 'mailto';
            }

            //download
            else if(/files\//gi.test(href) || fileExtensionPattern.test(href) ){

                title = "Download:" + hostName + pathName + (query.length ? ("?" + query) : "");
                eventType = 'download';

            }

            //external link?
            else if(!hostPattern.test(window.location.host)){

                title = "Offsite:" + hostName + pathName + (query.length ? ("?" + query) : "");
                eventType = 'external';

            }
            //otherwise it will be just an 'a' tag
            else {
                eventType = 'anchor';
            }

            return {
                eventType   : eventType,
                title       : title
            };

        }

    },

    /**
     * Log the response to a webtrends call.
     * @param options The object passed into the appropriate analyticsModule comply function
     * @param response The response from WebTrends
     */
    logResponse: function(options, response) {
        var action = 'view of';

        //let's check that the event was collected.
        if (response.errors && response.errors.length) {           
            utils.logger.log('Errors logging analytics data');
            utils.logger.log(response.errors);
        } else {
            if (options.eventType || options.actionName) {
                action = options.eventType + ':' + options.actionName + ' on';
            }
            utils.logger.log(action + ' uri "' + options.uri + '" logged successfully');
        }
    }
};
