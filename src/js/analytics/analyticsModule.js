/* global Marionette, WebTrends*/
/**
 * A skeleton module for encapsulating an analytics package.
 * This module should be initialized on app start and use Backbone.Radio command/comply.
 *
 * Created by Jason.Bell on 4/8/2015.
 */

var config = require('../config/config');
var utils  = require('../utils');

var WebTrendsUtil = require('./webtrends-util');

// Defining eladId here, rather than in the module because the closure scope for the
// 'comply' functions is this overall module NOT the AnalyticsModule var.
var eladId = null;

var AnalyticsModule  = Marionette.Object.extend({

    init: function(options) {

        if (options.eladId) {
            eladId = options.eladId;
        }

        // Let's make the interface with Backbone.Radio
        var channelName = options && options.channelName || 'analytics';
        this.analyticsChannel = Backbone.Radio.channel(channelName);

        // Initialize and configure the analytics utility (if necessary)
        // Configure for things it can do automatically even in SPA (if possible)
        // Disable automatic things that we'll need to do programatically in SPA (if necessary)
        // Store a ref to the main analytics object in this scope, to be used by the callbacks
        // in the 'comply' sections below (if necessary)

        var dcs = new WebTrends.dcs();
        dcs.init({
            
            //Webtrends data collection server ID (DCSID)
            dcsid: config.hosts.WEBTRENDS.DCSID,  
            
            //default for Webtrends On Demand accounts
            domain:'statse.webtrendslive.com', 
            
            //time zone (EST) for Webtrends data source
            timezone:-5, 
            i18n:false,
            
            //types of on-page click activities to track (offsite, download,anchor, javascript)  
            offsite:true,                         
            download:true, 
            downloadtypes:'xls,doc,pdf,txt,csv,zip,docx,xlsx,rar,gzip',
            anchor:true,
            javascript: true,
            
            //primary domain of the site you want to track
            onsitedoms: config.hosts.WEBTRENDS.ONSITEDOMS,
            
            //to track visits as completely separate visits, exculde subdomains
            fpcdom: config.hosts.WEBTRENDS.FPCDOM,  
            plugins:{
            }
        });
        
        /*
         * Create a hash to define the 'comply' functions.
         */
        this.analyticsChannel.comply({
            trackView: this.trackView,
            trackAction: this.trackAction,
            setUser: this.setUser
        });
    },

    /**
     * Track that a user viewed a page
     * @param options
     */
    trackView : function(options) {

        // set up the args object
        var args = {
            'DCS.dcsuri': options.uri,

            //page view refernce :- http://bit.ly/1g38dyF
            'WT.dl'     : 0,

            //title of the page
            'WT.ti'     : options.title || document.title,

            //scenario name
            'WT.si_n'   : '',

            //Identifies the step by name for scenario
            'WT.si_p'   : '',

            //home page or not
            'WT.hp'     : options.home || 0
        };

        // Add the DCS.dcsaut property
        if (eladId) {
            args['DCS.dcsaut'] = eladId;
        }

        // Call WebTrends
        WebTrends.multiTrack({
            args: args,
            callback: function (a) {
                WebTrendsUtil.logResponse(options, a);
            }
        });

        utils.logger.log('WebTrends Analytics logging. URI :' + options.uri);
    },

    /*
     * Call this on login, so that all logging includes this info
     */
    setUser: function(id) {
        eladId = id;
        utils.logger.log('setting user for analytics. eladId is : '+ eladId);
    },

    /**
     * Track that an action occurred
     * @param options the args param is an object of the following format:
     * {
     *    // URI of the page on which the event happened (eg, #my-biz/pending)
     *    uri: 'string',
     *
     *    // Name of the action taken (eg, 'Filter')
     *    actionName: 'string',
     *
     *    // The type of the event, to be translated to the analytic provider's code
     *    // This is optional, and will default to a 'miscellaneous' value
     *    // (eg, 'formGet')
     *    eventType: 'string'
     * }
     */
    trackAction: function(options) {

        var eventDetails = WebTrendsUtil.getWebTrendsEventDetails(options.event);

        var eventId = eventDetails.eventId;
        var uri     = eventDetails.uri;
        var referenceURI =   eventDetails.referenceURI;
        var title = eventDetails.title;

        
        // Considering going with custom params, but those need special reports
        //'DCSext.action': options.actionName
        var args = {
            'DCS.dcsuri': uri,
            'WT.ti': title,
            'WT.hp': 0,
            'WT.dl': eventId
        };

        //refer this URL for WT.dl - http://bit.ly/1g38dyF
        if(eventId === 20 || eventId === 24){
            args['DCS.dcsref'] = referenceURI;
        }

        if (eladId) {
            args['DCS.dcsaut'] = eladId;
        }

        WebTrends.multiTrack({
            args: args,
            callback: function (a) {
                WebTrendsUtil.logResponse(eventDetails, a);
            }
        });
    },

});

// export a singleton instance of the module
module.exports = new AnalyticsModule();