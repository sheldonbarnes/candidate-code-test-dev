var MenuUtils = {

    // Recurse through the nested menus to build the lookup
    concatValue: function(item, masterHash, parentStructure) {

        var self = this;
        // The value we might need to pass down
        parentStructure = parentStructure || [];

        // Create the value for this item
        parentStructure.push(item.displayText);
        // Break the array reference, store on the hash if needed
        // Only items that are actually a link store a value
        if (item.link) {
            masterHash[item.link] = $.extend([], parentStructure);
        }

        // Recurse, passing along the current structure so it can be added to
        if (item && item.subItems && _.isArray(item.subItems) === true) {
            // recurse
            _.each(item.subItems, function(subItem) {
                self.concatValue(subItem, masterHash, $.extend([], parentStructure));
            });
        }

        return masterHash;

    },

    // Take an array of items and create a hash with the route as the key
    // and the nested titles as the value. This will get passed along to
    // the routing in the main app so the content views can consume it.
    createTitleLookup: function(items) {

        var hash = {};
        var flattened = null;
        var self = this;

        // Create the data in each item
        _.each(items, function(item) {
            self.concatValue(item, hash);
        });

        return hash;
    }

};

module.exports = MenuUtils;
