var _global    = (function() { return this; })();

// Allows them to kill the session ONLY when it first opens.
function sessionFlag(url) {

    // Pass this to the backend so they know when we are opening a new window
    var param = 'clearSession=true';

    // append to the URL correctly, there might already be params in place
    if (url.indexOf('?') === -1) {
        url += '?' + param;
    } else {
        url += '&' + param;
    }

    return url;
}

var analytics = Backbone.Radio.channel('analytics');
var config    = require('config');
var CONST     = require('const');
var ROLES     = require('logic/roles');
var SEL       = require('selectors');
var routes    = require('./data/routes');
var utils     = require('utils');

//var UserInfoView = require('../apps/userinfo/views/userinfo-v');

var EbenRouter = Marionette.AppRouter.extend({

    defaultRoute: function(routeName){

        // Needed if the user is a delegate and tries to go to commissions
        var delegateID = null;
        // Check as above.
        var isDelegate = false;
        var isPolicyholder = false;
        // Will change depending on above.
        var commissionsUrl;
        // Update document.title
        var routeTitle = null;

        // prevent opening any windows to the old stack if they are not logged in
        // Current behavior was trying to open the new winder even if the user
        // was at the login screen.
        this.isPublic = (this.user.get('role') === ROLES._PUBLIC);

        // placeholder value for the url to open
        var url = null;

        utils.logger.log('Route called with name ', routeName);

        // First check if the entire app needs to be reloaded for security reasons
        if (this.state && this.state.securityBlock) {

            if (this.state.securityBlock.needsReload === true) {

                // reload the app to clear out error conditions
                _global.location.href = '/secure/index.html';

            } else {

                // Only permit certain routes?
                if (this.state.securityBlock.allowedRoute) {

                    utils.logger.warn('Security block detected.');
                    utils.logger.warn(' Use can only route to predetermined pages.');
                    routeName = this.state.securityBlock.allowedRoute;
                    // This will cause a reload on the next navigation
                    this.state.securityBlock.needsReload = true;

                }
            }

        } else {

            if (
                this.user &&
                    this.user.get('role') &&
                    this.user.get('role') === ROLES.DELEGATE
            ) {
                isDelegate = true;

                if (!this.delegateSelectionShown) {
                    utils.logger.warn('need to select producer');
                    routeName = routes.SELECT_PRODUCER.LINK;
                    this.delegateSelectionShown = true;
                }
            }

            // if external, go to that page
            // Content app will show a message that it should be open now.
            // BCENTER

            if (routeName === routes.ALERTS.LINK) {
              utils.logger.log('I clicked on the ALERTS');
              //_global.open('#alert-messages');
            }

            if (routeName === routes.USERINFO.LINK) {
              utils.logger.log('I clicked on the USER_INFO');
              console.log(parent);


              //_global.open('#user-info');
            }

            if (routeName === routes.BCENTER.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.BCENTER));
                } else {
                    utils.logger.error('Public role, cannot open a new window for the Business Center');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // BENEFITS
            if (routeName === routes.BENEFITS_PAGES.LINK) {

                if (
                    this.user &&
                        this.user.get('role') &&
                        this.user.get('role') === ROLES.POLICYHOLDER
                ) {
                    isPolicyholder = true;
                }

                if (_global.open && this.isPublic === false) {
                    // Two different URLs depending on the role of the user
                    url = (isPolicyholder === true ? config.hosts.BENEFITS_POLICYHOLDERS : config.hosts.BENEFITS);
                    _global.open(sessionFlag(url));
                } else {
                    utils.logger.error('Public role, cannot open a new window for Benefits');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // Commissions
            if (routeName === routes.COMMISSIONS.LINK) {

                // user MUST have selected a producer to act as
                // before they can continue
                if (isDelegate) {

                    delegateID = this.user
                        .get('delegateProducerAccess');

                    if (delegateID) {
                        delegateID = delegateID.findWhere({ active: true });

                        // route them to the page to select a producer
                        // if they have not done so
                        if (!delegateID) {
                            routeName = routes.SELECT_PRODUCER.LINK;
                            // Change the hash
                            this.navigate(routeName);
                            // And send to that view
                            this.defaultRoute.call(this, routeName);
                            return false;
                        } else {
                            // get the param needed to pass to commssion
                            delegateID = delegateID.get('delegateProducerRelationshipID');
                        }
                    }
                }

                if (_global.open && this.isPublic === false) {

                    // The old stack expects this exact parameter
                    commissionsUrl = config.hosts.COMMISSIONS;
                    if (isDelegate) {
                        commissionsUrl += '?delegateId=' + delegateID;
                    }
                    _global.open(sessionFlag(commissionsUrl));

                } else {
                    utils.logger.error('Public role, cannot open a new window for Commisions');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // DELEGATES
            if (routeName === routes.DELEGATE_ACCESS.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.DELEGATES));
                } else {
                    utils.logger.error('Public role, cannot open a new window for Delegates');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // DISABILITY_INSURANCE
            if (routeName === routes.DISABILITY_INSURANCE.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.DISABILITY_INSURANCE));
                } else {
                    utils.logger.error('Public role, cannot open a new window for the Disability Insurance information page');
                }
            }
            // IBILL
            if (routeName === routes.IBILL.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.IBILL));
                } else {
                    utils.logger.error('Public role, cannot open a new window for iBill');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // Policyholder Registration
            if (routeName === routes.POLICYHOLDER_REGISTRATION.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.POLICYHOLDER_REGISTRATION));
                } else {
                    utils.logger.error('Public role, cannot open a new window for Registration');
                    routeName = routes.LOGIN.LINK;
                }
            }

            // Registration
            // Note that a public role CAN access this link
            if (routeName === routes.REGISTER.LINK) {
                if (_global.open) {
                    _global.open(sessionFlag(config.hosts.REGISTER));
                } else {
                    utils.logger.error('Cannot open a new window for Register');
                }
            }

            // TALENT
            if (routeName === routes.TALENT.LINK) {

                if (_global.open && this.isPublic === false) {
                    _global.open(sessionFlag(config.hosts.TALENT));
                } else {
                    utils.logger.error('Public role, cannot open a new window for the Attractive Benefits page');
                    routeName = routes.LOGIN.LINK;
                }
            }

        } // else for securityBlock check

        if (
            document &&
                document.title &&
                routeName
        ) {
            routeTitle = utils.titleCase(routeName.replace('-', ' '));
            document.title = CONST.TITLE + ' - ' + routeTitle;
        }

        // call analytics with fragment
        analytics.command('trackView', {
            uri     : routeName ? routeName : 'home',
            home    : (routeName === 'home' || !routeName)  ? 1 : 0
        });

        // Send a command back to the main application, telling it what
        // route was called. This lets the app send the content app the
        // info that app needs.
        this.comm.channel.command(this.comm.commandName, routeName);

    },

    // This is used to force showing the producer selection list when the user is a delegate.
    delegateSelectionShown: false,

    initialize: function(options) {
        var self = this;

        this.comm = null;
        this.user = null;

        if (
            options &&
                options.comm &&
                options.comm.channel &&
                options.comm.commandName
        ) {
            // Communications channel to talk back to the application
            this.comm = options.comm;
        } else {
            utils.logger.error('No comm object passed to the router');
            return false;
        }

        if (
            options &&
                options.user &&
                options.user instanceof Backbone.Model
        ) {
            this.user = options.user;
        } else {
            utils.logger.error('No user model passed to the router');
            return false;
        }

        if (
            options &&
                options.state
        ) {
            this.state = options.state;
        }

        // default route, just pass the name of the route each time
        var routes = [
            [ /(.*)/ ]
        ];

        // Will call the route and pass along the text of the route
        // Which is actually an anchor
        _.each(routes, function(rt) {
            self.route.call(self, rt[0], self.defaultRoute);
        });
    }

});

module.exports = EbenRouter;
