var Handlebars = require("hbsfy/runtime");
var CONST = require('const');
var DEL = CONST.HELPERS.FORMS.ATTR_DELIMINATOR;
var utils = require('utils');

function parseAttributes (attrs) {
    var str = '';
    if (!attrs) {
        return str;
    }
    if (utils.isArray(attrs) !== true) {
        attrs = Array.prototype.slice.call(attrs);
    }
    attrs.pop();

    var i;
    var len = attrs.length;
    var propName;
    var propValue;
    var value;

    for (i = 0; i < len; i++) {
        value = attrs[i].split(DEL);
        propName = value.shift();
        propValue = value.join('');
        str += ' ' + propName + '="' + propValue + '"';
    }
    return str;
}

Handlebars.registerHelper({

    // value MUST come first
    label: function(value) {
        if (typeof value !== 'string') {
            throw new Error('label help must have String as first argument');
        }
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        return [
            '<label',
            parseAttributes(args),
            '>',
            value,
            '</label>'
        ].join('');
    },

    select: function(options) {
        if (utils.isArray(options) !== true) {
            throw new Error('select helper must have Array of option objects as first argument');
        }
        var args = Array.prototype.slice.call(arguments);
        args.shift();

        var i;
        var len = options.length;
        var str;

        str = [
            '<select',
            parseAttributes(args),
            '>'
        ].join('');

        for (i = 0; i < len; i++) {
            str += '<option value="' + options[i].key + '">' + options[i].value + '</option>';
        }

        return str + '</select>';
    },

    textInput: function() {
        return [
            '<input type="text"',
            parseAttributes(arguments),
            ' />'
        ].join('');
    },

});

module.exports = Handlebars;
