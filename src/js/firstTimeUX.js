var _global = (function() { return this;})();

var config          = require('config');
var FirstTimeUXView = require('firstTimeUX-v');
var utils           = require('utils');

var firstTimeUX = {

    checkFirstTimeUX: function() {

        var cookieName = config.namespace + '-firstTimeUX';

        // When we stop doing this
        // Arguably this should live in a config file, but this keeps all the code
        // In one place.
        // If this date changes, will need code changes below read the cookie expire
        // date and then remove the cookie using that exact date value.
        var modalEndDate = 'November 4, 2015 00:00:00';
        var modalEndDateObj = new Date(modalEndDate);
        var modalEndDateTimestamp = Date.parse(modalEndDate);
        var currentTime = Date.now();

        if (currentTime > modalEndDateTimestamp) {
            
            // Campaign is over
            // See note above about expiration date.
            _global.Cookies.remove(cookieName, 'false', { expires: modalEndDateObj, path: '/' });
            
        } else {
            
            if (_global.Cookies(cookieName) !== 'false') {
                
                this.initFirstTimeUX();
                _global.Cookies(cookieName, 'false', { expires: modalEndDateObj, path: '/' });
                
            }
            
        }

    },

    initFirstTimeUX: function() {
        // Channel for app to talk to modal
        var modalChannel;
        var modalCommandName;
        
        // Modal for FTUX
        modalChannel     = Backbone.Radio.channel(config.namespace + '-modal');
        modalCommandName = 'modal:destroy';

        var modal = new FirstTimeUXView({
            el: utils.createWrapper({
                wrapper : 'body'
            }),
            comm: {
                channel     : modalChannel,
                commandName : modalCommandName
            }
        }).render();

        // Remove the modal when done
        modalChannel.comply(modalCommandName, function() {
            modal.destroy();
        });
    }

};

module.exports = firstTimeUX;
