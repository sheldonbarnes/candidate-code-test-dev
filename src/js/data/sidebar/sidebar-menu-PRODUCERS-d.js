var ROUTES = require('data/routes');

// Ordering is important here because items are added dynamically in the logic module
/*
var items1 = [
    {
        icon: 'briefcase',
        displayText: 'My Business',
        subItems: []
    },
];

var items2 = require('./sidebar-menu-PRODUCTS-d');

var items3 = require('./sidebar-menu-CLAIMS-d');

var items4 = require('./sidebar-menu-RESOURCES-d');

var items5 = require('./sidebar-menu-FORMS-d');

var items6 = require('./sidebar-menu-CONTACTUS-d');

var items = [].concat(items1, items2, items3, items4, items5, items6);
*/
var items = [];
module.exports = items;
