var ROUTES = require('../routes');

// Lives under the resources tab.
module.exports = [
    {
        icon: 'fax',
        displayText: 'Claims',
        subItems: [
            {
                displayText: 'Filing a Claim',
                subItems: [
                	{
                        displayText: 'Life Claims',
                        link: ROUTES.LIFE_CLAIMS.LINK
                    },
                    {
                        displayText: 'Disability Claims',
                        link: ROUTES.DISABILITY_CLAIMS.LINK
                    }
                ]
            }
        ]
    }
];
