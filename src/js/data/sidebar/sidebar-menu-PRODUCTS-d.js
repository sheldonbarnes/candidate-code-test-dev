var ROUTES = require('data/routes');

// Lives under the resources tab.
module.exports = [
    {
        // Top level items don't have an actual page
        icon: 'book',
        displayText: 'Products & Services',
        subItems: [
            {
                displayText: 'Overview',
                link: ROUTES.PRODUCTS_SERVICES_OVERVIEW.LINK
            },
            {
                displayText: 'Traditional Group',
                subItems: [
                    {
                        displayText: 'Term Life and AD&D',
                        link: ROUTES.TRADITIONAL_TERM_LIFE_AND_ADD.LINK
                    },
                    {
                        displayText: 'Short-term Disability',
                        link: ROUTES.TRADITIONAL_SHORT_TERM_DISABILITY.LINK
                    },
                    {
                        displayText: 'Long-term Disability',
                        link: ROUTES.TRADITIONAL_LONG_TERM_DISABILITY.LINK
                    }
                ]
            },
            {
                displayText: 'Voluntary',
                subItems: [
                    {
                        displayText: 'Term Life and AD&D',
                        link: ROUTES.VOLUNTARY_TERM_LIFE_AND_ADD.LINK
                    },
                    {
                        displayText: 'Whole Life',
                        link: ROUTES.VOLUNTARY_WHOLE_LIFE.LINK
                    },
                    {
                        displayText: 'Worksite Disability',
                        link: ROUTES.VOLUNTARY_WORKSITE_DISABILITY.LINK
                    },
                    {
                        displayText: 'Lump Sum Disability',
                        link: ROUTES.VOLUNTARY_LUMP_SUM_DISABILITY.LINK
                    }
                ]
            },
            {
                displayText: 'Services',
                subItems: [
                    {
                        displayText: 'EAP',
                        link: ROUTES.EAP.LINK
                    },
                    {
                        displayText: 'FMLA Admin',
                        link: ROUTES.FMLA_ADMIN.LINK
                    },
                    {
                        displayText: 'Travel Assistance',
                        link: ROUTES.TRAVEL_ASSISTANCE.LINK
                    },
                    {
                        displayText: 'Online Benefits Admin',
                        link: ROUTES.ONLINE_BENEFITS_ADMIN.LINK
                    }
                ]
            }
        ]
    }
];
