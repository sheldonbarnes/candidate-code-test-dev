var ROUTES = require('data/routes');

var item1 = {
    displayText: 'Policyholder Registration',
    link: ROUTES.POLICYHOLDER_REGISTRATION.LINK
};

module.exports = item1;
