var ROUTES = require('data/routes');

module.exports = [
    {
        icon: 'download-alt',
        displayText: 'Forms',
        link: ROUTES.FORMS.LINK
    }
];
