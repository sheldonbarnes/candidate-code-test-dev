var ROUTES = require('data/routes');

module.exports = [
    {
        icon: 'desktop',
        displayText: 'Contact Us',
        link: ROUTES.CONTACT_US.LINK
    }
];
