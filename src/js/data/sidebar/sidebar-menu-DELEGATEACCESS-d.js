var ROUTES = require('data/routes');

var item1 = {
    displayText: 'Maintain Delegate Access',
    link: ROUTES.DELEGATE_ACCESS.LINK
};

module.exports = item1;
