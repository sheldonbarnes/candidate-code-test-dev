var _global = (function() { return this;})();

var ROUTES = require('data/routes');
/*
// Ordering is important here because items are added dynamically in the logic module
var items1 = [
    {
        // Top level items do not get a link
        icon: 'briefcase',
        displayText: 'Our Plans',
        // This will be populated or removed in the logic
        subItems: []
    }
];

var items2 = require('./sidebar-menu-PRODUCTS-d');

var items3 = [
    {
        icon: 'wrench',
        displayText: 'Sales Resources',
        subItems: [
            {
                displayText: 'Continuing Education',
                link: ROUTES.CONTINUING_EDUCATION.LINK
            },
            {
                displayText: 'Social Media',
                link: ROUTES.SOCIAL_MEDIA.LINK
            }
        ]
    }

];

var items4 = require('./sidebar-menu-CLAIMS-d');

var items5 = _global.$.extend(true, [], require('./sidebar-menu-RESOURCES-d'));

items5[0].subItems.push({
    displayText: 'Tax Services',
    link: ROUTES.TAX_SERVICES.LINK
});

var items6 = require('./sidebar-menu-FORMS-d');

var items7 = require('./sidebar-menu-CONTACTUS-d');

var allItems = [].concat(items1, items2, items3, items4, items5, items6, items7);
*/

var allItems = [];
module.exports = allItems;
