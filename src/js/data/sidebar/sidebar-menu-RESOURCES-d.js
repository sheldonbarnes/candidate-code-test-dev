var ROUTES = require('data/routes');

module.exports = [
    {
        icon: 'info-circle',
        displayText: 'FAQs & Resources',
        link: ROUTES.RESOURCES.LINK,
        subItems: [
            {
                displayText: 'Life',
                link: ROUTES.LIFE.LINK
            },
            {
                displayText: 'Disability',
                link: ROUTES.DISABILITY.LINK
            },
            {
                displayText: 'Billing and Payments',
                link: ROUTES.BILLING_PAYMENTS.LINK
            },
            {
                displayText: 'Admin of Benefits',
                link: ROUTES.ADMIN_OF_BENFITS.LINK
            },
            {
                displayText: 'Guides and Tools',
                link: ROUTES.TOOLS.LINK
            },
            {
                displayText: 'Training',
                link: ROUTES.TRAINING.LINK
            }
        ]
    }
];
