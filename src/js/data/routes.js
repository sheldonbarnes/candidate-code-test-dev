// This is the only piece that's not explicity detailed in one place.
// The Content application has a set of views, and that's what we're mapping
// to here. The names of the required-in view modules in the content view
// MUST match the names used here.

var CONTENTVIEWS = {
    __NEW_WINDOW             : 'NewWindowView',
    __UNKNOWN                : 'None specified in routes.js for the main app',
    BENEFITS                 : 'BenefitsView',
    HOME                     : 'HomeView',
    USERINFO                 : 'UserInfoView',
    ALERTS                   : 'AlertsView',
    PRODUCTS                 : 'ProductsView',
    FAQ                      : 'FaqView',
    FORMS                    : 'FormsView',
    LOGIN                    : 'LoginView',
    SECURITY_CHANGE_INFO     : 'SecurityChangeInfoView',
    SECURITY_CHANGE_USERID   : 'SecurityChangeUserIdView',
    SECURITY_CHANGE_PASSWORD : 'SecurityChangePasswordView',
    SECURITY_FORGOT_PASSWORD : 'SecurityForgotPasswordView',
    SECURITY_FORGOT_USERID   : 'SecurityForgotUserIdView',
    SELECT_PRODUCER          : 'SelectProducerView',
    TRAINING                 : 'TrainingView'
};

// Piece that ties it all together.
// Used to tell the sidebar what the links should be
// Used to tell the content app what view to render by matching against the route
// Used to tell the wcmContent app what content to load

// For FAQ views, the CONTENTVIEW.FAQ is a flag that also tells the code
// it needs to load the FAQs from the special data location.
var routes = {
    ALERTS: {
        CONTENTVIEW: CONTENTVIEWS.ALERTS,
        LINK: 'alert-messages',
        OPTIONS: null,
        WCMID: null
    },
    USERINFO: {
        CONTENTVIEW: CONTENTVIEWS.USERINFO,
        LINK: 'user-info',
        OPTIONS: null,
        WCMID: null
    },
    BENEFITS_PAGES: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'benefits-pages',
        OPTIONS: null,
        WCMID: null
    },
    BILLING_PAYMENTS: {
        CONTENTVIEW: CONTENTVIEWS.FAQ,
        LINK: 'billing-payments',
        // OPTIONS contains any metadata needed to when processing
        // this route.
        OPTIONS: {
            isFaq: true
        },
        WCMID: '871138cc-5cf7-4e33-947c-e8f632cff6e5'
    },
    CHANGE_SECURITY_INFO: {
        CONTENTVIEW: CONTENTVIEWS.SECURITY_CHANGE_INFO,
        LINK: 'change-security-info',
        OPTIONS: {
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Change Your Security Information' ]
        },
        WCMID: null
    },
    CHANGE_PASSWORD: {
        CONTENTVIEW: CONTENTVIEWS.SECURITY_CHANGE_PASSWORD,
        LINK: 'change-password',
        OPTIONS: {
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Change Your Password' ]
        },
        WCMID: null
    },
    CHANGE_USERID: {
        CONTENTVIEW: CONTENTVIEWS.SECURITY_CHANGE_USERID,
        LINK: 'change-user-id',
        OPTIONS: {
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Change Your User ID' ]
        },
        WCMID: null
    },
    COMMISSIONS: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'commissions',
        OPTIONS: null,
        WCMID: null
    },
    CONTACT_US: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'contact-us',
        OPTIONS: {
            isPublic: true
        },
        WCMID: '1bbbbaba-51c6-4aa2-99be-5f53de278855'
    },
    CONTINUING_EDUCATION: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'continuing-education',
        OPTIONS: null,
        WCMID: 'cde26d22-4867-4c14-865a-278a7120da47'
    },
    DELEGATE_ACCESS: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'delegate-access',
        OPTIONS: null,
        WCMID: null
    },
    DISABILITY: {
        CONTENTVIEW: CONTENTVIEWS.FAQ,
        LINK: 'disability',
        // OPTIONS contains any metadata needed to when processing
        // this route.
        OPTIONS: {
            isFaq: true
        },
        WCMID: '4d7aa3ce-464e-4e19-872e-78ba6ad2b306'
    },
    DISABILITY_CLAIMS: {
        CONTENTVIEW: CONTENTVIEWS.FAQ,
        LINK: 'disability-claims',
        // OPTIONS contains any metadata needed to when processing
        // this route.
        OPTIONS: {
            isFaq: true
        },
        WCMID: 'eee17e97-0c7f-44e9-9dbb-81fd16ccb61c'
    },
    DISABILITY_INSURANCE: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'disability-insurance',
        OPTIONS: null,
        WCMID: null
    },
    EAP: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'eap',
        OPTIONS: null,
        WCMID: '53fd9852-9edb-43ff-8a9e-06f95453c20a'
    },
    FMLA_ADMIN: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'fmla-admin',
        OPTIONS: null,
        WCMID: 'ca70463b-99ec-4c09-b321-9852d62ecf4b'
    },
    FORGOT_PASSWORD: {
        CONTENTVIEW: CONTENTVIEWS.SECURITY_FORGOT_PASSWORD,
        LINK: 'forgot-password',
        OPTIONS: {
            isPublic: true,
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Forgot Password' ]
        },
        WCMID: null
    },
    FORGOT_USERID: {
        CONTENTVIEW: CONTENTVIEWS.SECURITY_FORGOT_USERID,
        LINK: 'forgot-user-id',
        OPTIONS: {
            isPublic: true,
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Forgot User ID' ]
        },
        WCMID: null
    },
    FORMS: {
        CONTENTVIEW: CONTENTVIEWS.FORMS,
        LINK: 'forms',
        OPTIONS: {
            isPublic: true
        },
        WCMID: '4bda2b44-8e53-4bd6-8446-d22006a597ed'
    },
    HOME: {
        CONTENTVIEW: CONTENTVIEWS.HOME,
        LINK: 'home',
        OPTIONS: {
            isPublic: true
        },
        WCMID: 'bce7b26c-5c72-40bf-a431-92516d94d29a'
    },
    IBILL: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'ibill',
        OPTIONS: null,
        WCMID: null
    },
    LIFE: {
        CONTENTVIEW: CONTENTVIEWS.FAQ,
        LINK: 'life',
        // OPTIONS contains any metadata needed to when processing
        // this route.
        OPTIONS: {
            isFaq: true
        },
        WCMID: '4a9fd932-cb04-418b-b240-7fa188d34654'
    },
    LIFE_CLAIMS: {
        CONTENTVIEW: CONTENTVIEWS.FAQ,
        LINK: 'life-claims',
        // OPTIONS contains any metadata needed to when processing
        // this route.
        OPTIONS: {
            isFaq: true
        },
        WCMID: 'bcb15e1b-e992-4cba-9a76-bfde03371b8e'
    },
    LOGIN: {
        CONTENTVIEW: CONTENTVIEWS.LOGIN,
        LINK: 'login',
        OPTIONS: null,
        WCMID: '23100bfb-69d7-4c14-ab3a-5d0ce2a6e576'
    },
    LOGOUT: {
        CONTENTVIEW: null,
        LINK: '/public/logout.html',
        OPTIONS: null,
        WCMID: null
    },
    ONLINE_BENEFITS_ADMIN: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'online-benefits-admin',
        OPTIONS: null,
        WCMID: '782901b0-f192-40bd-8feb-2addf2728bf5'
    },
    PRODUCTS_SERVICES_OVERVIEW: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'products-services-overview',
        OPTIONS: null,
        WCMID: '0877b520-1fbd-42b3-92b2-b52f2ab46c03'
    },
    POLICYHOLDER_REGISTRATION: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'policyholder-registration',
        OPTIONS: null,
        WCMID: null
    },
    REGISTER: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'register',
        OPTIONS: {
            isPublic: true
        },
        WCMID: null
    },

    BCENTER: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'resources',
        OPTIONS: null,
        WCMID: '25cadcef-405e-44e9-97f9-cb8b4fadface'
    },
    RESOURCES: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'resources',
        OPTIONS: null,
        WCMID: '25cadcef-405e-44e9-97f9-cb8b4fadface'
    },
    SELECT_PRODUCER: {
        CONTENTVIEW: CONTENTVIEWS.SELECT_PRODUCER,
        LINK: 'select-producer',
        OPTIONS: {
            isPublic: false,
            // Pass an array here ONLY if it's not part of the main navigation
            navStructure: [ 'Select Producer' ]
        },
        WCMID: null
    },
    SOCIAL_MEDIA: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'social-media',
        OPTIONS: null,
        WCMID: 'aba26c88-024e-4c5d-8544-5957afb2cec1'
    },
    TALENT: {
        CONTENTVIEW: CONTENTVIEWS.__NEW_WINDOW,
        LINK: 'talent',
        OPTIONS: null,
        WCMID: null
    },
    TAX_SERVICES: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'tax-services',
        OPTIONS: null,
        WCMID: '73f73b5c-547b-4529-8ba2-66dffd17615f'
    },
    TOOLS: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'tools',
        OPTIONS: null,
        WCMID: '45690100-8f5e-4758-abff-c7b3fcb59af5'
    },
    TRADITIONAL_LONG_TERM_DISABILITY: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'traditional-long-term-disability',
        OPTIONS: null,
        WCMID: '8a067c4c-6bc4-4c7a-85ce-98cc13f23b4b'
    },
    TRADITIONAL_SHORT_TERM_DISABILITY: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'traditional-short-term-disability',
        OPTIONS: null,
        WCMID: '8bb14f02-4553-4c9d-9b1f-b549e072f13a'
    },
    TRADITIONAL_TERM_LIFE_AND_ADD: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'traditional-term-life-and-add',
        OPTIONS: null,
        WCMID: '80d7b3c5-526d-4173-b5eb-aff4a8b6a882'
    },
    TRAINING: {
        CONTENTVIEW: CONTENTVIEWS.TRAINING,
        LINK: 'training',
        OPTIONS: null,
        WCMID: 'f0f76e3b-6d64-45e2-82f9-1be373013fda'
    },
    TRAVEL_ASSISTANCE: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'travel-assistance',
        OPTIONS: null,
        WCMID: 'afc28e2c-cc72-4a65-b090-a1759aab0634'
    },
    VOLUNTARY_LUMP_SUM_DISABILITY: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'voluntary-lump-sum-disability',
        OPTIONS: null,
        WCMID: '3fc8bd60-c3a6-4495-93bb-7fdd322d305b'
    },
    VOLUNTARY_WHOLE_LIFE: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'voluntary-whole-life',
        OPTIONS: null,
        WCMID: '6ef2da42-5002-429f-aff5-9f31ed9b6a07'
    },
    VOLUNTARY_WORKSITE_DISABILITY: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'voluntary-worksite-disability',
        OPTIONS: null,
        WCMID: 'f168ee35-ed68-47ba-960c-9b95090cd03e'
    },
    VOLUNTARY_TERM_LIFE_AND_ADD: {
        CONTENTVIEW: CONTENTVIEWS.PRODUCTS,
        LINK: 'voluntary-term-life-and-add',
        OPTIONS: null,
        WCMID: '767aba9e-6ba4-479a-af66-3b418d19e23c'
    }
};

module.exports = routes;
