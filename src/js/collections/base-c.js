var Backbone = require('backbone');

var BaseCollection = Backbone.Collection.extend({});

module.exports = BaseCollection;
