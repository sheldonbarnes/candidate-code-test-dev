var DelegateProducerRelationshipModel = require('../models/delegate-producer-relationship-m');

var DelegateProducerRelationshipCollection = Backbone.Collection.extend({

    model: DelegateProducerRelationshipModel

});

module.exports = DelegateProducerRelationshipCollection;
