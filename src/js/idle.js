var _global = (function() { return this; })();

var utils = require('utils');

/*
 Idle Watcher
 Monitors user activity, and periodically lets Siteminder on the server know that the
 user is still active so that the Siteminder session won't time out.

 Usage:      var idleWatcher = require('idleModule');

 //option 'callback' will be a method to call after idle timeout
 //currently provided 'logout' method from userModule
 idleWatcher.start({
     // optional, will default to half hour
     idleTime: 10000,
     // optional, will default to one min
     pingTime: 60000,
     // Assuming  you have a logout function defined
     callback: logout()
 });

 */

var IdleModule = (function () {

    var idleTimerID = null;

    //1 min, in millis
    var oneMin = 60000;

    // 30 minutes, in millis
    var halfHour = 1800000;

    //singleton object of idle module
    var idleModule;

    var idleCallback;

    //Idle master class
    var Idle = function (){

        var self = this;
        
        /**
         * Send a GET request to let Siteminder know that the user is still active.
         * To avoid cross-origin problems, the GET request must come from the browser itself
         * and not from JavaScript. We accomplish this by changing the src property of a
         * hidden image. The browser will request the new image file, thus generating the GET.
         *
         * Since the request is a direct GET on the browser, if the session has expired,
         * SiteMinder will return a 30x which the browser will follow, forcing the user to
         * log in again. This is also why it CANNOT be an XHR - if it was, the browser would
         * just follow the XHR versus redirecting to the new page.
         */
        var ping = function() {
            // Insert the image tag into the DOM if it's not already there
            var imageEl = $('#smping');
            if (imageEl.length === 0) {
                imageEl = $('<img>').prop('id', 'smping').hide().appendTo('body');
            }
            
            // Set the src of the image to the blank image, adding a timestamp querystring
            // param to force the image to reload
            /* Date.now() = IE9 AND ABOVE. EVAN 6/17/2014 */
            imageEl.prop('src', '/secure/servlet/BlankImage?_=' + ( +new Date().valueOf() ) );  
        };

        /**
         * Event handler triggered on user activity.
         */
        var activityDetected = function (event) {
            
            var restartTimer;

            // By default, this event handler will execute once for each event type: click,
            // scroll, and keydown. We only want to react to *one* of those events, so cancel
            // the event listener here.
            $(_global).off('click scroll keydown', activityDetected);

            // Restart the idle timer
            startIdleTimer();

            // Notify Siteminder that the user is active
            ping();

            // Restart the activity listener
            restartTimer = _global.setTimeout(startActivityListener, self.pingTime);
        };

        /**
         * Start listening for user activity: mouse clicks, keypresses, or _global scrolling.
         */
        var startActivityListener = function () {
            // Only catch the first activity event, then stop listening.
            $(_global).one('click scroll keydown', activityDetected);
        };

        /**
         * User has been idle for too long
         */
        var idleTimeout = function () {
            if (self.idleCallback){
                self.idleCallback('Session timed out');
            }
        };

        /**
         * Start (or restart) the idle timer.
         * If the timer is allowed to complete, the user will be logged out of the application.
         */
        var startIdleTimer = function () {
            if (idleTimerID) {
                _global.clearTimeout(idleTimerID);
            }
            idleTimerID = _global.setTimeout(idleTimeout, self.idleTime);
        };

        //prerocess rules to make private method to public 
        //which enable unit testing on private methods
        /* @ifdef TEST **
         this.startActivityListener = startActivityListener;
         this.startIdleTimer = startIdleTimer;
         /* @endif */


        /**
         * Kick off the Idle Watcher
         */
        this.start = function (options) {

            // defaults
            self.pingTime = oneMin;
            self.idleTime = halfHour;
            
            // Effectively a no-op
            self.idleCallback = function() {
                utils.logger.warn('No callback defined for the idleTimer module');
                utils.logger.warn('Using default no-op as callback');
                return true;
            };

            // Verify we have integers if passed times to use
            options.idleTime = parseInt(options.idleTime, 10);
            options.pingTime = parseInt(options.pingTime, 10);

            // Override defaults
            if (
                options.idleTime &&
                    _.isNumber(options.idleTime) &&
                    options.idleTime !== 0
            ) {
                self.idleTime = options.idleTime;
            }
            
            if (
                options.pingTime &&
                    _.isNumber(options.pingTime) &&
                    options.pingTime !== 0 &&
                    options.idleTime > options.pingTime
            ) {
                self.pingTime = options.pingTime;
            }

            if (
                options.callback &&
                    _.isFunction(options.callback) === true
            ) {
                self.idleCallback = options.callback;
            }
            
            startActivityListener();
            startIdleTimer();

            //prerocess rules to call public mthod which are defined private scope
            //which enable unit testing(spy) on private methods
            /* @ifdef TEST **
             this.startActivityListener(pingTime);
             this.startIdleTimer(idleTime);
             /* @endif */
        };

    };

    if (!idleModule) {
        idleModule = new Idle();
    }

    return idleModule;  

})();

module.exports = IdleModule;
