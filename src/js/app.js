

var _global = (function() { return this;})();

// Base setup needs
//var analytics   = require('./analytics/analyticsModule');
var config      = require('config/config');



//var logic       = require('logic');
var menuUtils   = require('menu-utils');
//var roles       = require('logic/roles');
var SEL         = require('selectors');
var utils       = require('utils');

// Views
var ErrorView   = require('error-v');
var LoadingView = require('loading-v');

// Apps
var Content     = require('../apps/content');
var Alerts      = require('../apps/alerts');
//var UserInfo    = require('../apps/userinfo');
var Footer      = require('../apps/footer');
var Navbar      = require('../apps/navbar');

// Containing element for each app
// This allows the app to specify certain HTML w/o the main app
// needing to know anything about the HTML itself.
var contentEl   = require('../apps/content/el');
var alertsEl    = require('../apps/alerts/el');
//var userInfoEl    = require('../apps/userinfo/el');
//var footerEl    = require('../apps/footer/el');
var navbarEl    = require('../apps/navbar/el');

// Routing and data
var MainRouter = require('main-rtr');
var routes     = require('data/routes');

// User info
var UserModel = require('models/user-m');
var AlertsModel = require('models/alerts-m');

// App will be exposed on the window
var app;

var ROLES    = require('logic/roles');
// This is to avoid multiple 'selfs'
var _self = this;

// Make sure we are not framed
//frameBuster();

// Hold some application state like errors, security
_self.state = {};

//////////////////////////////////////////////////
// START APP LOGIC
//////////////////////////////////////////////////

// Error out immediately if this app is already running
if (_global[config.namespace]) {
    throw new Error('Namespace already exists, cannot create again.');
} else {
    // App is the globally exposed item
    app = _global[config.namespace] = new Marionette.Application();
}

// What sort of runtime configuration do we have?
utils.logger.log('Config is:');
utils.logger.log(config);

// Default ajax timeout
_global.$.ajaxSetup({
    timeout: (parseInt(config.hosts.AJAX_TIMEOUT, 10) || 10000)
});

// Create a model for our user information. What is displayed in the app
// all flows from the role property defined in this model.
app.user = new UserModel();
app.alerts = new AlertsModel();

// Heart of the application logic. This is the real starting point for
// the application because at this point a user role has been defined.
// At this point we have a user role defined either as a set of data
// fetched from a REST call, or because the REST call threw a 401 back
// because no user was logged in.
// This will be triggered by the loadUser function below

this.alertsComplete = function(model, response, option){
  utils.logger.log('In the alerts model fetch callback');

};

this.userComplete = function(model, response, option){

    // Remove the spinner
    _self.initialLoadingView.destroy();

    var self = this;

    utils.logger.log('In the user model fetch callback');

    // For public roles, we may not turn on the menu
    var navUserConfig;

    // Will store the collection of menu data that we will assemble below
    // based on the user role.
    var menu;
    var navStructure;

    // The global reference to the app itself. This is used to create a single
    // namespace on the global object, so each subapp can exist inside that
    // namespace. Example - window.EBEN will exist, and so will window.EBEN.SIDEBAR
    var parent;

    // Channels for Backbone.Radio. This is used to have navigation events
    // be passed into the content app, so it knows what views to show. This means
    // that the sidebar application defined later can use links like #contact-us
    // and not worry about what has to happen when that link is clicked.

    // Channel for router to talk back to app, and command to fire.
    var routerChannel;
    var routerCommandName;
    var routerCommandCallback;
    // Channel for app to talk to content app, and command to fire.
    var contentChannel;
    var contentCommandName;

    // Namespace, as per above.
    parent = _global[config.namespace];

    // Logic module should make all decisions about business logic, such as
    // what menus to show depending on the user model.
    //menu = logic.getMenuType(app.user);
    // Determine the navigation structure to pass into the content app
    // This will be used to render out the breadcrumbs
    parent.navStructure = menuUtils.createTitleLookup($.extend(true, [], menu));

    // expose the routes on the app
    parent.routes = routes;

    // Router will see the hashchange events triggered by the Sidebar
    // Will pass along information on what links in the navigation were clicked.
    // The pattern for channel should be var actionChannel uses NAMESPACE-action
    // Need to think this through more - should this be onrouteChannel, or
    // routeChannel instead?
    // Channels should be passed via a 'comm' property on subobjects
    // As in 'communication'. A 'comm' property should define the channel and the
    // action that the listener will take (reply, command, etc);
    // By using reply, the main application should be able to know what happened
    // on the child.
    routerChannel     = Backbone.Radio.channel(config.namespace + '-router');
    routerCommandName = 'router:route';
    contentChannel    = Backbone.Radio.channel(config.namespace + '-content');
    contentCommandName = 'content:route';

    // The app will comply with the route command and tell the content app what it
    // needs to know in order to render
    routerCommandCallback = function(routeName) {

        if (parent.navStructure[routeName]) {

            // If the item is part of the main navigation, we can get that info from there
            // note that if it is not currently defined here, it will get appended below
            // so that we do not continue to look it up each time.
            navStructure = parent.navStructure[routeName];

        } else {

            // If it is not part of the main navigation, then it can be set
            // directly in the routes.js file
            navStructure = _.find(parent.routes, function(route) {
                return route.LINK === routeName;
            });

            if (navStructure && navStructure.OPTIONS && navStructure.OPTIONS.navStructure) {
                navStructure = navStructure.OPTIONS.navStructure;
            }

            // Did we find just the object in the routes, or did we find an actual array
            navStructure = (_.isArray(navStructure) === true ? navStructure : []);

            if (navStructure.length !== 0) {
                // append to the existing object to save a lookup next time
                parent.navStructure[routeName] = navStructure;
            }
        }

        // Finally, send the content app the command that will trigger the rendering
        // of the content view.
        contentChannel.command(contentCommandName, routeName, navStructure);
    };

    // When a route is called, the router will send a message back to the application
    // The router should be passing a data object that contains the needed information
    // And the application should then send a message to the content application
    // And forward that data
    routerChannel.comply(routerCommandName, routerCommandCallback);

    // Init the router now that all messaging is in place
    parent.EbenRouter = new MainRouter({
        comm: {
            channel     : routerChannel,
            commandName : routerCommandName
        },
        state: _self.state,
        user: app.user,
        alerts_1: app.alerts
    });

    // Start defining the subapplications.
    // The pattern should be to setup the application, then start it.
    // The order is important, because the root element of each application
    // may depend on an order in the DOM.
    if (app.user.get('role') === ROLES._PUBLIC) {

        navUserConfig = false;

    } else {

        // If here, then we have a logged in user

        navUserConfig = {
            data                    : app.user,
            alerts                : app.alerts,
            selectProducerRoute     : (app.user.get('role') === ROLES.DELEGATE ? parent.routes.SELECT_PRODUCER.LINK : null),
            logoutRoute             : routes.LOGOUT.LINK,
            alertsRoute             : routes.ALERTS.LINK,
            userInfoRoute           : routes.USERINFO.LINK,
            changePasswordRoute     : routes.CHANGE_PASSWORD.LINK,
            changeSecurityInfoRoute : routes.CHANGE_SECURITY_INFO.LINK,
            changeUserIdRoute       : routes.CHANGE_USERID.LINK
        };
    }

    parent.Navbar = new Navbar({
        API: {
            messages      : false,
            notifications : false,
            tasks         : false,
            user          : navUserConfig,
            alerts        : this.alerts
        },
        $rootEl: utils.createWrapper({
            el: navbarEl,
            prepend: true
        })
    });
    parent.Navbar.start();

    parent.Content = new Content({
        $rootEl: utils.createWrapper({
            el      : contentEl,
            prepend : false,
            wrapper : SEL.WRAPPER._SELF
        }),
        comm: {
            // What channel we will listen to, and what command we will comply with
            channel     : contentChannel,
            commandName : contentCommandName
        },
        // This part of the content app is getting out of control.
        // It is too tighly coupled to this file, but it turns out that
        // these items are needed to allow the content app to properly display.
        // Most of these could be setup using a channel as well, so that they
        // are only called as needed.
        errorView   : ErrorView,
        hosts       : config.hosts,
        loadingView : LoadingView,
        namespace   : config.namespace,
        routes      : routes,
        state       : _self.state,
        user        : app.user,
        alertmessages       : app.alerts,
        userRole    : app.user.get('role')
    });
    Backbone.history.start();

};

this.loadUser = function() {
    function _loadUser() {
        app.user.fetch({
            cache   : false,
            error   : _self.userComplete,
            success : _self.userComplete
        });
    }
    app.alerts.fetch();
    setTimeout(_loadUser, 1 );
};

// Start showing some UI - the initial loading spinner
// This is triggered by the before:start of the app below.
this.showIntialLoadingView = function () {

    // Remove the HTML message that says we are starting.
    $(SEL.INITIAL_LOADING).remove();

    // Create the view itself and show it.
    _self.initialLoadingView = new LoadingView({
        el: utils.createWrapper()
    }).render();

};

//////////////////////////////////////////////////
// START THE APPLICATION
//////////////////////////////////////////////////

// set up events for when the app starts
app.on('before:start', this.showIntialLoadingView);
app.on('start', this.loadUser);
app.start();
