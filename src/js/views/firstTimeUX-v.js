var firstTimeUXTemplate = require('firstTimeUX-t');
var SEL                 = require('selectors');

var FirstTimeUXView = Marionette.ItemView.extend({

    attachCleanup: function() {
        this.modal.on('hidden.bs.modal', this.cleanup);
    },
    
    cleanup: function() {
        if (this.comm && this.comm.channel) {
            this.comm.channel.command(this.comm.commandName);
        }
    },
    
    initialize: function(options) {

        _.bindAll(this,
                  'attachCleanup',
                  'cleanup'
                 );
        
        // This view can let the main view know when to destroy it
        if (options.comm && options.comm.channel && options.comm.commandName) {
            this.comm = options.comm;
        }
    },
    
    onRender: function() {
        var self = this;
        this.modal = this.$el.find(SEL.FIRST_TIME_UX.MODAL);
        // Slight delay to allow rendering to finish
        setTimeout(function() {
            self.modal.modal();
            self.modal.on('shown.bs.modal', self.attachCleanup);
        }, 1000);
    },

    template: firstTimeUXTemplate,

    templateHelpers: function() {
        return {
            MODAL: SEL.FIRST_TIME_UX.MODAL.substr(1)
        };
    }

});

module.exports = FirstTimeUXView;
