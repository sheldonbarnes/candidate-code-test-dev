var loadingTemplate = require('loading-t');

var LoadingView = Marionette.ItemView.extend({

    onBeforeRender: function(){
        this.spinner = new Spinner().spin();
    },

    onRender: function(){
        this.$el.html(this.spinner.el);
    },

    onBeforeDestroy: function(){
        this.spinner.stop();
    },

    template: loadingTemplate

});

module.exports = LoadingView;
