var errorTemplate = require('error-t');

var ErrorView = Marionette.ItemView.extend({

    template: errorTemplate

});

module.exports = ErrorView;
