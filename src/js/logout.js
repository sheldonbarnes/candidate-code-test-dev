var _global = (function() { return this;})();

var routes = require('data/routes');

function logout() {
    _global.location.href = routes.LOGOUT.LINK;
}

module.exports = logout;
