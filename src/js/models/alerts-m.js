var BaseModel                              = require('base-m');
var config                                 = require('config');


var AlertsModel = BaseModel.extend({

    defaults:
    {
        "items" :
        [ {
          "id" : null,
          "message" : null,
          "messageType" : null,
          }
        ]
    },
    parse: function(response) {
        return response;
    },

    urlRoot: function() {
        return 'http://private-d7f0fa-sheldonbarnes.apiary-mock.com/alerts';
        //return config.apiBaseUrl + '/alerts';
    }
});

module.exports = AlertsModel;
