var BaseModel = require('base-m');

var DelegateProducerRelationshipModel = BaseModel.extend({

    defaults: function() {
        return {
            active                         : false,
            delegateProducerRelationshipID : null,
            producerTaxId                  : null,
            producerName                   : null
        };
    },

    idAttribute: 'delegateProducerRelationshipID'

});

module.exports = DelegateProducerRelationshipModel;
