var BaseModel                              = require('base-m');
var config                                 = require('config');
var DelegateProducerRelationshipCollection = require('../collections/delegate-producer-relationships-c');
var ROLES                                  = require('logic/roles');

var UserModel = BaseModel.extend({

    defaults: function() {
        return {

            //delegateProducerAccess: new DelegateProducerRelationshipCollection(),
            //eladId      : null,
            id: null,
            email: null,
            firstName   : null,
            lastName    : null,
            email_address: null,
            permissions : {
                benefits            : false,
                commissions         : false,
                csrsiteaccess       : false,
                ibill               : false,
                registration        : false
            },
            role        : ROLES._PUBLIC,
            userId      : null
        };
    },

    parse: function(response) {
        //console.log('Parsing' + JSON.stringify(response));
        var data;

        if (
            response &&
                response.items &&
                _.isArray(response.items) === true &&
                response.items[0]
        ) {
            data = response.items[0];
            if (!data.role) {
                data.role = ROLES._PUBLIC;
            }

            // Override for a role we found late
            // Can be removed once WCM permit/deny rules are updated
            if (data.role === ROLES.HOMEOFFICEREMOTE) {
                data.role = ROLES.HOMEOFFICE;
            }

            if (
                data.delegateProducerAccess &&
                    _.isArray(data.delegateProducerAccess) === true
            ) {
                data.delegateProducerAccess = new DelegateProducerRelationshipCollection(data.delegateProducerAccess);
            } else {
                data.delegateProducerAccess = [];
            }

        }

        return data;
    },

    urlRoot: function() {
        return 'http://private-d7f0fa-sheldonbarnes.apiary-mock.com/user';
        //return config.apiBaseUrl + '/user';
    },

    validate: function(attributes, options) {
        if (typeof ROLES[attributes.role.toUpperCase()] === 'undefined') {
            return 'Role must be one defined in CONST.ROLES';
        }
    }

});

module.exports = UserModel;
