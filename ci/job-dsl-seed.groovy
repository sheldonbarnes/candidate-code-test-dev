//////////////////////////////////////////////////
// SETUP
//////////////////////////////////////////////////

// servers
def SERVER_SB = [ 'SLDWEB11', 'SLDWEB12' ]

def gitRepoName = 'employeebenefits-frontend'
def gitGroupName = 'ebus'
// we use a main folder in jenkins
def parentFolder = 'EBUS'
def jobName = "EBUS_${gitRepoName}"
def jobNameTemp = "${jobName}-TEMP"

// gradle version
def gradleBuildFile = 'build.gradle'
def gradleVersion = 'Gradle 2.1'

// this is a location on the prod Jenkins machine
def scriptFixBranch = '/e/DEV/git/jenkins-utils-dev/scripts/checkout-local-tracking-branch.sh'

// our branching/job strategy
def branches = [ master: 'master', feature: 'feature' ]

// environments
// add st: 'ST' and ft: 'FT'
def environments = [ sb: 'SB' ]

def gitUrl = "git@bitbucket.org:ebus/${gitRepoName}.git"

// variations in the jobs
// No real need to archive files in some builds, but the build fails if you don't
def archivePattern   = 'release/**/*'
def gradleTasks      = ''
def gitBranchPattern = ''

// MANUAL builds use these
def stringParamBranch     = null
def choiceParamPublish    = null
def choiceParamScope      = null
def choiceParamTask       = null

//////////////////////////////////////////////////
// BEGIN ACTUAL JOB CREATION
//////////////////////////////////////////////////

// make a folder for the series of jobs
// that will be created
// We use TEMP here to allow moving this job
folder("${parentFolder}/${jobNameTemp}"){}

// this will be in the folder created above since we use the /
def jobNamePrefix = "${parentFolder}/${jobNameTemp}/${jobName}"
def manualJobName = jobNamePrefix + "_MANUAL_build"

//////////////////////////////////////////////////
// BRANCH based builds
//////////////////////////////////////////////////

for ( i in branches.values() ) {

  def branchJobName = jobNamePrefix + "_${i}_build"

  // branch/job specific settings
  if (i == 'master') {
    gitBranchPattern = '*/master'
    gradleTasks = 'build snapshot'
  } else if (i == 'feature') {
    gitBranchPattern = '*/feature/*'
    gradleTasks = 'build devSnapshot'
  }

  freeStyleJob(branchJobName) {

    environmentVariables {
      keepSystemVariables(true)
      keepBuildVariables(true)
      envs([
            GIT_PROJECT_NAME  : gitRepoName,
            GIT_GROUP_NAME    : gitGroupName,
            SCRIPT_FIX_BRANCH : scriptFixBranch
           ])
    }

    // log rotation
    // 20 days or 20 builds
    logRotator(20, 20, 20, 20)

    // use git
    scm {
      git {
        remote {
          url(gitUrl)
        }
        branch(gitBranchPattern)
        // this is single quotes so that it writes out the actual string
        // ${GIT_BRANCH}, which is an env variable we want to use
        localBranch('${GIT_BRANCH}')
      }
    }

    wrappers {
      // inject passwords
      injectPasswords()

    }

    // actual build steps
    steps {
      // gets rid of any files except node_modules and the checksum file
      // gives faster builds because we may not need to run npm install
      shell('git clean -xfd --exclude=checksum.txt --exclude=node_modules --exclude=bin')
      // get a checkout that we can push to
      shell('$SHELL -ex ${SCRIPT_FIX_BRANCH}')
      // do we need to run npm install?
      shell('$SHELL -ex ./scripts/checksum.sh')
      shell('git submodule update --init');

      gradle {
        tasks(gradleTasks)
        useWrapper(false)
        buildFile(gradleBuildFile)
        gradleName(gradleVersion)
      }

    }

    // archive files
    publishers {

      // what to save, no excludes, keep all
      archiveArtifacts(archivePattern, null, false)

      extendedEmail(null, '$DEFAULT_SUBJECT', '$DEFAULT_CONTENT') {
        trigger([
                 triggerName: 'Failure',
                 sendToDevelopers: true,
                 sendToRequester: true,
                 includeCulprits: true,
                 sendToRecipientList: false
                ])
        trigger([
                 triggerName: 'Success',
                 sendToDevelopers: false,
                 sendToRequester: true,
                 includeCulprits: false,
                 sendToRecipientList: false
                ])

      }
    }

  }

}

//////////////////////////////////////////////////
// MANUAL builds
//////////////////////////////////////////////////


freeStyleJob(manualJobName) {

  parameters {
    stringParam("BRANCH_NAME", "", "The branch you wish to build")
    choiceParam("TASK", ["candidate", "final"], "Kind of Nebula release plugin task to run")
    choiceParam("SCOPE", [" ", "patch", "minor", 'major' ], "Choose the 'blank' entry if you want the inference of the version to happen automatically. Pick the specific scope if you need to specify.")
  }

  environmentVariables {
    keepSystemVariables(true)
    keepBuildVariables(true)
    envs([
          GIT_PROJECT_NAME  : gitRepoName,
          GIT_GROUP_NAME    : gitGroupName,
          SCRIPT_FIX_BRANCH : scriptFixBranch
         ])
  }

  // log rotation
  // 20 days or 20 builds
  logRotator(20, 20, 20, 20)

  // use git
  scm {
    git {
      remote {
        url(gitUrl)
      }
      branch('${BRANCH_NAME}')
      localBranch('${GIT_BRANCH}')
    }
  }

  wrappers {
    // inject passwords
    injectPasswords()

    // wipe out all files before start
    preBuildCleanup {
      // this intentionally commented out because that means all files deleted
      // includePattern(String pattern)  // all files are deleted if omitted
      //        deleteDirectories(true) // defaults to false if omitted
    }
  }

  // actual build steps
  steps {

    // gets rid of any files except node_modules and the checksum file
      // gives faster builds because we may not need to run npm install
      shell('git clean -xfd --exclude=checksum.txt --exclude=node_modules --exclude=bin')
      // get a checkout that we can push to
      shell('$SHELL -ex ${SCRIPT_FIX_BRANCH}')
      // do we need to run npm install?
      shell('$SHELL -ex ./scripts/checksum.sh')
      shell('git submodule update --init');

    gradle {
      tasks('build $TASK')
      useWrapper(false)
      buildFile(gradleBuildFile)
      gradleName(gradleVersion)
    }

  }

  // archive files
  publishers {

    // what to save, no excludes, keep all
    archiveArtifacts(archivePattern, null, false)

    extendedEmail(null, '$DEFAULT_SUBJECT', '$DEFAULT_CONTENT') {
      trigger([
               triggerName: 'Success',
               sendToRequester: true,
               sendToRecipientList: false
              ])
    }

  }

}

//////////////////////////////////////////////////
// JAVASCRIPT DEPLOYS
//////////////////////////////////////////////////

def JOB_TO_DEPLOY = "${parentFolder}/${jobNameTemp}/${jobName}_master_build"
def ARTIFACTS_TO_DEPLOY = 'release/**/*'
def ARTIFACTS_PREFIX = 'release'
def IHS_FOLDER_NAME = 'sbemployeebenefits'

// add further values here when ready to deploy to those environments
def deployEnvironments = [ sb: 'SB' ]

def SERVER1 = ''
def SERVER2 = ''

for ( i in deployEnvironments.values() ) {

  def envJobName = "${parentFolder}/${jobNameTemp}/${jobName}_${i}_deploy"

  if ( i == 'SB') {
    SERVER1 = SERVER_SB[0]
    SERVER2 = SERVER_SB[1]
  }
  if ( i == 'ST') {
    SERVER = SERVER_ST
  }
  if ( i == 'FT') {
    SERVER = SERVER_FT
  }

  freeStyleJob(envJobName) {

    environmentVariables {
      keepSystemVariables(true)
      keepBuildVariables(true)
    }

    // log rotation
    // 20 days or 20 builds
    logRotator(20, 20, 20, 20)

    wrappers {
      // inject passwords
      injectPasswords()

      // wipe out all files before start
      preBuildCleanup {
        // this intentionally commented out because that means all files deleted
        // includePattern(String pattern)  // all files are deleted if omitted
        // deleteDirectories(true) // defaults to false if omitted
      }
    }

    // actual build steps
    steps {

      // copy files over
      copyArtifacts("${JOB_TO_DEPLOY}", "$ARTIFACTS_TO_DEPLOY") {
        latestSuccessful(true)
      }

      publishOverSsh {
        // TODO
        // how to abstract this to arbitrary number of servers
        server(SERVER1) {
          // credentials are done at the global level in jenkins
          verbose(true)
          transferSet {
            // all front end pushes should come from release
            // push a zip file as well for easy download
            sourceFiles("${ARTIFACTS_TO_DEPLOY}")
            removePrefix("${ARTIFACTS_PREFIX}")
            // directory below is under the htdocs folder for IBM IHS server
            // that is set on a global level as well
            remoteDirectory("${IHS_FOLDER_NAME}")
          }
        }
        server(SERVER2) {
          // credentials are done at the global level in jenkins
          verbose(true)
          transferSet {
            // all front end pushes should come from release
            // push a zip file as well for easy download
            sourceFiles("${ARTIFACTS_TO_DEPLOY}")
            removePrefix("${ARTIFACTS_PREFIX}")
            // directory below is under the htdocs folder for IBM IHS server
            // that is set on a global level as well
            remoteDirectory("${IHS_FOLDER_NAME}")
          }
        }
        // will try to publish to all servers
        continueOnError(true)
        // but will fail the job
        failOnError(true)
        // SSH from this jenkins server always, not any secondary servers we might add
        alwaysPublishFromMaster(true)
      }
    } // steps

    publishers {

      // what to save, no excludes, keep all
      archiveArtifacts("${ARTIFACTS_TO_DEPLOY}", null, false)

      fingerprint("${ARTIFACTS_TO_DEPLOY}", true)

      // email vars defined in Jenkins
      extendedEmail('$EMAIL_EBUS_DEVS', '$DEFAULT_SUBJECT', '$DEFAULT_CONTENT') {
        trigger([
                 triggerName: 'Failure',
                 sendToDevelopers: true,
                 sendToRequester: true,
                 includeCulprits: true,
                 sendToRecipientList: false
                ])
        trigger([
                 triggerName: 'Success',
                 sendToDevelopers: false,
                 sendToRequester: true,
                 includeCulprits: false,
                 sendToRecipientList: true
                ])

      }

    }

  }

}
