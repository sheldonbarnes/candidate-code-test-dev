# Sheldon Barnes OneAmerica Code Test

The main part of the application involves displaying alerts, and allowing the end-user to add and delete alerts.

![Alerts_1.PNG](https://bitbucket.org/repo/9kB8g4/images/1262363455-Alerts_1.PNG)

The model behind the the alerts view is defined in the src/js/models/alerts-m.js file.

The model includes default values, the urlRoot function for defining the URL that Backbone will make a RESTful JSON request to, and a parse function for doing any necessary parsing.  

When the "Alerts" link is clicked, the routing in the application (main-rts.js and routes.js) transitions to the View defined in src/apps/alerts.

The folder for the view is divided into 3 folders (partials, templates, and views).  

![Alerts_3.PNG](https://bitbucket.org/repo/9kB8g4/images/2805986365-Alerts_3.PNG)

The views folder contains the majority of the logic and event handling for the view.  For example, the views/alerts-v.js file contains the methods that handle the adding (addAlert) and deleting (deleteAlert) of the alerts.  These method act on the model that is set during the initialize function.  The file also contains a reference to a partials JS file that contains the partial template that is utilized to display each individual alert.

The templates folder contains the main template for the view defined in a HandleBars template file (.hbs).

The main template file references an alerts-alert-item partial template that displays each individual alert ( {{> alerts-alert-item.... )

![Alerts_2.PNG](https://bitbucket.org/repo/9kB8g4/images/2595460217-Alerts_2.PNG)

The UserInfo view is defined as a partial template in the src/apps/userinfo/partials/userinfo-update.hbs file.  It utilizes BootStrap's Modal for displaying the modal view.  

![Alerts_4.PNG](https://bitbucket.org/repo/9kB8g4/images/2879279031-Alerts_4.PNG)

The majority of the logic and event handling for the UserInfo view piggybacks on the src/apps/navbar/views/navbar-v.js file.  This component is responsible for displaying the the top bar that contains the logo, and the Alerts and Username links.  


## REST API Service ##

![Alerts_5.PNG](https://bitbucket.org/repo/9kB8g4/images/1198545961-Alerts_5.PNG)

The 2 models point to an Appiary, a service that allows developers to quickly design, prototype, document and test APIs.

For example, whenever the UsersModel fetches the data from /users, the service returns the appropriate data.  

![Alerts_6.PNG](https://bitbucket.org/repo/9kB8g4/images/1355660382-Alerts_6.PNG)