# Homepage Template

Block Type: Image
Contains: Image URL

Block Type: Basic Content
Contains: Heading, Text
Can repeat 1-N times

Block Type: Quick Links
Contains: Type, Heading, Text, Link
Can repeat 1-N times

Example

    Image URL - ShortTextComponent

    Basic Content Heading - ShortTextComponent
    Basic Content Text - RichTextComponent

    Quick Links
        Quick Link Type - ShortTextComponent
        Quick Link Header - ShortTextComponent
        Quick Link Text - RichTextComponent
        Quick Link Link - ShortTextComponent

        Quick Link Type
        Quick Link Header
        Quick Link Text
        Quick Link Link

        Quick Link Type
        Quick Link Header
        Quick Link Text
        Quick Link Link

TODO - template needs up to 5 of the basic content types
Right now only have one

# Product Template

Block Type: Section Content
Contains: Heading, Text
Only 1

Block Type: SubSection Content
Contains: Title, Text
Can repeat 1-N times

Block Type: Grid Content
Contains: Heading, Text, Media Blocks
Only 1 of these, but it contains 1-N of the Media Blocks (below)

Block Type: Media Block
Contains: Type, Heading, Text, Link

Example

(1 of these)
    SectionContent-Heading - ShortTextComponent
    SectionContent-Text - RichTextComponent

(0-5 of these)
    SubsectionContent-Heading - ShortTextComponent
    SubsectionContent-Text - RichTextComponent

(1 of these)
    GridContent-Heading - ShortTextComponent
    GridContent-Text - RichTextComponent

(0-9 of these)
    MediaBlock
        MediaBlock-Type - ShortTextComponent
        MediaBlock-Heading - ShortTextComponent
        MediaBlock-Text - RichTextComponent

# FAQ Template

Block Type: Title
Contains: Heading
Only 1

Block Type: FAQ Tab
Contains: Heading, Q&A Text
Can repeat 1-N times, each one contains 1-N Q&A Text

Q&A Text
Contains: Text, made up of question and answer
Can repeat 1-N times

(5 of these)
    FaqContent-Heading - ShortTextComponent
    FaqContent-Text - RichTextComponent

The rich text part of this will have to be parsed as a series of
<p>question</p>
<p>answer</p>

# Forms Template

Block Type: Tab Container Block
Contains: Contains 1-N Tab Content Containers
Only 1, but can have multiple Tab Content Containers

Block Type: Tab Content Containers
Contains: Type, Title, Text, 1-N Tab Content Blocks

Block: Tab Content Block
Containts: Title, Text
Can repeat 1-N times

Example

        TabContent-Type ShortTextComponent
        TabContent-Title ShortTextComponent
        TabContent-Text RichTextComponent

            TabContentBlock-Section
            TabContentBlock-Text

The rich text part of this will have to be parsed as a series of
<hX>Subsection title</hX> (optional)
<a><hX>Form title</hX><a>
<p>form text</p>
