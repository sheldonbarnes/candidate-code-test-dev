# WCM proxy pass for the rest stuff

We proxy /wps/contenthandler to WCMSERVER/wps/contenthandler because we need to for REST.
The inital response will come as a 302 with /wps/contenthandler/blah/blah
We need to follow that 302, so we have to have the proxypass match that exact format again.
So the server follows it again, gets the real data, and comes back to us.

Also need proxy passes like this:

 ProxyPass /wps/wcm/myconnect/ http://www.stwcm.oneamerica.com/wps/wcm/myconnect/
  ProxyPassReverse /wps/wcm/myconnect/ http://www.stwcm.oneamerica.com/wps/wcm/myconnect/
  ProxyPass /wps/wcm/connect/ http://www.stwcm.oneamerica.com/wps/wcm/connect/
  ProxyPassReverse /wps/wcm/connect/ http://www.stwcm.oneamerica.com/wps/wcm/connect/
  ProxyPass /wps/contenthandler/ http://www.stwcm.oneamerica.com/wps/contenthandler/
  ProxyPassReverse /wps/contenthandler/ http://www.stwcm.oneamerica.com/wps/contenthandler/

We had to add the proxy pass for the FAQ data as well here. CHeck the real setup before changing.


in addition to any other ones needed for OASec, etc.

Anonymous WCM access

The REST api doesn't authenticate at all. because of this, and how we link internally inside WCM, we have to remove the session based links that WCM creates.
Ex - wcm creates a link like /wps/wcm/myconnect/
We need it to be /wps/wcm/connect/

So we handle that on the content parsing side for any text item

# OASec and groups

From Nick -
I think maybe there is still some setup missing for oasec in ST.  I could log in to ST employee benefits and then access the /oasec/secure services but ibill001 could not.  I think the OASec-SecurePolicy in ST needs changed to be more like prod and use the OneAmerica-Users group.  It only has some AcctServer… groups now.  I added AcctServ_Read_Only to ibill001 and then it could access /oasec/secure services from eben.


Can we also add Put as an allowed action for oasec/secure and /public?  The Put method is supposed to be used for updates in REST.  There is no rush on this.

OAsec needs PUT allowed as well
OAsec needs to have the OneAmerica users, and allow PUT as a method


# REST errors


For 40x errors, just return back that response and I'll handle it, using the conventions Leo wrote in the wiki.

For field level errors, they took the object that was sent (person) and return back the errors array like so:

{

"items" : null,

"errors" : [ {

"field" : "persona.firstname",

"code" : "Value is required."

}]
}


For global errors, the field was null. I believe this is where you said it can't be blank. What I'd say we use then is something like __global as a key, something that would never be part of a model I send you.  I'm open to suggestions on this.

# WCM

anonymous REST api access in production
