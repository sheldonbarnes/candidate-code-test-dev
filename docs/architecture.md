# Architecture

## Multiple apps

The overall app is a work-in-progress attempt to break out the common pieces that we will have across multiple applications. There are separate apps for the header/navigation, the sidebar, the footer, and the content.

In addition, there is a WCM Content app, that is just a collection of content fetched from WCM.

The guiding principle is that each app should know NOTHING about any other application where possible. Of the above apps, the Content app is the only one that has some knowledge of others, in that it uses the WCM Content application.

### Communication between apps

All communication between apps is done using [Backbone.Radio](https://github.com/marionettejs/backbone.radio). Make sure to inspect the `package.json` file to determine the exact version being used.

The main app sets up communication channels and passes those into the subapps as appropriate. Some apps rely on basic browser events to communicate. The sidebar application changes the location.hash property, and main router listens for the built-in `onhashchange` event, and triggers communcation through a channel to the content application.

## Code Reuse

Eventually, these applications will be NPM modules, composed of other modules that OneAmerica will create. Common pieces like the testing framework, the utils file, and config files will be require'd in by other modules. For the moment, these pieces are separate files that live in each app, and may/may not have all the same functionality.

## Globals

This architecture exposes globals on the window such as $, Backbone, Marionette, and other libraries. This enables use of the common debugging tools like the browser developer tools, and also specific Backbone and Marionette Inspectors.

## Data

This app uses the localData-dev repository to provide local data. The app will send calls to URLs like `/employeebenefits/wps/.*` to localhost:5000, which should be the localData-dev repo. That repo uses the defined mappings for URLs that match to know what data to serve.

Note that the `/employeebenefits` part of the URL is ONLY used when doing local development. When outside of local development, the URLS will NOT have that prefix, but the rest of the URL will match.
