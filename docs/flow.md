# Application Initialization and Flow

This is a  breakdown of the entire process of the application. It assumes a working knowledge of the entire tech stack used.

The `gulp` build process assembles the files and places them in `dist`. There is a single complied `app.build.js` file that maps to the `src/js/app.js` file. File references from this point on are relative to teh `src/js` directory.

## app.js and modules

First loaded in that file is a `config/config.js` file.

### config/config.js

This file is an IIFE, meaning that it executes the first time it is called, and from that point afterwards it is stored in memory.

It returns an object with various properties that can be used to determine code flow. As part of the process of determining the values of the properties, it parses through the query parameters and localStorage. It converts 'true' to true, 'false' to false, and leaves strings as is.

Ex: if you load the app with http://localhost:3000/?demo=true, the config object will have a property of `demo: true`.

LocalStorage settings will override query param settings.

The config file also sets a namespace to be used, and sets a debug setting that will evaluate to true only when doing local development.

### logic/index.js

This file should contain any code that makes a boolean decision based on business logic. Instead of scattering code and checks through the app, pass in variables to functions here and return a boolean. This should keep all business logic in a single place.

Currently the menu to show is determined using this file.

### logic/roles.js

Contains the list of the roles for the application.

### selectors.js

All classnames should live in this file, and be passed to templates from here. Classes used for JavaScript only should be prefixed with `js`.

### utils.js

Any utility function should live here. The main feature of this file currently is a wrapper for the `console` object. Instead of having to write `console.log('EBEN: some message here')`, you can write `utils.logger.log('some messages here')`. The logger function will be a no-op unless the application is running in debug/developer mode to prevent production console logging.

### Views

Error and Loading views are used as needed, and passed into other apps as needed.
### Apps and app `el` modules

Each app lives in its own folder. Each may have a config, utils, and multiple view/template files.
Each app also has an `el.js` module. This returns back a string of HTML that should be used to create the containing element for the application (see below).

### Routing and data

The `MainRouter` listens for the onhashchange events and dispatches events along the communication channel. When a route is called, the route name (like `contact-us`) is passed along to the listening consumer.

Specifically, the Content app listens for these events to know what content item to render.

The `data/routes.js` file is the linchpin to the application. It indicates what view to show when the appropriate route is called, It also indicates what WCM content is needed for that view.

### Sidebar Menu

This menu is created via a collection, and listens for any changes to the collection to rerender itself.

### User

The app needs to know what role the logged in user has - producer, homeoffice, etc.

## Startup

On startup, a loading view is presented. While this is rendered, the app does a check for demo mode. If in demo mode, it will trigger a callback that normally happens after the user data is loaded. If not in demo mode, it will fetch the user role and then trigger the callback.

When the callback is triggered, the 4 apps are started. The navbar is created first. Then the sidebar, the content app, and the footer app. Each of those apps is passed whatever it needs to properly render the content.

The Navigation, sidebar, and footer apps are basic and just render out content.

## Content app

The Content app is more complicated.

This piece ties together the entire application. It listens for a route event, and then checks to see if the appropriate content is already loaded into memory by the wcm content app. (That app is just a Backbone collection of text.) It runs the appropriate checks to see if the data is there, or waits for it to be fetched. Finally it renders out the view needed with that content.



## WCM Content app

One note about this app is that it uses backbone-fetch-cache to store cached items for 5 minutes in localStorage.This means that even on page reload, data can be cached.

### FAQs

FAQs are a special case. Flow is:
the routes file defines an FAQ view with the normal ID, because it needs to load that data
There is a single FAQ item stored in the WcmContent collection, because a single call gets ALL that data.

Will not be fetched again until a page refresh - navigation will not do it

## Security and login/logout

Anything under secure will still be secured by SM.
This includes both the REST routes and anything that we put into a secure/ folder.

He's setting site minder to return a 401 anytime something that is secured is requested w/o the appropriate cookie. This means that the REST routes will be be handled the same way whether it's a cookie authenticated item that's not allowed, or if it's requested w/o the SM cookie.

He's also setting the SM url for the default login page to be public/index.html.

In the app itself, I'm structuring it like this:
images/
fonts/
css/
js/

public/
    index.html
secure/
    index.html

The two index.html files are exactly the same, and both reference the same files in the images, css, fonts, js folders.  The login view is actually part of the app itself, not a separate file. This keeps the logic all in once place.

The default page for the app should be secure/index.html.  The login view is part of the app itself, not a separate page.

If a user goes there and is not logged in, the user gets bounced to public/index.html. The app checks the user route, gets a 401 (no cookie), checks to see if it's already on the publiv page, and renders out in public mode, showing a login screen plus the left hand menus for the public view.

After logging in, the user is sent to secure/index.html, the user route runs, and then shows the appropriate content.

At any point, since the cookie is for the domain, if the cookie expires and a call is made to get new data*, SM will see the cookie is expired, and will send back a 401. App sees that, checks to make sure it's not already on the public page, and sends the user back to the public/index.html page to login in again.

For logout, the user is sent to /secure/logout.html, which does a meta refresh back to the /secure/index.html page, where the normal process should kick in.

### Security Flows

#### Local development issues

The localDate-dev branch for this is eben-wcm-content. It started as just WCM content, but now contains all the data. It is SIGNIFICANTLY behind master of that repo, and cannot easily be merged or have master merged right now.

The local development does NOT use cookies currently. You can get functionality by looking in the `src/js/app.js` file at the 'setupDemo' code and adjusting JSON there. You can then pass '?dmeo=true&role=FOO' where roles is a valid role, and the app will act like production.

For a single type of role, `delegate`, the app WILL fetch from the localData-dev repo.

##### Forgot password

First step: POST captcha challenge and response to a URL like:
/oasec/public/rest/user/{userID}/forgotpassword/captcha.json

#### Data

To see/user real data:
https://www.stemployeebenefits.aul.com/oasec/jsonTest.jsp
https://www.stemployeebenefits.aul.com/jsonTest.jsp

Login using the URL below first.
https://www.stemployeebenefits.aul.com/employeebenefitsWeb/public/login.faces

#### Overview of how these mini-apps work

The content app gets a routing request and maps the view to the views that it knows about.
For these mini apps, the view that the content app knows about is a LayoutView. This LayoutView functions almost like a controller for a mini app. Down the road, they can be their own apps.

The layout view sets up the following:

- An initial data fetch.
- Success and error callbacks for that initial data fetch
- A view to render on success of the initial data fetch. This view will handle the rest of the work for the app.
- A success callback and view rendering for successful completion of the entire process. (No error handling at this level, the user will stay in the process if the process itself gives an error.)
- A clickable link for the success view that allows repetition of the process (Backbone won't rerender a view if you're already on the route that loads that view.)
- A Backbone.Radio channel for the subview to talk back to the layout view, to indicate the successful completion.

##### SubView Flow

In the subview, the following are setup/used:

A model for the information needed. THis model may use a 'save' override to prevent syncing back properties such as the errors array. The model also includes a validation step that will populate an errors property on the model to allow direct re-rendering of the same model.

An template that renders out the HTML,along with empty error elements that can be filled on a rerender.

Submit events for a form that trigger validation and then an initial save of the data.

##### Security Features

We use custom OASec features for Forgot password, and Change security Info. We do this because the Eben users have the old security setup, where they have the 4 standard questions that they answer. The new OASec flow uses the 5 questions chosen by the user.

For Forgot User ID, Change User ID, and Change Password, we use the standard flows.

The views/models for all these are a little strange, because we sometimes change the data we post depending on the step. The localDate-dev files often have to be manipulated by hand, because the error handling is not set up to automatically flip back and forth.

The views for all these live in src/apps/content/, in a folder for each.

## Benefits

POST /benefit/search - send along search criterial for account name of other info.
returns list of possible accounts.

If user is branch/dept aware (QUESTION - how do we flag this?), they get a list of all accounts, and then select and call the next service.

If user is branch/dept aware AND there is only one account, should automatically see that single account. QUESTION - why would we even show a search then? If this scenario that there was only ONE account that matched the search, or one account overall?

If user is not branch/dept aware, they see a list of all documents for the one account they can see.

GET /benefit/{accountNumber}/benefitDocuments - list of documents for that account. Seems like it should be /benefit/documents for all accounts, and benefit/documents/{accountNumber} for a single account

POST /benefit/{accountNumber}/streamBenefitDocument - returns the PDF to view. QUESTION - why a POST when it is really a GET?
